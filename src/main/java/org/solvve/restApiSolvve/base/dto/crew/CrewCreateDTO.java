package org.solvve.restapisolvve.base.dto.crew;

import lombok.Data;
import org.solvve.restapisolvve.base.domain.CrewDepartment;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class CrewCreateDTO {
    @NotNull
    private UUID personId;
    private CrewDepartment department;
    private String job;
}
