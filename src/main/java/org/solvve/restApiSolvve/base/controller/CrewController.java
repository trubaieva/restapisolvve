package org.solvve.restapisolvve.base.controller;

import org.solvve.restapisolvve.base.dto.crew.CrewCreateDTO;
import org.solvve.restapisolvve.base.dto.crew.CrewPatchDTO;
import org.solvve.restapisolvve.base.dto.crew.CrewPutDTO;
import org.solvve.restapisolvve.base.dto.crew.CrewReadDTO;
import org.solvve.restapisolvve.base.service.CrewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/movies/{movieId}/crews")
public class CrewController {

    @Autowired
    private CrewService crewService;

    @PreAuthorize("permitAll()")
    @GetMapping
    public List<CrewReadDTO> getMovieCrews(@PathVariable UUID movieId) {
        return crewService.getMovieCrews(movieId);
    }

    @PreAuthorize("permitAll()")
    @GetMapping("/{id}")
    public CrewReadDTO getMovieCrew(@PathVariable UUID movieId, @PathVariable UUID id) {
        return crewService.getMovieCrew(movieId, id);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'CONTENT_MANAGER')")
    @PostMapping
    public CrewReadDTO createMovieCrew(@PathVariable UUID movieId,
            @RequestBody @Valid CrewCreateDTO createDTO) {
        return crewService.createMovieCrew(movieId, createDTO);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'CONTENT_MANAGER')")
    @PutMapping("/{id}")
    public CrewReadDTO updateMovieCrew(@PathVariable UUID movieId, @PathVariable UUID id,
            @RequestBody @Valid CrewPutDTO putDTO) {
        return crewService.updateMovieCrew(movieId, id, putDTO);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'CONTENT_MANAGER')")
    @PatchMapping("/{id}")
    public CrewReadDTO patchMovieCrew(@PathVariable UUID movieId, @PathVariable UUID id,
            @RequestBody @Valid CrewPatchDTO patchDTO) {
        return crewService.patchMovieCrew(movieId, id, patchDTO);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'CONTENT_MANAGER')")
    @DeleteMapping("/{id}")
    public void deleteMovieCrew(@PathVariable UUID movieId, @PathVariable UUID id) {
        crewService.deleteMovieCrew(movieId, id);
    }
}
