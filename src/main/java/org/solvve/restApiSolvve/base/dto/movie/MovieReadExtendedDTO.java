package org.solvve.restapisolvve.base.dto.movie;

import lombok.Data;
import org.solvve.restapisolvve.base.dto.actorrole.ActorRoleReadDTO;
import org.solvve.restapisolvve.base.dto.crew.CrewReadDTO;
import org.solvve.restapisolvve.base.dto.genre.GenreReadDTO;
import org.solvve.restapisolvve.base.dto.moviereview.MovieReviewReadDTO;
import org.solvve.restapisolvve.base.dto.news.NewsReadDTO;
import org.solvve.restapisolvve.base.dto.productioncompany.ProductionCompanyReadDTO;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Data
public class MovieReadExtendedDTO {
    private UUID id;
    private String name;
    private String country;
    private String language;
    private List<GenreReadDTO> genres;
    private List<CrewReadDTO> crews;
    private List<ActorRoleReadDTO> actorRoles;
    private String storyLine;
    private LocalDate releaseDate;
    private Integer runtime;
    private List<ProductionCompanyReadDTO> productionCompanies;
    private Integer budget;
    private Integer revenue;
    private Boolean isReleased;
    private List<MovieReviewReadDTO> movieReviews;
    private Double avgRating;
    private Double forecastRating;
    private List<NewsReadDTO> news;
    private Instant createdAt;
    private Instant updatedAt;
}
