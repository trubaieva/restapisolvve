package org.solvve.restapisolvve.base.repository;

import org.solvve.restapisolvve.base.domain.Genre;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface GenreRepository extends CrudRepository<Genre, UUID> {
    Genre findByName(String name);
}
