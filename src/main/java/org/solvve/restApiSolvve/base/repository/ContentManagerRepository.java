package org.solvve.restapisolvve.base.repository;

import org.solvve.restapisolvve.base.domain.ContentManager;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ContentManagerRepository extends CrudRepository<ContentManager, UUID> {
    boolean existsByIdAndEmail(UUID id, String email);
}
