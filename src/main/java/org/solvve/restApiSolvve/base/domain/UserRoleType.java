package org.solvve.restapisolvve.base.domain;

public enum UserRoleType {
    REGISTERED_USER,
    ADMIN,
    MODERATOR,
    CONTENT_MANAGER
}
