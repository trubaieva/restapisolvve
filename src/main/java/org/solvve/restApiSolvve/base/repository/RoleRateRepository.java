package org.solvve.restapisolvve.base.repository;

import org.solvve.restapisolvve.base.domain.RoleRate;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface RoleRateRepository extends CrudRepository<RoleRate, UUID> {

    @Query("select avg(rr.rate) from RoleRate rr where rr.actorRole.id = :roleId")
    Double calcAverageRateOfRole(UUID roleId);
}
