package org.solvve.restapisolvve.base.repository;

import org.solvve.restapisolvve.base.domain.MovieReview;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface MovieReviewRepository extends CrudRepository<MovieReview, UUID> {
}
