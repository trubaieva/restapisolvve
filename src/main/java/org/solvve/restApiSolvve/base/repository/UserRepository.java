package org.solvve.restapisolvve.base.repository;

import org.solvve.restapisolvve.base.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface UserRepository extends CrudRepository<User, UUID> {

    //Optional<User> findByEmail(String email);
    User findByEmail(String email);

    boolean existsByIdAndEmail(UUID id, String email);
}
