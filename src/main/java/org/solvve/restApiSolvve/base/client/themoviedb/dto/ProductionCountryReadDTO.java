package org.solvve.restapisolvve.base.client.themoviedb.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ProductionCountryReadDTO {

    @JsonProperty("iso_3166_1")
    private String code;
    private String name;
}
