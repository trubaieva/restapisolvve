package org.solvve.restapisolvve.base.job;

import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.solvve.restapisolvve.base.domain.Movie;
import org.solvve.restapisolvve.base.domain.RegisteredUser;
import org.solvve.restapisolvve.base.repository.MovieRepository;
import org.solvve.restapisolvve.base.service.MovieService;
import org.solvve.restapisolvve.base.util.EntityCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
@Import(ScheduledConfigurationsTest.ScheduledTestConfig.class)
@ActiveProfiles("test")
@Sql(statements = {"delete from rate", "delete from registered_user", "delete from app_user", "delete from movie",
        "delete from actor", "delete from person"},
        executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class UpdateAverageRatingOfMoviesJobTest {

    @Autowired
    private UpdateAverageRatingOfMoviesJob updateAverageRatingOfMoviesJob;

    @Autowired
    private EntityCreator entityCreator;

    @Autowired
    private MovieRepository movieRepository;

    @SpyBean
    private MovieService movieService;

    @Test
    public void testUpdateAverageRatingOfMovies() {
        RegisteredUser creator = entityCreator.createRegisteredUser();
        Movie movie = entityCreator.createMovieWithoutAvgRating();

        entityCreator.createMovieRate(creator, movie, 3);
        entityCreator.createMovieRate(creator, movie, 5);
        updateAverageRatingOfMoviesJob.updateAverageRatingOfMovies();

        movie = movieRepository.findById(movie.getId()).get();
        Assert.assertEquals(4.0, movie.getAvgRating(), Double.MIN_NORMAL);
    }

    @Test
    public void testMoviesUpdatedIndependently() {
        RegisteredUser creator = entityCreator.createRegisteredUser();
        Movie movie1 = entityCreator.createMovieWithoutAvgRating();
        Movie movie2 = entityCreator.createMovieWithoutAvgRating();

        entityCreator.createMovieRate(creator, movie1, 4);
        entityCreator.createMovieRate(creator, movie2, 5);
        UUID[] failedId = new UUID[1];
        Mockito.doAnswer(invocationOnMock -> {
            if (failedId[0] == null) {
                failedId[0] = invocationOnMock.getArgument(0);
                throw new RuntimeException();
            }
            return invocationOnMock.callRealMethod();
        }).when(movieService).updateAverageRating(Mockito.any());

        updateAverageRatingOfMoviesJob.updateAverageRatingOfMovies();

        for (Movie m : movieRepository.findAll()) {
            if (m.getId().equals(failedId[0])) {
                Assert.assertNull(m.getAvgRating());
            } else {
                Assert.assertNotNull(m.getAvgRating());
            }
        }
    }

}
