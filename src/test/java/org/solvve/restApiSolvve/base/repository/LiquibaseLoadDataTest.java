package org.solvve.restapisolvve.base.repository;

import org.junit.Assert;
import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.TestPropertySource;

@TestPropertySource(properties = "spring.liquibase.change-log=classpath:db/changelog/db.changelog-master.xml")
public class LiquibaseLoadDataTest extends BaseTest {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private CrewRepository crewRepository;

    @Autowired
    private ActorRoleRepository actorRoleRepository;

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private ProductionCompanyRepository productionCompanyRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private ModeratorRepository moderatorRepository;

    @Autowired
    private ContentManagerRepository contentManagerRepository;

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private CheckRequestRepository checkRequestRepository;

    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private RequestToCorrectRepository requestToCorrectRepository;

    @Autowired
    private LikeRepository likeRepository;

    @Autowired
    private RateRepository rateRepository;

    @Autowired
    private RoleRateRepository roleRateRepository;

    @Autowired
    private RoleReviewRepository roleReviewRepository;

    @Autowired
    private ReviewLikeRepository reviewLikeRepository;

    @Autowired
    private NewsLikeRepository newsLikeRepository;

    @Autowired
    private MovieRateRepository movieRateRepository;

    @Autowired
    private MovieReviewRepository movieReviewRepository;

    @Test
    public void testDataLoaded() {
        Assert.assertTrue(personRepository.count() > 0);
        Assert.assertTrue(actorRepository.count() > 0);
        Assert.assertTrue(movieRepository.count() > 0);
        Assert.assertTrue(crewRepository.count() > 0);
        Assert.assertTrue(actorRoleRepository.count() > 0);
        Assert.assertTrue(genreRepository.count() > 0);
        Assert.assertTrue(productionCompanyRepository.count() > 0);
        Assert.assertTrue(userRepository.count() > 0);
        Assert.assertTrue(adminRepository.count() > 0);
        Assert.assertTrue(registeredUserRepository.count() > 0);
        Assert.assertTrue(moderatorRepository.count() > 0);
        Assert.assertTrue(contentManagerRepository.count() > 0);
        Assert.assertTrue(reviewRepository.count() > 0);
        Assert.assertTrue(checkRequestRepository.count() > 0);
        Assert.assertTrue(newsRepository.count() > 0);
        Assert.assertTrue(requestToCorrectRepository.count() > 0);
        Assert.assertTrue(likeRepository.count() > 0);
        Assert.assertTrue(rateRepository.count() > 0);
        Assert.assertTrue(roleRateRepository.count() > 0);
        Assert.assertTrue(roleReviewRepository.count() > 0);
        Assert.assertTrue(reviewLikeRepository.count() > 0);
        Assert.assertTrue(newsLikeRepository.count() > 0);
        Assert.assertTrue(movieRateRepository.count() > 0);
        Assert.assertTrue(movieReviewRepository.count() > 0);
        Assert.assertTrue(userRoleRepository.count() > 0);
    }
}