package org.solvve.restapisolvve.base.dto.actorrole;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class ActorRoleReadDTO {
    private UUID id;
    private String character;
    private Integer castOrder;
    private UUID actorId;
    private UUID movieId;
    private Double avgRating;
    private Instant createdAt;
    private Instant updatedAt;
}
