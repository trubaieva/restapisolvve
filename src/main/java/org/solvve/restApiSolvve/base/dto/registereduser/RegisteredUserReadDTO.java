package org.solvve.restapisolvve.base.dto.registereduser;

import lombok.Data;
import org.solvve.restapisolvve.base.domain.UserRoleType;

import java.time.Instant;
import java.util.UUID;

@Data
public class RegisteredUserReadDTO {
    private UUID id;
    private String name;
    private String email;
    private UserRoleType role;
    private Instant createdAt;
    private Instant updatedAt;
}
