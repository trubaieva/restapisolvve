package org.solvve.restapisolvve.base.dto.crew;

import lombok.Data;
import org.solvve.restapisolvve.base.domain.CrewDepartment;

import java.util.UUID;

@Data
public class CrewPatchDTO {
    private UUID personId;
    private CrewDepartment department;
    private String job;
}
