package org.solvve.restapisolvve.base.client.themoviedb.dto;

import lombok.Data;

@Data
public class MovieReadShortDTO {

    private String id;
    private String title;
}
