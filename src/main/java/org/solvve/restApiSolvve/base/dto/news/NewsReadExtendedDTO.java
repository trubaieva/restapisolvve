package org.solvve.restapisolvve.base.dto.news;

import lombok.Data;
import org.solvve.restapisolvve.base.dto.movie.MovieReadDTO;
import org.solvve.restapisolvve.base.dto.person.PersonReadDTO;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Data
public class NewsReadExtendedDTO {
    private UUID id;
    private String header;
    private String text;
    private Integer likeCount;
    private List<MovieReadDTO> movies;
    private List<PersonReadDTO> persons;
    private Instant createdAt;
    private Instant updatedAt;
}
