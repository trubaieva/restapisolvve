package org.solvve.restapisolvve.base.controller;

import org.solvve.restapisolvve.base.controller.documentation.ApiPageable;
import org.solvve.restapisolvve.base.dto.PageResult;
import org.solvve.restapisolvve.base.dto.movie.*;
import org.solvve.restapisolvve.base.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/movies")
public class MovieController {

    @Autowired
    private MovieService movieService;

    @ApiPageable
    @PreAuthorize("permitAll()")
    @GetMapping
    public PageResult<MovieReadDTO> getMovies(MovieFilter filter, @ApiIgnore Pageable pageable) {
        return movieService.getMovies(filter, pageable);
    }

    @PreAuthorize("permitAll()")
    @GetMapping("/{id}")
    public MovieReadDTO getMovie(@PathVariable UUID id) {
        return movieService.getMovie(id);
    }

    @PreAuthorize("permitAll()")
    @GetMapping("/{id}/extended")
    public MovieReadExtendedDTO getMovieExtended(@PathVariable UUID id) {
        return movieService.getMovieExtended(id);
    }

    @PreAuthorize("permitAll()")
    @GetMapping("/leader-board")
    public List<MovieInLeaderBoardReadDTO> getMoviesLeaderBoard() {
        return movieService.getMoviesLeaderBoard();
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'CONTENT_MANAGER')")
    @PostMapping
    public MovieReadDTO createMovie(@RequestBody MovieCreateDTO createDTO) {
        return movieService.createMovie(createDTO);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'CONTENT_MANAGER')")
    @PutMapping("/{id}")
    public MovieReadDTO updateMovie(@PathVariable UUID id, @RequestBody MoviePutDTO putDTO) {
        return movieService.updateMovie(id, putDTO);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'CONTENT_MANAGER')")
    @PatchMapping("/{id}")
    public MovieReadDTO patchMovie(@PathVariable UUID id, @RequestBody MoviePatchDTO patchDTO) {
        return movieService.patchMovie(id, patchDTO);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'CONTENT_MANAGER')")
    @DeleteMapping("/{id}")
    public void deleteMovie(@PathVariable UUID id) {
        movieService.deleteMovie(id);
    }
}
