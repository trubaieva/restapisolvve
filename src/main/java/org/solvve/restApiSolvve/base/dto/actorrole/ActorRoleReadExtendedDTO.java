package org.solvve.restapisolvve.base.dto.actorrole;

import lombok.Data;
import org.solvve.restapisolvve.base.dto.actor.ActorReadDTO;
import org.solvve.restapisolvve.base.dto.movie.MovieReadDTO;
import org.solvve.restapisolvve.base.dto.rolereview.RoleReviewReadDTO;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Data
public class ActorRoleReadExtendedDTO {
    private UUID id;
    private String character;
    private Integer castOrder;
    private ActorReadDTO actor;
    private MovieReadDTO movie;
    private List<RoleReviewReadDTO> roleReviews;
    private Double avgRating;
    private Instant createdAt;
    private Instant updatedAt;
}
