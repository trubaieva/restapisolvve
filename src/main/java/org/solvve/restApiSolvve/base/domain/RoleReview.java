package org.solvve.restapisolvve.base.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@DiscriminatorValue("role")
public class RoleReview extends Review {

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "actor_role_id", nullable = false, updatable = false)
    private ActorRole actorRole;
}
