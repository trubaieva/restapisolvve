package org.solvve.restapisolvve.base.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class Actor extends AbstractEntity {

    @NotNull
    @ManyToOne
    @JoinColumn(name = "person_id", nullable = false, updatable = false, unique = true)
    private Person person;

    @OneToMany(mappedBy = "actor", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ActorRole> actorRoles = new ArrayList<>();

    private Double avgRatingByRoles;

    private Double avgMovieRating;

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;
}
