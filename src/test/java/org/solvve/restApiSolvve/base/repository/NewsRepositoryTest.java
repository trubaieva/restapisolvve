package org.solvve.restapisolvve.base.repository;

import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.News;
import org.solvve.restapisolvve.base.util.EntityCreator;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;

import static org.junit.Assert.*;

public class NewsRepositoryTest extends BaseTest {

    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private EntityCreator entityCreator;

    @Test
    public void testCreatedAtIsSet() {
        News news = entityCreator.createNews();

        Instant createdAtBeforeReload = news.getCreatedAt();
        assertNotNull(createdAtBeforeReload);
        news = newsRepository.findById(news.getId()).get();

        Instant createdAtAfterReload = news.getCreatedAt();
        assertNotNull(createdAtAfterReload);
        assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        News news = entityCreator.createNews();

        Instant updatedAtBeforeReload = news.getUpdatedAt();
        assertNotNull(updatedAtBeforeReload);
        news = newsRepository.findById(news.getId()).get();

        Instant updatedAtAfterReload = news.getUpdatedAt();
        assertNotNull(updatedAtAfterReload);
        assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        news.setHeader("tro-lo-lo");
        newsRepository.save(news);
        news = newsRepository.findById(news.getId()).get();
        Instant updatedAtAfterModificationAndReload = news.getUpdatedAt();
        assertTrue(updatedAtAfterModificationAndReload.isAfter(updatedAtAfterReload));
    }

}
