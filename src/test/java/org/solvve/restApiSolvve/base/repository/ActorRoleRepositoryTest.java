package org.solvve.restapisolvve.base.repository;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.Actor;
import org.solvve.restapisolvve.base.domain.ActorRole;
import org.solvve.restapisolvve.base.domain.Movie;
import org.solvve.restapisolvve.base.domain.Person;
import org.solvve.restapisolvve.base.util.EntityCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.transaction.support.TransactionTemplate;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class ActorRoleRepositoryTest extends BaseTest {

    @Autowired
    private ActorRoleRepository actorRoleRepository;

    @Autowired
    private EntityCreator entityCreator;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test(expected = TransactionSystemException.class)
    public void testSaveActorRoleValidation() {
        ActorRole actorRole = new ActorRole();
        actorRoleRepository.save(actorRole);
    }

    @Test
    public void testFindByIdAndMovieId() {
        List<ActorRole> actorRoles = createTestActorRoles();
        ActorRole expected = actorRoles.get(2);

        ActorRole actual = actorRoleRepository.findByIdAndMovieId(expected.getId(), expected.getMovie().getId());

        Assertions.assertThat(actual.getId()).isEqualTo(expected.getId());
        Assertions.assertThat(actual.getMovie().getId()).isEqualTo(expected.getMovie().getId());
    }

    @Test
    public void testFindAllByMovieId() {
        List<ActorRole> actorRoles = createTestActorRoles();

        List<ActorRole> actual = actorRoleRepository.findAllByMovieId(actorRoles.get(2).getMovie().getId());
        Assertions.assertThat(actual)
                .extracting(ActorRole::getId)
                .isEqualTo(Arrays.asList(actorRoles.get(2).getId(), actorRoles.get(3).getId()));
        Assertions.assertThat(actual)
                .extracting(ActorRole::getMovie)
                .extracting("id")
                .isEqualTo(Arrays.asList(actorRoles.get(2).getMovie().getId(), actorRoles.get(3).getMovie().getId()));
    }

    @Test
    public void testCreatedAtIsSet() {
        ActorRole actorRole = entityCreator.createActorRole();

        Instant createdAtBeforeReload = actorRole.getCreatedAt();
        assertNotNull(createdAtBeforeReload);
        actorRole = actorRoleRepository.findById(actorRole.getId()).get();

        Instant createdAtAfterReload = actorRole.getCreatedAt();
        assertNotNull(createdAtAfterReload);
        assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        ActorRole actorRole = entityCreator.createActorRole();

        Instant updatedAtBeforeReload = actorRole.getUpdatedAt();
        assertNotNull(updatedAtBeforeReload);
        actorRole = actorRoleRepository.findById(actorRole.getId()).get();

        Instant updatedAtAfterReload = actorRole.getUpdatedAt();
        assertNotNull(updatedAtAfterReload);
        assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        actorRole.setActor(entityCreator.createActor());
        actorRoleRepository.save(actorRole);
        actorRole = actorRoleRepository.findById(actorRole.getId()).get();
        Instant updatedAtAfterModificationAndReload = actorRole.getUpdatedAt();
        assertTrue(updatedAtAfterModificationAndReload.isAfter(updatedAtAfterReload));
    }

    @Test
    public void testGetIdsFromRoles() {
        Set<UUID> expectedIdsOfRoles = new HashSet<>();
        expectedIdsOfRoles.add(entityCreator.createActorRole().getId());
        expectedIdsOfRoles.add(entityCreator.createActorRole().getId());

        transactionTemplate.executeWithoutResult(status -> {
            Assert.assertEquals(expectedIdsOfRoles, actorRoleRepository.getIdsFromRoles().collect(Collectors.toSet()));
        });
    }

    @Test
    public void testFindByMovieNameAndActorPersonNameAndCharacter() {
        entityCreator.createActorRole();
        Person person = entityCreator.createPerson();
        Actor actor = entityCreator.createActor(person);
        Movie movie = entityCreator.createMovie();
        ActorRole actorRole = generateFlatEntityWithoutId(ActorRole.class);
        actorRole.setActor(actor);
        actorRole.setMovie(movie);
        actorRole.setCharacter("Simba");

        transactionTemplate.executeWithoutResult(status -> {
            actorRoleRepository.save(actorRole);
            ActorRole result = actorRoleRepository
                    .findByMovieNameAndActorPersonNameAndCharacter(movie.getName(), person.getName(),
                            "Simba");
            Assertions.assertThat(result).isEqualToIgnoringGivenFields(actorRole, "actor", "movie", "roleReviews");
            assertEquals(person, result.getActor().getPerson());
            assertEquals(actor, result.getActor());
            assertEquals(movie, result.getMovie());
        });
    }

    private List<ActorRole> createTestActorRoles() {
        Movie movie1 = entityCreator.createMovie();
        Movie movie2 = entityCreator.createMovie();

        Actor actor1 = entityCreator.createActor();
        Actor actor2 = entityCreator.createActor();

        ActorRole actorRole1 = entityCreator.createActorRole(actor1, movie1);
        ActorRole actorRole2 = entityCreator.createActorRole(actor2, movie1);
        ActorRole actorRole3 = entityCreator.createActorRole(actor1, movie2);
        ActorRole actorRole4 = entityCreator.createActorRole(actor2, movie2);

        return Arrays.asList(actorRole1, actorRole2, actorRole3, actorRole4);
    }
}
