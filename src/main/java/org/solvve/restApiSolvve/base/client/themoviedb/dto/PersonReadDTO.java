package org.solvve.restapisolvve.base.client.themoviedb.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class PersonReadDTO {

    private String id;
    private String name;
    private LocalDate birthday;
    private String biography;
    private String placeOfBirth;
    private Integer gender;
    private LocalDate deathday;
    private String knownForDepartment;

    /*private Double popularity;
    private Boolean adult;
    private String profilePath;
    private String homepage;*/
}
