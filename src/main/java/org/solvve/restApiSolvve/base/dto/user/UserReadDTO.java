package org.solvve.restapisolvve.base.dto.user;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class UserReadDTO {
    private UUID id;
    private String name;
    private String email;
    private Instant createdAt;
    private Instant updatedAt;
}
