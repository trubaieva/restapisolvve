package org.solvve.restapisolvve.base.repository;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.Actor;
import org.solvve.restapisolvve.base.domain.Person;
import org.solvve.restapisolvve.base.util.EntityCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;

import java.time.Instant;

import static org.junit.Assert.*;

public class ActorRepositoryTest extends BaseTest {

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private EntityCreator entityCreator;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testCreatedAtIsSet() {
        Actor actor = entityCreator.createActor();

        Instant createdAtBeforeReload = actor.getCreatedAt();
        assertNotNull(createdAtBeforeReload);
        actor = actorRepository.findById(actor.getId()).get();

        Instant createdAtAfterReload = actor.getCreatedAt();
        assertNotNull(createdAtAfterReload);
        assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSetAfterChildFieldModification() {
        Actor actor = entityCreator.createActor();

        Instant updatedAtBeforeReload = actor.getUpdatedAt();
        assertNotNull(updatedAtBeforeReload);
        actor = actorRepository.findById(actor.getId()).get();

        Instant updatedAtAfterReload = actor.getUpdatedAt();
        assertNotNull(updatedAtAfterReload);
        assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        actor.setAvgMovieRating(9.0);
        actorRepository.save(actor);

        actor = actorRepository.findById(actor.getId()).get();
        Instant updatedAtAfterModificationAndReload = actor.getUpdatedAt();
        assertTrue(updatedAtAfterModificationAndReload.isAfter(updatedAtAfterReload));
    }

    @Test
    public void testFindByPerson() {
        Person person = entityCreator.createPerson();
        entityCreator.createActor();

        transactionTemplate.executeWithoutResult(status -> {
            Actor actor = entityCreator.createActor(person);
            Actor result = actorRepository.findByPerson(person);
            Assertions.assertThat(result).isEqualToIgnoringGivenFields(actor, "person", "actorRoles");
            assertEquals(person, result.getPerson());
        });
    }
}
