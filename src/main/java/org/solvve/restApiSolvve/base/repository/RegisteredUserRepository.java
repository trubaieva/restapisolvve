package org.solvve.restapisolvve.base.repository;

import org.solvve.restapisolvve.base.domain.RegisteredUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface RegisteredUserRepository extends CrudRepository<RegisteredUser, UUID> {
    boolean existsByIdAndEmail(UUID id, String email);
}
