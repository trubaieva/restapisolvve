package org.solvve.restapisolvve.base.controller;

import org.solvve.restapisolvve.base.dto.productioncompany.ProductionCompanyCreateDTO;
import org.solvve.restapisolvve.base.dto.productioncompany.ProductionCompanyPutDTO;
import org.solvve.restapisolvve.base.dto.productioncompany.ProductionCompanyReadDTO;
import org.solvve.restapisolvve.base.service.ProductionCompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class ProductionCompanyController {

    @Autowired
    private ProductionCompanyService productionCompanyService;

    @PreAuthorize("permitAll()")
    @GetMapping("/production-companies")
    public List<ProductionCompanyReadDTO> getProductionCompanies() {
        return productionCompanyService.getProductionCompanies();
    }

    @PreAuthorize("permitAll()")
    @GetMapping("/production-companies/{id}")
    public ProductionCompanyReadDTO getProductionCompany(@PathVariable UUID id) {
        return productionCompanyService.getProductionCompany(id);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'CONTENT_MANAGER')")
    @PostMapping("/production-companies")
    public ProductionCompanyReadDTO createProductionCompany(@RequestBody ProductionCompanyCreateDTO createDTO) {
        return productionCompanyService.createProductionCompany(createDTO);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'CONTENT_MANAGER')")
    @PutMapping("/production-companies/{id}")
    public ProductionCompanyReadDTO updateProductionCompany(@PathVariable UUID id, @RequestBody
            ProductionCompanyPutDTO putDTO) {
        return productionCompanyService.updateProductionCompany(id, putDTO);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'CONTENT_MANAGER')")
    @DeleteMapping("/production-companies/{id}")
    public void deleteProductionCompany(@PathVariable UUID id) {
        productionCompanyService.deleteProductionCompany(id);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'CONTENT_MANAGER')")
    @PostMapping("/movies/{movieId}/production-companies/{id}")
    public List<ProductionCompanyReadDTO> addProductionCompanyToMovie(@PathVariable UUID movieId,
            @PathVariable UUID id) {
        return productionCompanyService.addProductionCompanyToMovie(movieId, id);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'CONTENT_MANAGER')")
    @DeleteMapping("/movies/{movieId}/production-companies/{id}")
    public List<ProductionCompanyReadDTO> removeProductionCompanyFromMovie(@PathVariable UUID movieId,
            @PathVariable UUID id) {
        return productionCompanyService.removeProductionCompanyFromMovie(movieId, id);
    }
}
