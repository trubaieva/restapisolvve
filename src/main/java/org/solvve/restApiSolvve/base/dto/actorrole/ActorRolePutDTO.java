package org.solvve.restapisolvve.base.dto.actorrole;

import lombok.Data;

import javax.validation.constraints.Max;
import java.util.UUID;

@Data
public class ActorRolePutDTO {
    private String character;
    private Integer castOrder;
    private UUID actorId;

    @Max(value = 10, message = "Rate shouldn't be bigger than 10")
    private Double avgRating;
}
