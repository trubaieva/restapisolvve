package org.solvve.restapisolvve.base.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToMany;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
public class UserRole extends AbstractEntity {

    @Enumerated(EnumType.STRING)
    private UserRoleType type;

    @ManyToMany(mappedBy = "userRoles")
    private Set<User> users = new HashSet<>();
}
