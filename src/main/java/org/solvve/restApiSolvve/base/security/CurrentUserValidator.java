package org.solvve.restapisolvve.base.security;

import org.solvve.restapisolvve.base.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class CurrentUserValidator {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthenticationResolver authenticationResolver;

    public boolean isCurrentUser(UUID userId) {
        Authentication authentication = authenticationResolver.getCurrentAuthentication();
        return userRepository.existsByIdAndEmail(userId, authentication.getName());
    }
}
