package org.solvve.restapisolvve.base.exception.handler;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
@AllArgsConstructor
public class ErrorInfo {
    private final HttpStatus status;
    private final Class exceptionClass;
    private final String message;
}
