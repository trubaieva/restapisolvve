package org.solvve.restapisolvve.base.domain;

public enum CorrectStatus {
    NEED_TO_VERIFY,
    FIXED,
    CANCEL
}
