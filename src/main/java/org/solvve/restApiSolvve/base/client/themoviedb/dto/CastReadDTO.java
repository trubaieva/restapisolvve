package org.solvve.restapisolvve.base.client.themoviedb.dto;

import lombok.Data;

@Data
public class CastReadDTO {

    private String castId;
    private String character;
    private String id; //personId
    private String name;
    private Integer order;
}
