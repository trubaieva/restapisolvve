package org.solvve.restapisolvve.base.repository;

import org.junit.Assert;
import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.Arrays;

public class MovieRelationshipTest extends BaseTest {

    @Autowired
    public ActorRepository actorRepository;

    @Autowired
    public ActorRoleRepository actorRoleRepository;

    @Autowired
    public CrewRepository crewRepository;

    @Autowired
    public GenreRepository genreRepository;

    @Autowired
    public PersonRepository personRepository;

    @Autowired
    public ProductionCompanyRepository productionCompanyRepository;

    @Autowired
    public MovieReviewRepository movieReviewRepository;

    @Autowired
    public NewsRepository newsRepository;

    @Autowired
    public RegisteredUserRepository registeredUserRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private TransactionTemplate transactionTemplate;

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status -> {
            runnable.run();
        });
    }

    @Test
    public void testCreateCrewsAndLink() {
        Movie movie = movieRepository.save(new Movie());

        Crew crew = new Crew();
        Person person = personRepository.save(new Person());
        crew.setPerson(person);
        crew.setMovie(movie);
        crewRepository.save(crew);

        movie.setCrews(Arrays.asList(crew));
        movieRepository.save(movie);

        inTransaction(() -> {
            Movie savedMovie = movieRepository.findById(movie.getId()).get();
            Assert.assertEquals(1, savedMovie.getCrews().size());

            Crew savedCrew = crewRepository.findById(crew.getId()).get();
            Assert.assertEquals(savedMovie, savedCrew.getMovie());
        });
    }

    @Test
    public void testCreateActorRolesAndLink() {
        Movie movie = movieRepository.save(new Movie());

        ActorRole actorRole = new ActorRole();
        Actor actor = new Actor();
        Person person = personRepository.save(new Person());
        actor.setPerson(person);
        actor = actorRepository.save(actor);
        actorRole.setActor(actor);
        actorRole.setMovie(movie);
        actorRoleRepository.save(actorRole);

        movie.setActorRoles(Arrays.asList(actorRole));
        movieRepository.save(movie);

        inTransaction(() -> {
            Movie savedMovie = movieRepository.findById(movie.getId()).get();
            Assert.assertEquals(1, savedMovie.getActorRoles().size());

            ActorRole savedActorRole = actorRoleRepository.findById(actorRole.getId()).get();
            Assert.assertEquals(savedMovie, savedActorRole.getMovie());
        });
    }

    @Test
    public void testCreateMovieReviewsAndLink() {
        Movie movie = movieRepository.save(new Movie());

        MovieReview movieReview = new MovieReview();
        RegisteredUser creator = registeredUserRepository.save(new RegisteredUser());
        movieReview.setCreator(creator);
        movieReview.setMovie(movie);
        movieReviewRepository.save(movieReview);

        movie.setMovieReviews(Arrays.asList(movieReview));
        movieRepository.save(movie);

        inTransaction(() -> {
            Movie savedMovie = movieRepository.findById(movie.getId()).get();
            Assert.assertEquals(1, savedMovie.getMovieReviews().size());

            MovieReview savedMovieReview = movieReviewRepository.findById(movieReview.getId()).get();
            Assert.assertEquals(savedMovie, savedMovieReview.getMovie());
        });
    }

    @Test
    public void testLinkGenres() {
        Movie movie = movieRepository.save(new Movie());
        Genre genre = genreRepository.save(new Genre());

        genre.setMovies(Arrays.asList(movie));
        genreRepository.save(genre);

        movie.setGenres(Arrays.asList(genre));
        movieRepository.save(movie);

        inTransaction(() -> {
            Movie savedMovie = movieRepository.findById(movie.getId()).get();
            Assert.assertEquals(1, savedMovie.getGenres().size());

            Genre savedGenre = genreRepository.findById(genre.getId()).get();
            Assert.assertEquals(1, savedGenre.getMovies().size());
        });
    }

    @Test
    public void testCreateGenresAndLink() {
        Movie movie = movieRepository.save(new Movie());
        Genre genre = new Genre();

        movie.setGenres(Arrays.asList(genre));
        movieRepository.save(movie);

        inTransaction(() -> {
            Movie savedMovie = movieRepository.findById(movie.getId()).get();
            Assert.assertEquals(1, savedMovie.getGenres().size());

            Genre savedGenre = genreRepository.findById(savedMovie.getGenres().get(0).getId()).get();
            Assert.assertEquals(1, savedGenre.getMovies().size());
        });
    }

    @Test
    public void testLinkProductionCompanies() {
        Movie movie = movieRepository.save(new Movie());
        ProductionCompany company = productionCompanyRepository.save(new ProductionCompany());

        company.setMovies(Arrays.asList(movie));
        productionCompanyRepository.save(company);

        movie.setProductionCompanies(Arrays.asList(company));
        movieRepository.save(movie);

        inTransaction(() -> {
            Movie savedMovie = movieRepository.findById(movie.getId()).get();
            Assert.assertEquals(1, savedMovie.getProductionCompanies().size());

            ProductionCompany savedCompany = productionCompanyRepository.findById(company.getId()).get();
            Assert.assertEquals(1, savedCompany.getMovies().size());
        });
    }

    @Test
    public void testCreateProductionCompaniesAndLink() {
        Movie movie = movieRepository.save(new Movie());
        ProductionCompany company = new ProductionCompany();

        movie.setProductionCompanies(Arrays.asList(company));
        movieRepository.save(movie);

        inTransaction(() -> {
            Movie savedMovie = movieRepository.findById(movie.getId()).get();
            Assert.assertEquals(1, savedMovie.getProductionCompanies().size());

            ProductionCompany savedCompany = productionCompanyRepository.findById(savedMovie.getProductionCompanies()
                    .get(0)
                    .getId()).get();
            Assert.assertEquals(1, savedCompany.getMovies().size());
        });
    }

    @Test
    public void testLinkNews() {
        Movie movie = movieRepository.save(new Movie());
        News news = newsRepository.save(new News());

        news.setMovies(Arrays.asList(movie));
        newsRepository.save(news);

        movie.setNews(Arrays.asList(news));
        movieRepository.save(movie);

        inTransaction(() -> {
            Movie savedMovie = movieRepository.findById(movie.getId()).get();
            Assert.assertEquals(1, savedMovie.getNews().size());

            News savedNews = newsRepository.findById(news.getId()).get();
            Assert.assertEquals(1, savedNews.getMovies().size());
        });
    }

    @Test
    public void testCreateNewsAndLink() {
        Movie movie = movieRepository.save(new Movie());
        News news = new News();

        movie.setNews(Arrays.asList(news));
        movieRepository.save(movie);

        inTransaction(() -> {
            Movie savedMovie = movieRepository.findById(movie.getId()).get();
            Assert.assertEquals(1, savedMovie.getNews().size());

            News savedNews = newsRepository.findById(savedMovie.getNews().get(0).getId()).get();
            Assert.assertEquals(1, savedNews.getMovies().size());
        });
    }

}
