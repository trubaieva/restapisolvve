package org.solvve.restapisolvve.base.dto.actorrole;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class ActorRoleCreateDTO {
    private String character;
    private Integer castOrder;

    @NotNull
    private UUID actorId;
}
