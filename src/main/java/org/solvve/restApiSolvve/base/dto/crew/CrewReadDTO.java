package org.solvve.restapisolvve.base.dto.crew;

import lombok.Data;
import org.solvve.restapisolvve.base.domain.CrewDepartment;

import java.time.Instant;
import java.util.UUID;

@Data
public class CrewReadDTO {
    private UUID id;
    private UUID personId;
    private CrewDepartment department;
    private String job;
    private UUID movieId;
    private Instant createdAt;
    private Instant updatedAt;
}
