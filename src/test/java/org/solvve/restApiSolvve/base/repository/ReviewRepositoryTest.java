package org.solvve.restapisolvve.base.repository;

import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.Review;
import org.solvve.restapisolvve.base.util.EntityCreator;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;

import static org.junit.Assert.*;

public class ReviewRepositoryTest extends BaseTest {

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private EntityCreator entityCreator;

    @Test
    public void testCreatedAtIsSet() {
        Review review = entityCreator.createReview();

        Instant createdAtBeforeReload = review.getCreatedAt();
        assertNotNull(createdAtBeforeReload);
        review = reviewRepository.findById(review.getId()).get();

        Instant createdAtAfterReload = review.getCreatedAt();
        assertNotNull(createdAtAfterReload);
        assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        Review review = entityCreator.createReview();

        Instant updatedAtBeforeReload = review.getUpdatedAt();
        assertNotNull(updatedAtBeforeReload);
        review = reviewRepository.findById(review.getId()).get();

        Instant updatedAtAfterReload = review.getUpdatedAt();
        assertNotNull(updatedAtAfterReload);
        assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        review.setTextReview("text");
        reviewRepository.save(review);
        review = reviewRepository.findById(review.getId()).get();
        Instant updatedAtAfterModificationAndReload = review.getUpdatedAt();
        assertTrue(updatedAtAfterModificationAndReload.isAfter(updatedAtAfterReload));
    }

}
