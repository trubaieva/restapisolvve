package org.solvve.restapisolvve.base.controller;

import org.solvve.restapisolvve.base.controller.documentation.ApiPageable;
import org.solvve.restapisolvve.base.dto.PageResult;
import org.solvve.restapisolvve.base.dto.requesttocorrect.RequestToCorrectCreateDTO;
import org.solvve.restapisolvve.base.dto.requesttocorrect.RequestToCorrectFilter;
import org.solvve.restapisolvve.base.dto.requesttocorrect.RequestToCorrectFixDTO;
import org.solvve.restapisolvve.base.dto.requesttocorrect.RequestToCorrectReadDTO;
import org.solvve.restapisolvve.base.service.RequestToCorrectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class RequestToCorrectController {

    @Autowired
    private RequestToCorrectService requestToCorrectService;

    @ApiPageable
    @PreAuthorize("hasAnyAuthority('ADMIN', 'CONTENT_MANAGER')")
    @GetMapping("/requests-to-correct")
    public PageResult<RequestToCorrectReadDTO> getRequestsToCorrect(RequestToCorrectFilter filter, Pageable pageable) {
        return requestToCorrectService.getRequestsToCorrect(filter, pageable);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'CONTENT_MANAGER')")
    @GetMapping("/requests-to-correct/{id}")
    public RequestToCorrectReadDTO getRequestToCorrect(@PathVariable UUID id) {
        return requestToCorrectService.getRequestToCorrect(id);
    }

    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('CONTENT_MANAGER') or (hasAuthority('REGISTERED_USER') and "
            + "authentication.principal.id == #id)")
    @PostMapping("/users/{id}/requests-to-correct")
    public RequestToCorrectReadDTO createRequestToCorrect(@PathVariable UUID id,
            @RequestBody @Valid RequestToCorrectCreateDTO createDTO) {
        return requestToCorrectService.createRequestToCorrect(id, createDTO);
    }

    @PreAuthorize("hasAuthority('ADMIN') or (hasAuthority('CONTENT_MANAGER') and authentication.principal.id == #cmId)")
    @PostMapping("/content-managers/{cmId}/requests-to-correct/{requestId}/fix")
    public RequestToCorrectReadDTO fixRequestToCorrect(@PathVariable UUID cmId, @PathVariable UUID requestId,
            @RequestBody RequestToCorrectFixDTO fixDTO) {
        return requestToCorrectService.fixRequestToCorrect(cmId, requestId, fixDTO);
    }

    @PreAuthorize("hasAuthority('ADMIN') or (hasAuthority('CONTENT_MANAGER') and authentication.principal.id == #cmId)")
    @PostMapping("/content-managers/{cmId}/requests-to-correct/{requestId}/cancel")
    public RequestToCorrectReadDTO cancelRequestToCorrect(@PathVariable UUID cmId, @PathVariable UUID requestId) {
        return requestToCorrectService.cancelRequestToCorrect(cmId, requestId);
    }
}
