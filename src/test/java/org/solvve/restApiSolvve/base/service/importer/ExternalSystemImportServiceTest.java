package org.solvve.restapisolvve.base.service.importer;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.*;
import org.solvve.restapisolvve.base.exception.ImportAlreadyPerformedException;
import org.solvve.restapisolvve.base.repository.ExternalSystemImportRepository;
import org.solvve.restapisolvve.base.repository.MovieRepository;
import org.solvve.restapisolvve.base.util.EntityCreator;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

import static org.junit.Assert.assertFalse;

public class ExternalSystemImportServiceTest extends BaseTest {

    @Autowired
    EntityCreator entityCreator;
    @Autowired
    MovieRepository movieRepository;
    @Autowired
    private ExternalSystemImportService externalSystemImportService;
    @Autowired
    private ExternalSystemImportRepository externalSystemImportRepository;

    @Test
    public void testValidateNotImportedFromExternalSystem() throws ImportAlreadyPerformedException {
        externalSystemImportService.validateNotImported(Movie.class, "some-id");
    }

    @Test
    public void testExceptionWhenExternalSystemImportAndEntityExist() {
        Movie movie = entityCreator.createMovie();
        ExternalSystemImport esi = generateFlatEntityWithoutId(ExternalSystemImport.class);
        esi.setEntityType(ImportedEntityType.MOVIE);
        esi.setEntityId(movie.getId());
        esi = externalSystemImportRepository.save(esi);
        String idInExternalSystem = esi.getIdInExternalSystem();

        ImportAlreadyPerformedException ex = Assertions.catchThrowableOfType(
                () -> externalSystemImportService.validateNotImported(Movie.class, idInExternalSystem),
                ImportAlreadyPerformedException.class);
        Assertions.assertThat(ex.getExternalSystemImport()).isEqualToComparingFieldByField(esi);
    }

    @Test
    public void testExceptionWhenExternalSystemImportExistsButEntityNotFound() throws ImportAlreadyPerformedException {
        ExternalSystemImport esi = generateFlatEntityWithoutId(ExternalSystemImport.class);
        esi.setEntityType(ImportedEntityType.PERSON);
        esi = externalSystemImportRepository.save(esi);

        externalSystemImportService.validateNotImported(Movie.class, esi.getIdInExternalSystem());
    }

    @Test
    public void testNoExceptionWhenAlreadyImportedButDifferentEntityType() throws ImportAlreadyPerformedException {
        Person person = entityCreator.createPerson();
        ExternalSystemImport esi = generateFlatEntityWithoutId(ExternalSystemImport.class);
        esi.setEntityType(ImportedEntityType.PERSON);
        esi.setEntityId(person.getId());
        esi = externalSystemImportRepository.save(esi);

        externalSystemImportService.validateNotImported(Movie.class, esi.getIdInExternalSystem());
    }

    @Test
    public void testCreateExternalSystemImport() throws ImportAlreadyPerformedException {
        Movie movie = entityCreator.createMovie();
        String idInExternalSystem = "id1";
        UUID importId = externalSystemImportService.createExternalSystemImport(movie, idInExternalSystem);
        Assert.assertNotNull(importId);
        ExternalSystemImport esi = externalSystemImportRepository.findById(importId).get();
        Assert.assertEquals(idInExternalSystem, esi.getIdInExternalSystem());
        Assert.assertEquals(ImportedEntityType.MOVIE, esi.getEntityType());
        Assert.assertEquals(movie.getId(), esi.getEntityId());
    }

    @Test
    public void testDeleteExistentExternalSystemImportBeforeCreatingNewNoEntityFoundInDb()
            throws ImportAlreadyPerformedException {
        Movie movie = entityCreator.createMovie();
        String idInExternalSystem = "id1";
        UUID importId = externalSystemImportService.createExternalSystemImport(movie, idInExternalSystem);
        Assert.assertNotNull(importId);
        ExternalSystemImport esi = externalSystemImportRepository.findById(importId).get();
        Assert.assertEquals(idInExternalSystem, esi.getIdInExternalSystem());
        Assert.assertEquals(ImportedEntityType.MOVIE, esi.getEntityType());
        Assert.assertEquals(movie.getId(), esi.getEntityId());

        movieRepository.delete(movie);

        UUID importId2 = externalSystemImportService.createExternalSystemImport(movie, idInExternalSystem);
        Assert.assertNotNull(importId2);
        ExternalSystemImport esi2 = externalSystemImportRepository.findById(importId2).get();
        Assert.assertEquals(idInExternalSystem, esi2.getIdInExternalSystem());
        Assert.assertEquals(ImportedEntityType.MOVIE, esi2.getEntityType());
        Assert.assertEquals(movie.getId(), esi2.getEntityId());

        assertFalse(externalSystemImportRepository.findById(importId).isPresent());
    }

    @Test
    public void testExceptionWhenExternalSystemImportAndEntityFoundWhileCreatingNew()
            throws ImportAlreadyPerformedException {
        Movie movie = entityCreator.createMovie();
        String idInExternalSystem = "id1";
        UUID importId = externalSystemImportService.createExternalSystemImport(movie, idInExternalSystem);
        Assert.assertNotNull(importId);
        ExternalSystemImport esi = externalSystemImportRepository.findById(importId).get();
        Assert.assertEquals(idInExternalSystem, esi.getIdInExternalSystem());
        Assert.assertEquals(ImportedEntityType.MOVIE, esi.getEntityType());
        Assert.assertEquals(movie.getId(), esi.getEntityId());

        ImportAlreadyPerformedException ex = Assertions.catchThrowableOfType(
                () -> externalSystemImportService.createExternalSystemImport(movie, idInExternalSystem),
                ImportAlreadyPerformedException.class);
        Assertions.assertThat(ex.getExternalSystemImport()).isEqualToComparingFieldByField(esi);
    }

    @Test
    public void testGetEntityIfAlreadyImported() {
        ExternalSystemImport esi = generateFlatEntityWithoutId(ExternalSystemImport.class);
        esi.setEntityType(ImportedEntityType.CAST);
        esi = externalSystemImportRepository.save(esi);
        String idInExternalSystem = esi.getIdInExternalSystem();
        UUID idInInternalSystem = externalSystemImportService.getEntityIfAlreadyImported(ActorRole.class,
                idInExternalSystem);
        Assert.assertEquals(esi.getEntityId(), idInInternalSystem);
    }

    @Test
    public void testGetEntityIfAlreadyImportedWhenWrongType() {
        ExternalSystemImport esi = generateFlatEntityWithoutId(ExternalSystemImport.class);
        esi.setEntityType(ImportedEntityType.CREW);
        esi = externalSystemImportRepository.save(esi);
        String idInExternalSystem = esi.getIdInExternalSystem();
        UUID idInInternalSystem = externalSystemImportService
                .getEntityIfAlreadyImported(ActorRole.class, idInExternalSystem);
        Assert.assertNull(idInInternalSystem);
    }
}
