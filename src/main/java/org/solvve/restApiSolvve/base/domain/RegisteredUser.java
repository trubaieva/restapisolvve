package org.solvve.restapisolvve.base.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
public class RegisteredUser extends User {

    @ManyToMany
    @JoinTable(name = "registered_user_genre", joinColumns = @JoinColumn(name = "registered_user_id"),
            inverseJoinColumns = @JoinColumn(name = "genre_id"))
    private List<Genre> prefferedGenres = new ArrayList<>();

    @ManyToMany
    @JoinTable(name = "registered_user_movie", joinColumns = @JoinColumn(name = "registered_user_id"),
            inverseJoinColumns = @JoinColumn(name = "movie_id"))
    private List<Movie> suggestedMovies = new ArrayList<>();

    private Boolean isBlocked;

    private Integer trustLevel;

    private Double reviewRating;

    private Double activityRating;

}
