package org.solvve.restapisolvve.base.repository;

import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.Like;
import org.solvve.restapisolvve.base.util.EntityCreator;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;

import static org.junit.Assert.*;

public class LikeRepositoryTest extends BaseTest {

    @Autowired
    private LikeRepository likeRepository;

    @Autowired
    private EntityCreator entityCreator;

    @Test
    public void testCreatedAtIsSet() {
        Like like = entityCreator.createLike();

        Instant createdAtBeforeReload = like.getCreatedAt();
        assertNotNull(createdAtBeforeReload);
        like = likeRepository.findById(like.getId()).get();

        Instant createdAtAfterReload = like.getCreatedAt();
        assertNotNull(createdAtAfterReload);
        assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        Like like = entityCreator.createLike();

        Instant updatedAtBeforeReload = like.getUpdatedAt();
        assertNotNull(updatedAtBeforeReload);
        like = likeRepository.findById(like.getId()).get();

        Instant updatedAtAfterReload = like.getUpdatedAt();
        assertNotNull(updatedAtAfterReload);
        assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        like.setCreator(entityCreator.createRegisteredUser());
        likeRepository.save(like);
        like = likeRepository.findById(like.getId()).get();
        Instant updatedAtAfterModificationAndReload = like.getUpdatedAt();
        assertTrue(updatedAtAfterModificationAndReload.isAfter(updatedAtAfterReload));
    }

}
