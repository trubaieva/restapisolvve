package org.solvve.restapisolvve.base.service;

import lombok.extern.slf4j.Slf4j;
import org.solvve.restapisolvve.base.domain.Movie;
import org.solvve.restapisolvve.base.dto.PageResult;
import org.solvve.restapisolvve.base.dto.movie.*;
import org.solvve.restapisolvve.base.repository.MovieRateRepository;
import org.solvve.restapisolvve.base.repository.MovieRepository;
import org.solvve.restapisolvve.base.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
@Slf4j
public class MovieService {

    @Autowired
    RepositoryHelper repositoryHelper;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private MovieRateRepository movieRateRepository;

    @Autowired
    private TranslationService translationService;

    public MovieReadDTO getMovie(UUID id) {
        Movie movie = repositoryHelper.getEntityIfExist(Movie.class, id);
        return translationService.translate(movie, MovieReadDTO.class);
    }

    @Transactional(readOnly = true)
    public MovieReadExtendedDTO getMovieExtended(UUID id) {
        Movie movie = repositoryHelper.getEntityIfExist(Movie.class, id);
        return translationService.translate(movie, MovieReadExtendedDTO.class);
    }

    public PageResult<MovieReadDTO> getMovies(MovieFilter filter, Pageable pageable) {
        Page<Movie> movies = movieRepository.findByFilter(filter, pageable);
        return translationService.toPageResult(movies, MovieReadDTO.class);
    }

    public List<MovieInLeaderBoardReadDTO> getMoviesLeaderBoard() {
        return movieRepository.getMoviesLeaderBoard();
    }

    public MovieReadDTO createMovie(MovieCreateDTO create) {
        Movie movie = translationService.translate(create, Movie.class);
        movie = movieRepository.save(movie);
        return translationService.translate(movie, MovieReadDTO.class);
    }

    public MovieReadDTO patchMovie(UUID id, MoviePatchDTO patchDTO) {
        Movie movie = repositoryHelper.getEntityIfExist(Movie.class, id);

        translationService.map(patchDTO, movie);

        movie = movieRepository.save(movie);
        return translationService.translate(movie, MovieReadDTO.class);
    }

    public MovieReadDTO updateMovie(UUID id, MoviePutDTO putDTO) {
        Movie movie = repositoryHelper.getEntityIfExist(Movie.class, id);

        translationService.map(putDTO, movie);

        movie = movieRepository.save(movie);
        return translationService.translate(movie, MovieReadDTO.class);
    }

    public void deleteMovie(UUID id) {
        movieRepository.delete(repositoryHelper.getEntityIfExist(Movie.class, id));
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateAverageRating(UUID movieId) {
        Double averageRate = movieRateRepository.calcAverageRateOfMovie(movieId);
        Movie movie = repositoryHelper.getEntityIfExist(Movie.class, movieId);
        log.info("Setting new avarage mark of movie: {}. Old value: {}, new value: {}", movieId, movie.getAvgRating(),
                averageRate);
        movie.setAvgRating(averageRate);
        movieRepository.save(movie);
    }
}
