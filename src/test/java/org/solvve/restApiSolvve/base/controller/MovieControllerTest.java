package org.solvve.restapisolvve.base.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.solvve.restapisolvve.base.domain.Movie;
import org.solvve.restapisolvve.base.dto.PageResult;
import org.solvve.restapisolvve.base.dto.movie.*;
import org.solvve.restapisolvve.base.exception.EntityNotFoundException;
import org.solvve.restapisolvve.base.exception.handler.ErrorInfo;
import org.solvve.restapisolvve.base.service.MovieService;
import org.solvve.restapisolvve.base.util.DtoCreator;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(controllers = MovieController.class)
public class MovieControllerTest extends BaseControllerTest {

    @MockBean
    private MovieService movieService;

    private DtoCreator dtoCreator = new DtoCreator();

    @Test
    public void testGetMovie() throws Exception {
        MovieReadDTO movie = dtoCreator.createMovieRead();

        Mockito.when(movieService.getMovie(movie.getId())).thenReturn(movie);

        String resultJson = mvc.perform(get("/api/v1/movies/{id}", movie.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MovieReadDTO actualMovie = objectMapper.readValue(resultJson, MovieReadDTO.class);
        Assertions.assertThat(actualMovie).isEqualToComparingFieldByField(movie);

        Mockito.verify(movieService).getMovie(movie.getId());
    }

    @Test
    public void testGetMovieExtended() throws Exception {
        MovieReadExtendedDTO movie = dtoCreator.createMovieReadExtended();

        Mockito.when(movieService.getMovieExtended(movie.getId())).thenReturn(movie);

        String resultJson = mvc.perform(get("/api/v1/movies/{id}/extended", movie.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MovieReadExtendedDTO actualMovie = objectMapper.readValue(resultJson, MovieReadExtendedDTO.class);
        Assertions.assertThat(actualMovie).isEqualToComparingFieldByField(movie);

        Mockito.verify(movieService).getMovieExtended(movie.getId());
    }

    @Test
    public void testGetMovieWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(Movie.class, wrongId);
        Mockito.when(movieService.getMovie(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/movies/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetMovieWrongFormatId() throws Exception {
        String wrongFormatId = "123";
        ErrorInfo expectedError = new ErrorInfo(HttpStatus.BAD_REQUEST,
                MethodArgumentTypeMismatchException.class,
                "Failed to convert value of type 'java.lang.String' to required type " +
                        "'java.util.UUID'; nested exception is java.lang" +
                        ".IllegalArgumentException: Invalid UUID string: 123"
        );
        String resultJson = mvc.perform(get("/api/v1/movies/{id}", wrongFormatId))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        ErrorInfo actualError = objectMapper.readValue(resultJson, ErrorInfo.class);
        Assertions.assertThat(actualError).isEqualToComparingFieldByField(expectedError);
    }

    @Test
    public void testCreateMovie() throws Exception {
        MovieCreateDTO create = dtoCreator.createMovieCreateDTO();

        MovieReadDTO read = dtoCreator.createMovieRead();

        Mockito.when(movieService.createMovie(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/movies")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MovieReadDTO actualMovie = objectMapper.readValue(resultJson, MovieReadDTO.class);
        Assertions.assertThat(actualMovie).isEqualToComparingFieldByField(read);

        Mockito.verify(movieService).createMovie(create);
    }

    @Test
    public void testPatchMovie() throws Exception {
        MoviePatchDTO patchDTO = dtoCreator.createMoviePatchDTO();

        MovieReadDTO read = dtoCreator.createMovieRead();

        Mockito.when(movieService.patchMovie(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/movies/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MovieReadDTO actualMovie = objectMapper.readValue(resultJson, MovieReadDTO.class);
        Assert.assertEquals(read, actualMovie);

        Mockito.verify(movieService).patchMovie(read.getId(), patchDTO);
    }

    @Test
    public void testUpdateMovie() throws Exception {
        MoviePutDTO putDTO = dtoCreator.createMoviePutDTO();

        MovieReadDTO read = dtoCreator.createMovieRead();

        Mockito.when(movieService.updateMovie(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/movies/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MovieReadDTO actualMovie = objectMapper.readValue(resultJson, MovieReadDTO.class);
        Assert.assertEquals(read, actualMovie);

        Mockito.verify(movieService).updateMovie(read.getId(), putDTO);
    }

    @Test
    public void testDeleteMovie() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/movies/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(movieService).deleteMovie(id);
    }

    @Test
    public void testGetMovies() throws Exception {
        MovieFilter movieFilter = dtoCreator.createMovieFilter();

        List<MovieReadDTO> movieReadDTOs = new ArrayList<>();
        MovieReadDTO movieReadDTO;
        for (int i = 0; i < 5; i++) {
            movieReadDTO = dtoCreator.createMovieRead();
            movieReadDTOs.add(movieReadDTO);
        }

        PageResult<MovieReadDTO> resultPage = new PageResult<>();
        resultPage.setData(movieReadDTOs);

        Mockito.when(movieService.getMovies(movieFilter, PageRequest.of(0, defaultPageSize))).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/movies")
                .param("movieName", movieFilter.getMovieName())
                .param("country", movieFilter.getCountry())
                .param("releaseDate", movieFilter.getReleaseDate().toString())
                .param("avgRating", movieFilter.getAvgRating().toString())
                .param("genres", movieFilter.getGenres().stream()
                        .collect(Collectors.joining(",")))
                .param("actorName", movieFilter.getActorName())
                .param("company", movieFilter.getCompany()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PageResult<MovieReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);

        Mockito.verify(movieService).getMovies(movieFilter, PageRequest.of(0, defaultPageSize));
    }

    @Test
    public void testGetMoviesWithPagingAndSorting() throws Exception {
        MovieFilter movieFilter = new MovieFilter();

        MovieReadDTO movieReadDTO = dtoCreator.createMovieRead();

        int page = 1;
        int size = 25;

        PageResult<MovieReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(page);
        resultPage.setPageSize(size);
        resultPage.setTotalElements(100);
        resultPage.setTotalPages(4);
        resultPage.setData(List.of(movieReadDTO));

        PageRequest pageRequest = PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "releaseDate"));
        Mockito.when(movieService.getMovies(movieFilter, pageRequest)).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/movies")
                .param("page", Integer.toString(page))
                .param("size", Integer.toString(size))
                .param("sort", "releaseDate,desc"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PageResult<MovieReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);

        Mockito.verify(movieService).getMovies(movieFilter, pageRequest);
    }

    @Test
    public void testGetMoviesWithBigPage() throws Exception {
        MovieFilter movieFilter = new MovieFilter();
        MovieReadDTO movieReadDTO = dtoCreator.createMovieRead();

        int page = 0;
        int size = 99999;

        PageResult<MovieReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(page);
        resultPage.setPageSize(size);
        resultPage.setTotalElements(100);
        resultPage.setTotalPages(4);
        resultPage.setData(List.of(movieReadDTO));

        PageRequest pageRequest = PageRequest.of(page, maxPageSize);
        Mockito.when(movieService.getMovies(movieFilter, pageRequest)).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/movies")
                .param("page", Integer.toString(page))
                .param("size", Integer.toString(size)))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PageResult<MovieReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);

        Mockito.verify(movieService).getMovies(movieFilter, pageRequest);
    }
}