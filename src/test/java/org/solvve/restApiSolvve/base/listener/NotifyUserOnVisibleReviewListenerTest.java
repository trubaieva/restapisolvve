package org.solvve.restapisolvve.base.listener;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.solvve.restapisolvve.base.event.ReviewVisibilityChangedEvent;
import org.solvve.restapisolvve.base.event.listener.NotifyUserOnVisibleReviewListener;
import org.solvve.restapisolvve.base.service.UserNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;

import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class NotifyUserOnVisibleReviewListenerTest {

    @MockBean
    private UserNotificationService userNotificationService;

    @SpyBean
    private NotifyUserOnVisibleReviewListener notifyUserOnVisibleReviewListener;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @Test
    public void testOnEvent() {
        ReviewVisibilityChangedEvent event = new ReviewVisibilityChangedEvent();
        event.setReviewId(UUID.randomUUID());
        event.setIsVisible(true);
        applicationEventPublisher.publishEvent(event);

        Mockito.verify(notifyUserOnVisibleReviewListener, Mockito.timeout(500)).onEvent(event);
        Mockito.verify(userNotificationService, Mockito.timeout(500)).notifyOnReviewBecomeVisible(event.getReviewId());
    }

    @Test
    public void testOnEventNotVisible() {
        ReviewVisibilityChangedEvent event = new ReviewVisibilityChangedEvent();
        event.setReviewId(UUID.randomUUID());
        event.setIsVisible(false);
        applicationEventPublisher.publishEvent(event);

        Mockito.verify(notifyUserOnVisibleReviewListener, Mockito.never()).onEvent(any());
        Mockito.verify(userNotificationService, Mockito.never()).notifyOnReviewBecomeVisible(any());
    }

    @Test
    public void testOnEventAsync() throws InterruptedException {
        ReviewVisibilityChangedEvent event = new ReviewVisibilityChangedEvent();
        event.setReviewId(UUID.randomUUID());
        event.setIsVisible(true);

        List<Integer> check = new ArrayList<>();
        CountDownLatch latch = new CountDownLatch(1);
        Mockito.doAnswer(invocationOnMock -> {
            Thread.sleep(500);
            check.add(2);
            latch.countDown();
            return null;
        }).when(userNotificationService).notifyOnReviewBecomeVisible(event.getReviewId());

        applicationEventPublisher.publishEvent(event);
        check.add(1);

        latch.await();
        Mockito.verify(notifyUserOnVisibleReviewListener).onEvent(event);
        Mockito.verify(userNotificationService).notifyOnReviewBecomeVisible(event.getReviewId());
        Assert.assertEquals(Arrays.asList(1, 2), check);
    }
}
