package org.solvve.restapisolvve.base.repository;

import org.assertj.core.api.Assertions;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.solvve.restapisolvve.base.domain.CorrectStatus;
import org.solvve.restapisolvve.base.domain.ObjectToCorrect;
import org.solvve.restapisolvve.base.domain.RequestToCorrect;
import org.solvve.restapisolvve.base.dto.requesttocorrect.RequestToCorrectFilter;
import org.solvve.restapisolvve.base.util.EntityCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.solvve.restapisolvve.base.domain.CorrectStatus.CANCEL;
import static org.solvve.restapisolvve.base.domain.CorrectStatus.FIXED;
import static org.solvve.restapisolvve.base.domain.ObjectToCorrect.*;

@ActiveProfiles("test")
@RunWith(Parameterized.class)
@SpringBootTest
@Sql(statements = {"delete from movie_news", "delete from person_news", "delete from news",
        "delete from request_to_correct", "delete from content_manager", "delete from registered_user",
        "delete from app_user", "delete from movie", "delete from person"},
        executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class RequestToCorrectFilterRepositoryParameterizedTest {

    @ClassRule
    public static final SpringClassRule SPRING_CLASS_RULE = new SpringClassRule();

    @Rule
    public final SpringMethodRule springMethodRule = new SpringMethodRule();

    @Autowired
    private RequestToCorrectRepository requestToCorrectRepository;

    @Autowired
    private EntityCreator entityCreator;

    private String movieName;
    private String personName;
    private Set<ObjectToCorrect> objectTypes;
    private CorrectStatus status;
    private String wrongData;
    private List<Integer> expectedNumberRequests;

    public RequestToCorrectFilterRepositoryParameterizedTest(String movieName, String personName,
            Set<ObjectToCorrect> objectTypes,
            CorrectStatus status, String wrongData, List<Integer> expectedNumberRequests) {
        this.movieName = movieName;
        this.personName = personName;
        this.objectTypes = objectTypes;
        this.status = status;
        this.wrongData = wrongData;
        this.expectedNumberRequests = expectedNumberRequests;
    }

    @Parameterized.Parameters(name = "{index}: filter - ({0}, {1}, {2}, {3}, {4})")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                        {null, null, null, null, null, List.of(0, 1, 2, 3, 4, 5)},
                        {"Cinderella", null, null, null, null, List.of(0, 1, 5)},
                        {null, "Lily James", null, null, null, List.of(2, 3, 5)},
                        {null, null, Set.of(MOVIE, PERSON), null, null, List.of(0, 2)},
                        {null, null, null, CANCEL, null, List.of(2, 4)},
                        {null, null, null, null, "Lily", List.of(2, 4)},
                        {"Cinderella", "Lily James", Set.of(NEWS), FIXED, "Alla", List.of(5)},
                        {"Cinderella", null, Set.of(MOVIE), null, null, List.of(0)},
                        {"Cinderella", null, Set.of(NEWS), null, null, List.of(1, 5)},
                        //Could not locate named parameter [movieName], expecting one of [objectTypes]
                        {"Cinderella", null, Set.of(PERSON), null, null, List.of()},
                        {null, "Lily James", Set.of(MOVIE, PERSON), null, null, List.of(2)},
                        {null, "Lily James", Set.of(MOVIE, PERSON, NEWS), null, null, List.of(2, 3, 5)},
                        {"Test", null, Set.of(MOVIE), null, null, List.of()}
                }
        );
    }

    @Test
    public void testGetMoviesByFilters() {
        List<RequestToCorrect> requests = createTestRequests();

        RequestToCorrectFilter filter = new RequestToCorrectFilter(movieName, personName, objectTypes, status,
                wrongData);
        Page<RequestToCorrect> actualRequests = requestToCorrectRepository.findByFilter(filter, Pageable.unpaged());
        List<RequestToCorrect> expectedRequests =
                expectedNumberRequests.stream().map(requests::get).collect(Collectors.toList());
        Assertions.assertThat(actualRequests).extracting("id")
                .containsExactlyInAnyOrderElementsOf(expectedRequests.stream().map(RequestToCorrect::getId)
                        .collect(Collectors.toList()));
    }

    private List<RequestToCorrect> createTestRequests() {
        return List.of(entityCreator.createRequestToCorrectMovie(),
                entityCreator.createRequestToCorrectNewsMovie(),
                entityCreator.createRequestToCorrectPerson(),
                entityCreator.createRequestToCorrectNewsPerson(),
                entityCreator.createRequestToCorrectNews(),
                entityCreator.createRequestToCorrectNewsMoviePerson());
    }

}
