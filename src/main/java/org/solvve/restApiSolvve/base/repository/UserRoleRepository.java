package org.solvve.restapisolvve.base.repository;

import org.solvve.restapisolvve.base.domain.UserRole;
import org.solvve.restapisolvve.base.domain.UserRoleType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface UserRoleRepository extends CrudRepository<UserRole, UUID> {

    @Query("select ur from UserRole ur join ur.users u where u.id = (:userId)")
    List<UserRole> findAllByUserId(UUID userId);

    @Query("select ur.id from UserRole ur where ur.type = :userRoleType")
    UUID findUserRoleIdByType(UserRoleType userRoleType);

    //@Query("select ur.id from UserRole ur where ur.type = :userRoleType")
    UserRole findUserRoleByType(UserRoleType userRoleType);
}
