package org.solvve.restapisolvve.base.dto.person;

import lombok.Data;
import org.solvve.restapisolvve.base.domain.CrewDepartment;
import org.solvve.restapisolvve.base.domain.GenderType;

import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;

@Data
public class PersonReadDTO {
    private UUID id;
    private String name;
    private String biography;
    private LocalDate birthDate;
    private String birthPlace;
    private Instant createdAt;
    private Instant updatedAt;
    private GenderType gender;
    private LocalDate deathDate;
    private CrewDepartment knownForDepartment;
}
