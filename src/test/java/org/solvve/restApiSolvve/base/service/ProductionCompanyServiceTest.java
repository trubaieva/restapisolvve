package org.solvve.restapisolvve.base.service;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.Movie;
import org.solvve.restapisolvve.base.domain.ProductionCompany;
import org.solvve.restapisolvve.base.dto.productioncompany.ProductionCompanyCreateDTO;
import org.solvve.restapisolvve.base.dto.productioncompany.ProductionCompanyPutDTO;
import org.solvve.restapisolvve.base.dto.productioncompany.ProductionCompanyReadDTO;
import org.solvve.restapisolvve.base.exception.EntityNotFoundException;
import org.solvve.restapisolvve.base.exception.LinkDuplicatedException;
import org.solvve.restapisolvve.base.repository.MovieRepository;
import org.solvve.restapisolvve.base.repository.ProductionCompanyRepository;
import org.solvve.restapisolvve.base.util.EntityCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ProductionCompanyServiceTest extends BaseTest {

    @Autowired
    EntityCreator entityCreator;

    @Autowired
    private ProductionCompanyRepository productionCompanyRepository;

    @Autowired
    private ProductionCompanyService productionCompanyService;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private TranslationService translationService;

    @Test
    public void testGetProductionCompany() {
        ProductionCompany productionCompany = entityCreator.createProductionCompany();

        ProductionCompanyReadDTO readDTO = productionCompanyService.getProductionCompany(productionCompany.getId());
        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(productionCompany);
    }

    @Test
    public void testGetProductionCompanies() {
        List<ProductionCompany> productionCompanies = new ArrayList<>();
        ProductionCompany productionCompany;
        for (int i = 0; i < 5; i++) {
            productionCompany = entityCreator.createProductionCompany();
            productionCompanyRepository.save(productionCompany);
            productionCompanies.add(productionCompany);
        }
        List<ProductionCompanyReadDTO> readDTOs = productionCompanyService.getProductionCompanies();
        Assertions.assertThat(readDTOs)
                .extracting("id")
                .containsExactlyInAnyOrder(productionCompanies.stream().map(ProductionCompany::getId).toArray());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetProductionCompanyWrongId() {
        productionCompanyService.getProductionCompany(UUID.randomUUID());
    }

    @Test
    public void testCreateProductionCompany() {
        ProductionCompanyCreateDTO create = generator.generateRandomObject(ProductionCompanyCreateDTO.class);

        ProductionCompanyReadDTO read = productionCompanyService.createProductionCompany(create);
        Assertions.assertThat(create).isEqualToIgnoringGivenFields(read);
        Assert.assertNotNull(read.getId());

        ProductionCompany productionCompany = productionCompanyRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(productionCompany);
    }

    @Test
    public void testPutProductionCompany() {
        ProductionCompany productionCompany = entityCreator.createProductionCompany();
        productionCompanyRepository.save(productionCompany);

        ProductionCompanyPutDTO putDTO = generator.generateRandomObject(ProductionCompanyPutDTO.class);
        ProductionCompanyReadDTO read = productionCompanyService.updateProductionCompany(productionCompany.getId(),
                putDTO);

        Assertions.assertThat(putDTO).isEqualToIgnoringGivenFields(read);

        productionCompany = productionCompanyRepository.findById(read.getId()).get();
        Assertions.assertThat(productionCompany).isEqualToIgnoringGivenFields(read, "movies");
    }

    @Test
    public void testUpdateProductionCompanyEmptyPut() {
        ProductionCompany productionCompany = entityCreator.createProductionCompany();
        productionCompanyRepository.save(productionCompany);

        ProductionCompanyPutDTO putDTO = new ProductionCompanyPutDTO();
        ProductionCompanyReadDTO read = productionCompanyService.updateProductionCompany(productionCompany.getId(),
                putDTO);

        Assertions.assertThat(read).hasAllNullFieldsOrPropertiesExcept("id", "createdAt", "updatedAt");

        ProductionCompany productionCompanyAfterUpdate = productionCompanyRepository.findById(read.getId()).get();

        Assertions.assertThat(productionCompanyAfterUpdate).hasAllNullFieldsOrPropertiesExcept("id", "movies",
                "createdAt",
                "updatedAt");
    }

    @Test
    public void testDeleteProductionCompany() {
        ProductionCompany productionCompany = entityCreator.createProductionCompany();
        productionCompanyRepository.save(productionCompany);

        productionCompanyService.deleteProductionCompany(productionCompany.getId());
        Assert.assertFalse(productionCompanyRepository.existsById(productionCompany.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteProductionCompanyNotFound() {
        productionCompanyService.deleteProductionCompany(UUID.randomUUID());
    }

    @Test
    public void testAddProductionCompanyToMovie() {
        Movie movie = entityCreator.createMovie();
        ProductionCompany productionCompany = entityCreator.createProductionCompany();

        List<ProductionCompanyReadDTO> res = productionCompanyService.addProductionCompanyToMovie(movie.getId(),
                productionCompany.getId());

        ProductionCompanyReadDTO expectedRead = translationService.translate(productionCompany,
                ProductionCompanyReadDTO.class);
        Assertions.assertThat(res).containsExactlyInAnyOrder(expectedRead);

        transactionTemplate.executeWithoutResult(status -> {
            Movie movieAfterSave = movieRepository.findById(movie.getId()).get();
            Assertions.assertThat(movieAfterSave.getProductionCompanies()).extracting((ProductionCompany::getId))
                    .containsExactlyInAnyOrder(productionCompany.getId());

            ProductionCompany productionCompanyAfterSave = productionCompanyRepository.findById(
                    productionCompany.getId()).get();
            Assertions.assertThat(productionCompanyAfterSave.getMovies()).extracting((Movie::getId))
                    .containsExactlyInAnyOrder(movie.getId());
        });
    }

    @Test
    public void testAddProductionCompanyToMovieWithProductionCompany() {
        Movie movie = entityCreator.createMovie();
        ProductionCompany productionCompany1 = entityCreator.createProductionCompany();
        ProductionCompany productionCompany2 = entityCreator.createProductionCompany();

        productionCompanyService.addProductionCompanyToMovie(movie.getId(), productionCompany1.getId());
        List<ProductionCompanyReadDTO> res2 = productionCompanyService.addProductionCompanyToMovie(movie.getId(),
                productionCompany2.getId());

        ProductionCompanyReadDTO expectedRead1 = translationService.translate(productionCompany1,
                ProductionCompanyReadDTO.class);
        ProductionCompanyReadDTO expectedRead2 = translationService.translate(productionCompany2,
                ProductionCompanyReadDTO.class);

        Assertions.assertThat(res2).containsExactlyInAnyOrder(expectedRead1, expectedRead2);

        transactionTemplate.executeWithoutResult(status -> {
            Movie movieAfterSave = movieRepository.findById(movie.getId()).get();
            Assertions.assertThat(movieAfterSave.getProductionCompanies()).extracting((ProductionCompany::getId))
                    .containsExactlyInAnyOrder(productionCompany1.getId(), productionCompany2.getId());

            ProductionCompany productionCompanyAfterSave1 = productionCompanyRepository.findById(
                    productionCompany1.getId()).get();
            Assertions.assertThat(productionCompanyAfterSave1.getMovies()).extracting((Movie::getId))
                    .containsExactlyInAnyOrder(movie.getId());

            ProductionCompany productionCompanyAfterSave2 = productionCompanyRepository.findById(
                    productionCompany1.getId()).get();
            Assertions.assertThat(productionCompanyAfterSave2.getMovies()).extracting((Movie::getId))
                    .containsExactlyInAnyOrder(movie.getId());
        });
    }

    @Test
    public void testDuplicatedProductionCompany() {
        Movie movie = entityCreator.createMovie();
        ProductionCompany productionCompany = entityCreator.createProductionCompany();

        productionCompanyService.addProductionCompanyToMovie(movie.getId(), productionCompany.getId());
        Assertions.assertThatThrownBy(() -> {
            productionCompanyService.addProductionCompanyToMovie(movie.getId(), productionCompany.getId());
        }).isInstanceOf(LinkDuplicatedException.class);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testWrongMovieId() {
        UUID wrongMovieId = UUID.randomUUID();
        ProductionCompany productionCompany = entityCreator.createProductionCompany();
        productionCompanyService.addProductionCompanyToMovie(wrongMovieId, productionCompany.getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testWrongProductionCompanyId() {
        Movie movie = entityCreator.createMovie();
        UUID wrongProductionCompanyId = UUID.randomUUID();
        productionCompanyService.addProductionCompanyToMovie(movie.getId(), wrongProductionCompanyId);
    }

    @Test
    public void testRemoveProductionCompanyFromMovie() {
        Movie movie = entityCreator.createMovie();
        ProductionCompany productionCompany = entityCreator.createProductionCompany();
        addProductionCompanyToMovie(movie.getId(), productionCompany.getId());

        List<ProductionCompanyReadDTO> remainingProductionCompanies = productionCompanyService
                .removeProductionCompanyFromMovie(movie.getId(), productionCompany.getId());
        Assert.assertTrue(remainingProductionCompanies.isEmpty());

        transactionTemplate.executeWithoutResult(status -> {
            Movie movieAfterRemove = movieRepository.findById(movie.getId()).get();
            Assert.assertTrue(movieAfterRemove.getProductionCompanies().isEmpty());

            ProductionCompany productionCompanyAfterSave = productionCompanyRepository.findById(
                    productionCompany.getId()).get();
            Assert.assertTrue(productionCompanyAfterSave.getMovies().isEmpty());
        });
    }

    @Test
    public void testRemoveProductionCompanyFromMovieWithSeveralProductionCompanies() {
        Movie movie = entityCreator.createMovie();
        ProductionCompany productionCompany1 = entityCreator.createProductionCompany();
        ProductionCompany productionCompany2 = entityCreator.createProductionCompany();

        addProductionCompanyToMovie(movie.getId(), productionCompany1.getId());
        addProductionCompanyToMovie(movie.getId(), productionCompany2.getId());

        ProductionCompanyReadDTO expectedRead2 = translationService.translate(productionCompany2,
                ProductionCompanyReadDTO.class);

        List<ProductionCompanyReadDTO> remainingProductionCompanies = productionCompanyService
                .removeProductionCompanyFromMovie(movie.getId(), productionCompany1.getId());

        Assertions.assertThat(remainingProductionCompanies).containsExactlyInAnyOrder(expectedRead2);

        transactionTemplate.executeWithoutResult(status -> {
            Movie movieAfterRemove = movieRepository.findById(movie.getId()).get();
            Assertions.assertThat(movieAfterRemove.getProductionCompanies()).extracting((ProductionCompany::getId))
                    .containsExactlyInAnyOrder(productionCompany2.getId());

            ProductionCompany productionCompanyAfterSave1 = productionCompanyRepository.findById(
                    productionCompany1.getId()).get();
            Assert.assertTrue(productionCompanyAfterSave1.getMovies().isEmpty());

            ProductionCompany productionCompanyAfterSave2 = productionCompanyRepository.findById(
                    productionCompany2.getId()).get();
            Assertions.assertThat(productionCompanyAfterSave2.getMovies()).extracting((Movie::getId))
                    .containsExactlyInAnyOrder(movie.getId());
        });
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotAddedProductionCompany() {
        Movie movie = entityCreator.createMovie();
        ProductionCompany productionCompany = entityCreator.createProductionCompany();

        productionCompanyService.removeProductionCompanyFromMovie(movie.getId(), productionCompany.getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotExistedProductionCompany() {
        Movie movie = entityCreator.createMovie();
        productionCompanyService.removeProductionCompanyFromMovie(movie.getId(), UUID.randomUUID());
    }

    private void addProductionCompanyToMovie(UUID movieId, UUID productionCompanyId) {
        transactionTemplate.executeWithoutResult(status -> {
            Movie movie = movieRepository.findById(movieId).get();
            ProductionCompany productionCompany = productionCompanyRepository.findById(productionCompanyId).get();
            movie.getProductionCompanies().add(productionCompany);
            movieRepository.save(movie);
        });
    }

}
