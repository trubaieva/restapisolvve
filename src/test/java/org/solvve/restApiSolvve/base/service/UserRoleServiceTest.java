package org.solvve.restapisolvve.base.service;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.User;
import org.solvve.restapisolvve.base.domain.UserRole;
import org.solvve.restapisolvve.base.domain.UserRoleType;
import org.solvve.restapisolvve.base.dto.userrole.UserRoleReadDTO;
import org.solvve.restapisolvve.base.exception.EntityNotFoundException;
import org.solvve.restapisolvve.base.exception.LinkDuplicatedException;
import org.solvve.restapisolvve.base.repository.UserRepository;
import org.solvve.restapisolvve.base.repository.UserRoleRepository;
import org.solvve.restapisolvve.base.util.DtoCreator;
import org.solvve.restapisolvve.base.util.EntityCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.List;
import java.util.UUID;

public class UserRoleServiceTest extends BaseTest {

    @Autowired
    EntityCreator entityCreator;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private UserRoleService userRoleService;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private DtoCreator dtoCreator;

    @Test
    public void testGetAllRoles() {
        UUID userRoleId1 = userRoleRepository.findUserRoleIdByType(UserRoleType.REGISTERED_USER);
        UUID userRoleId2 = userRoleRepository.findUserRoleIdByType(UserRoleType.MODERATOR);
        UUID userRoleId3 = userRoleRepository.findUserRoleIdByType(UserRoleType.ADMIN);
        UUID userRoleId4 = userRoleRepository.findUserRoleIdByType(UserRoleType.CONTENT_MANAGER);

        List<UserRoleReadDTO> res = userRoleService.getAllRoles();

        UserRoleReadDTO expectedRead1 = dtoCreator.createUserRoleRead(userRoleId1, UserRoleType.REGISTERED_USER);
        UserRoleReadDTO expectedRead2 = dtoCreator.createUserRoleRead(userRoleId2, UserRoleType.MODERATOR);
        UserRoleReadDTO expectedRead3 = dtoCreator.createUserRoleRead(userRoleId3, UserRoleType.ADMIN);
        UserRoleReadDTO expectedRead4 = dtoCreator.createUserRoleRead(userRoleId4, UserRoleType.CONTENT_MANAGER);

        Assertions.assertThat(res).containsExactlyInAnyOrder(expectedRead1, expectedRead2, expectedRead3,
                expectedRead4);
    }

    @Test
    public void testGetUserRoles() {

        User user = entityCreator.createUser();
        User user2 = entityCreator.createUser();

        UUID userRoleId1 = userRoleRepository.findUserRoleIdByType(UserRoleType.REGISTERED_USER);
        UUID userRoleId2 = userRoleRepository.findUserRoleIdByType(UserRoleType.MODERATOR);
        UUID userRoleId3 = userRoleRepository.findUserRoleIdByType(UserRoleType.ADMIN);

        addRoleToUser(user.getId(), userRoleId1);
        addRoleToUser(user.getId(), userRoleId2);
        addRoleToUser(user2.getId(), userRoleId2);
        addRoleToUser(user2.getId(), userRoleId3);

        List<UserRoleReadDTO> res = userRoleService.getUserRoles(user.getId());

        UserRoleReadDTO expectedRead1 = dtoCreator.createUserRoleRead(userRoleId1, UserRoleType.REGISTERED_USER);
        UserRoleReadDTO expectedRead2 = dtoCreator.createUserRoleRead(userRoleId2, UserRoleType.MODERATOR);

        Assertions.assertThat(res).containsExactlyInAnyOrder(expectedRead1, expectedRead2);

        transactionTemplate.executeWithoutResult(status -> {
            User userAfterSave = userRepository.findById(user.getId()).get();
            Assertions.assertThat(userAfterSave.getUserRoles()).extracting((UserRole::getId))
                    .containsExactlyInAnyOrder(userRoleId1, userRoleId2);
        });
    }

    @Test
    public void testAddRoleToUser() {
        User user = entityCreator.createUser();
        UUID userRoleId = userRoleRepository.findUserRoleIdByType(UserRoleType.REGISTERED_USER);

        List<UserRoleReadDTO> res = userRoleService.addRoleToUser(user.getId(), userRoleId);

        UserRoleReadDTO expectedRead = dtoCreator.createUserRoleRead(userRoleId, UserRoleType.REGISTERED_USER);
        Assertions.assertThat(res).containsExactlyInAnyOrder(expectedRead);

        transactionTemplate.executeWithoutResult(status -> {
            User userAfterSave = userRepository.findById(user.getId()).get();
            Assertions.assertThat(userAfterSave.getUserRoles()).extracting((UserRole::getId))
                    .containsExactlyInAnyOrder(userRoleId);
        });
    }

    @Test
    public void testAddRoleToUserWithRole() {
        User user = entityCreator.createUser();
        UUID userRoleId1 = userRoleRepository.findUserRoleIdByType(UserRoleType.REGISTERED_USER);
        UUID userRoleId2 = userRoleRepository.findUserRoleIdByType(UserRoleType.MODERATOR);

        userRoleService.addRoleToUser(user.getId(), userRoleId1);
        List<UserRoleReadDTO> res2 = userRoleService.addRoleToUser(user.getId(), userRoleId2);

        UserRoleReadDTO expectedRead1 = dtoCreator.createUserRoleRead(userRoleId1, UserRoleType.REGISTERED_USER);
        UserRoleReadDTO expectedRead2 = dtoCreator.createUserRoleRead(userRoleId2, UserRoleType.MODERATOR);

        Assertions.assertThat(res2).containsExactlyInAnyOrder(expectedRead1, expectedRead2);

        transactionTemplate.executeWithoutResult(status -> {
            User userAfterSave = userRepository.findById(user.getId()).get();
            Assertions.assertThat(userAfterSave.getUserRoles()).extracting((UserRole::getId))
                    .containsExactlyInAnyOrder(userRoleId1, userRoleId2);
        });
    }

    @Test
    public void testDuplicatedRole() {
        User user = entityCreator.createUser();
        UUID userRoleId = userRoleRepository.findUserRoleIdByType(UserRoleType.REGISTERED_USER);

        userRoleService.addRoleToUser(user.getId(), userRoleId);
        Assertions.assertThatThrownBy(() -> {
            userRoleService.addRoleToUser(user.getId(), userRoleId);
        }).isInstanceOf(LinkDuplicatedException.class);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testWrongUserId() {
        UUID wrongUserId = UUID.randomUUID();
        UUID userRoleId = userRoleRepository.findUserRoleIdByType(UserRoleType.REGISTERED_USER);
        userRoleService.addRoleToUser(wrongUserId, userRoleId);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testWrongUserRoleId() {
        User user = entityCreator.createUser();
        UUID wrongUserRoleId = UUID.randomUUID();
        userRoleService.addRoleToUser(user.getId(), wrongUserRoleId);
    }

    @Test
    public void testRemoveRoleFromUser() {
        User user = entityCreator.createUser();
        UUID userRoleId = userRoleRepository.findUserRoleIdByType(UserRoleType.REGISTERED_USER);
        addRoleToUser(user.getId(), userRoleId);

        List<UserRoleReadDTO> remainingRoles = userRoleService.removeRoleFromUser(user.getId(), userRoleId);
        Assert.assertTrue(remainingRoles.isEmpty());

        transactionTemplate.executeWithoutResult(status -> {
            User userAfterRemove = userRepository.findById(user.getId()).get();
            Assert.assertTrue(userAfterRemove.getUserRoles().isEmpty());
        });
    }

    @Test
    public void testRemoveRoleFromUserWithSeveralRoles() {
        User user = entityCreator.createUser();
        UUID userRoleId1 = userRoleRepository.findUserRoleIdByType(UserRoleType.REGISTERED_USER);
        UUID userRoleId2 = userRoleRepository.findUserRoleIdByType(UserRoleType.MODERATOR);
        addRoleToUser(user.getId(), userRoleId1);
        addRoleToUser(user.getId(), userRoleId2);

        UserRoleReadDTO expectedRead2 = dtoCreator.createUserRoleRead(userRoleId2, UserRoleType.MODERATOR);

        List<UserRoleReadDTO> remainingRoles = userRoleService.removeRoleFromUser(user.getId(), userRoleId1);

        Assertions.assertThat(remainingRoles).containsExactlyInAnyOrder(expectedRead2);

        transactionTemplate.executeWithoutResult(status -> {
            User userAfterRemove = userRepository.findById(user.getId()).get();
            Assertions.assertThat(userAfterRemove.getUserRoles()).extracting((UserRole::getId))
                    .containsExactlyInAnyOrder(userRoleId2);
        });
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotAddedRole() {
        User user = entityCreator.createUser();
        UUID userRoleId = userRoleRepository.findUserRoleIdByType(UserRoleType.REGISTERED_USER);

        userRoleService.removeRoleFromUser(user.getId(), userRoleId);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotExistedRole() {
        User user = entityCreator.createUser();
        userRoleService.removeRoleFromUser(user.getId(), UUID.randomUUID());
    }

    private void addRoleToUser(UUID userId, UUID userRoleId) {
        transactionTemplate.executeWithoutResult(status -> {
            User user = userRepository.findById(userId).get();
            UserRole userRole = userRoleRepository.findById(userRoleId).get();
            user.getUserRoles().add(userRole);
            userRepository.save(user);
        });
    }
}
