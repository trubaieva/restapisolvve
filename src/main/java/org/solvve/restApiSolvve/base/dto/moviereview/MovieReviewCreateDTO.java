package org.solvve.restapisolvve.base.dto.moviereview;

import lombok.Data;

import java.util.UUID;

@Data
public class MovieReviewCreateDTO {
    private String textReview;
    private UUID movieId;
}
