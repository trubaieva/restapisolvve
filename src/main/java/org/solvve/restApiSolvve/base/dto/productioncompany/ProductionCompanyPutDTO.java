package org.solvve.restapisolvve.base.dto.productioncompany;

import lombok.Data;

@Data
public class ProductionCompanyPutDTO {
    private String name;
}
