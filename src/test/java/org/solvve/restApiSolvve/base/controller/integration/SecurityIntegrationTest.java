package org.solvve.restapisolvve.base.controller.integration;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.postgresql.shaded.com.ongres.scram.common.bouncycastle.base64.Base64;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.*;
import org.solvve.restapisolvve.base.dto.PageResult;
import org.solvve.restapisolvve.base.dto.actorrole.ActorRolePutDTO;
import org.solvve.restapisolvve.base.dto.actorrole.ActorRoleReadDTO;
import org.solvve.restapisolvve.base.dto.movie.MovieReadDTO;
import org.solvve.restapisolvve.base.dto.requesttocorrect.RequestToCorrectCreateDTO;
import org.solvve.restapisolvve.base.dto.requesttocorrect.RequestToCorrectFixDTO;
import org.solvve.restapisolvve.base.dto.requesttocorrect.RequestToCorrectReadDTO;
import org.solvve.restapisolvve.base.dto.user.UserCreateDTO;
import org.solvve.restapisolvve.base.dto.user.UserReadDTO;
import org.solvve.restapisolvve.base.dto.userrole.UserRoleReadDTO;
import org.solvve.restapisolvve.base.repository.*;
import org.solvve.restapisolvve.base.util.EntityCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.UUID;

import static org.solvve.restapisolvve.base.domain.CorrectStatus.FIXED;
import static org.solvve.restapisolvve.base.domain.CorrectStatus.NEED_TO_VERIFY;

@ActiveProfiles({"test", "integration-test"})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class SecurityIntegrationTest extends BaseTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private EntityCreator entityCreator;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private RequestToCorrectRepository requestToCorrectRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private ContentManagerRepository contentManagerRepository;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private AdminRepository adminRepository;

    @Test
    public void testHealthNotSecurity() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Void> response = restTemplate.getForEntity("http://localhost:8080/health", Void.class);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetMoviesNoSecurity() {
        RestTemplate restTemplate = new RestTemplate();
        Assertions.assertThatThrownBy(() -> restTemplate.exchange(
                "http://localhost:8080/api/v1/movies", HttpMethod.GET, HttpEntity.EMPTY,
                new ParameterizedTypeReference<PageResult<MovieReadDTO>>() {
                }))
                .isInstanceOf(HttpClientErrorException.class).extracting("statusCode")
                .isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    @Test
    public void testGetMovies() {
        String email = "testt@gmail.com";
        String password = "pass123";

        createUser(email, password);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        ResponseEntity<PageResult<MovieReadDTO>> response = restTemplate.exchange(
                "http://localhost:8080/api/v1/movies", HttpMethod.GET, httpEntity,
                new ParameterizedTypeReference<>() {});
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetMoviesWrongPassword() {
        String email = "testt@gmail.com";
        String password = "pass123";

        createUser(email, password);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, "wrong pass"));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        Assertions.assertThatThrownBy(() -> restTemplate.exchange(
                "http://localhost:8080/api/v1/movies", HttpMethod.GET, httpEntity,
                new ParameterizedTypeReference<PageResult<MovieReadDTO>>() {
                }))
                .isInstanceOf(HttpClientErrorException.class).extracting("statusCode")
                .isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    @Test
    public void testGetMoviesWrongUser() {
        String email = "testt@gmail.com";
        String password = "pass123";

        createUser(email, password);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue("wrong user", password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        Assertions.assertThatThrownBy(() -> restTemplate.exchange(
                "http://localhost:8080/api/v1/movies", HttpMethod.GET, httpEntity,
                new ParameterizedTypeReference<PageResult<MovieReadDTO>>() {
                }))
                .isInstanceOf(HttpClientErrorException.class).extracting("statusCode")
                .isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    @Test
    public void testGetMoviesNoSession() {
        String email = "testt@gmail.com";
        String password = "pass123";

        createUser(email, password);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        ResponseEntity<PageResult<MovieReadDTO>> response = restTemplate.exchange(
                "http://localhost:8080/api/v1/movies", HttpMethod.GET, httpEntity,
                new ParameterizedTypeReference<>() {
                });
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertNull(response.getHeaders().get("Set-Cookie"));
    }

    @Test
    public void testCreateUserNotSecurity() {
        RestTemplate restTemplate = new RestTemplate();

        UserCreateDTO userCreateDTO = generator.generateRandomObject(UserCreateDTO.class);
        HttpEntity<UserCreateDTO> httpEntity = new HttpEntity<>(userCreateDTO);

        ResponseEntity<UserReadDTO> response = restTemplate.postForEntity("http://localhost:8080/api/v1/users",
                httpEntity,
                UserReadDTO.class);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetUserRolesNoRoles() {
        String email = "testt@gmail.com";
        String password = "pass123";

        User user = createUser(email, password);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        Assertions.assertThatThrownBy(() -> restTemplate.exchange(
                "http://localhost:8080/api/v1/users/" + user.getId() + "/roles", HttpMethod.GET, httpEntity,
                new ParameterizedTypeReference<List<UserRoleReadDTO>>() {}))
                .isInstanceOf(HttpClientErrorException.class).extracting("statusCode")
                .isEqualTo(HttpStatus.FORBIDDEN);
    }

    @Test
    public void testGetUserRolesAdmin() {
        String email = "test@gmail.com";
        String password = "pass123";

        User user = createUser(email, password, UserRoleType.ADMIN);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        ResponseEntity<List<UserRoleReadDTO>> response = restTemplate.exchange(
                "http://localhost:8080/api/v1/users/" + user.getId() + "/roles", HttpMethod.GET, httpEntity,
                new ParameterizedTypeReference<>() {});
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetUserRolesWrongUser() {
        String password = "pass123";

        User user1 = createUser("test1@gmail.com", "abc", UserRoleType.REGISTERED_USER);
        User user2 = createUser("test2@gmail.com", password, UserRoleType.REGISTERED_USER);
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(user2.getEmail(), password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        Assertions.assertThatThrownBy(() -> restTemplate.exchange(
                "http://localhost:8080/api/v1/users/" + user1.getId() + "/roles", HttpMethod.GET, httpEntity,
                new ParameterizedTypeReference<List<UserRoleReadDTO>>() {}))
                .isInstanceOf(HttpClientErrorException.class).extracting("statusCode")
                .isEqualTo(HttpStatus.FORBIDDEN);
    }

    @Test
    public void testGetUserRolesCurrentUser() {
        String email = "test@gmail.com";
        String password = "pass123";

        User user = createUser(email, password, UserRoleType.REGISTERED_USER);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        ResponseEntity<List<UserRoleReadDTO>> response = restTemplate.exchange(
                "http://localhost:8080/api/v1/users/" + user.getId() + "/roles", HttpMethod.GET, httpEntity,
                new ParameterizedTypeReference<>() {});
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testAddRoleToUserAdmin() {
        String email = "testt@gmail.com";
        String password = "pass123";

        UUID roleId = userRoleRepository.findUserRoleIdByType(UserRoleType.REGISTERED_USER);
        User user = createUser(email, password, UserRoleType.ADMIN);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        ResponseEntity<List<UserRoleReadDTO>> response = restTemplate.exchange(
                "http://localhost:8080/api/v1/users/" + user.getId() + "/roles/" + roleId, HttpMethod.POST,
                httpEntity, new ParameterizedTypeReference<>() {});
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetMovieActorRolesPermitAll() {
        String email = "testt@gmail.com";
        String password = "pass123";

        createUser(email, password, UserRoleType.CONTENT_MANAGER);

        Movie movie = entityCreator.createMovie();
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        ResponseEntity<List<UserRoleReadDTO>> response = restTemplate.exchange(
                "http://localhost:8080/api/v1/movies/" + movie.getId() + "/actor-roles", HttpMethod.GET, httpEntity,
                new ParameterizedTypeReference<>() {});
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testCreateRequestToCorrectCurrentRegisteredUser() {
        String email = "test1@gmail.com";
        String password = "1pass123";

        RegisteredUser regUser = createRegisteredUser(email, password);

        RequestToCorrectCreateDTO createDTO = generator.generateRandomObject(RequestToCorrectCreateDTO.class);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<RequestToCorrectCreateDTO> httpEntity = new HttpEntity<>(createDTO, headers);

        ResponseEntity<RequestToCorrectReadDTO> response = restTemplate.exchange(
                "http://localhost:8080/api/v1/users/" + regUser.getId() + "/requests-to-correct",
                HttpMethod.POST,
                httpEntity,
                new ParameterizedTypeReference<>() {});
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertNotNull(response.getBody());
        Assert.assertNotNull(response.getBody().getCreatedAt());
    }

    @Test
    public void testFixRequestToCorrectCurrentContentManager() {
        String email = "test@gmail.com";
        String password = "pass123";

        ContentManager cm = createContentManager(email, password);

        RequestToCorrect requestToCorrect = entityCreator.createRequestToCorrectMovie();
        requestToCorrect.setStatus(NEED_TO_VERIFY);
        requestToCorrect.setWrongData("wrong data");
        requestToCorrect.setVerifier(cm);
        requestToCorrectRepository.save(requestToCorrect);

        Movie movie = movieRepository.findById(requestToCorrect.getObjectId()).get();
        movie.setStoryLine("Here is wrong data. I hope, a wrong data is correct.");
        movieRepository.save(movie);

        RequestToCorrectFixDTO fix = new RequestToCorrectFixDTO();
        fix.setFix("valid fix");

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<RequestToCorrectFixDTO> httpEntity = new HttpEntity<>(fix, headers);

        ResponseEntity<RequestToCorrectReadDTO> response = restTemplate.exchange(
                "http://localhost:8080/api/v1/content-managers/" + cm.getId() + "/requests-to-correct/"
                        + requestToCorrect.getId() + "/fix",
                HttpMethod.POST,
                httpEntity,
                new ParameterizedTypeReference<>() {});
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertNotNull(response.getBody());
        Assert.assertNotNull(response.getBody().getFixedAt());
        Assert.assertEquals(FIXED, response.getBody().getStatus());

    }

    @Test
    public void testUpdateMovieActorRoleContentManager() {
        String email = "testt@gmail.com";
        String password = "pass123";

        createContentManager(email, password);

        Movie movie = entityCreator.createMovie();
        Actor actor = entityCreator.createActor();
        ActorRole role = entityCreator.createActorRole(actor, movie);

        ActorRolePutDTO putDTO = new ActorRolePutDTO();
        putDTO.setAvgRating(5.0);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));
        HttpEntity<ActorRolePutDTO> httpEntity = new HttpEntity<>(putDTO, headers);

        ResponseEntity<ActorRoleReadDTO> response = restTemplate.exchange(
                "http://localhost:8080/api/v1/movies/" + movie.getId() + "/actor-roles/" + role.getId(),
                HttpMethod.PUT, httpEntity, new ParameterizedTypeReference<>() {});

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(role.getId(), response.getBody().getId());
    }

    @Test
    public void testDeleteMovieAdmin() {
        String email = "testt@gmail.com";
        String password = "pass123";

        createAdmin(email, password);

        Movie movie = entityCreator.createMovie();

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        ResponseEntity<Void> response = restTemplate.exchange(
                "http://localhost:8080/api/v1/movies/" + movie.getId(),
                HttpMethod.DELETE, httpEntity, Void.class);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    private User createUser(String email, String password, UserRoleType role) {
        User user = new User();
        user.setEmail(email);
        user.setEncodedPassword(passwordEncoder.encode(password));
        user.getUserRoles().add(userRoleRepository.findUserRoleByType(role));
        user = userRepository.save(user);
        return user;
    }

    private User createUser(String email, String password) {
        User user = new User();
        user.setEmail(email);
        user.setEncodedPassword(passwordEncoder.encode(password));
        user = userRepository.save(user);
        return user;
    }

    private ContentManager createContentManager(String email, String password) {
        ContentManager cm = new ContentManager();
        cm.setEmail(email);
        cm.setEncodedPassword(passwordEncoder.encode(password));
        cm.getUserRoles().add(userRoleRepository.findUserRoleByType(UserRoleType.CONTENT_MANAGER));
        cm = contentManagerRepository.save(cm);
        return cm;
    }

    private RegisteredUser createRegisteredUser(String email, String password) {
        RegisteredUser ru = new RegisteredUser();
        ru.setEmail(email);
        ru.setEncodedPassword(passwordEncoder.encode(password));
        ru.getUserRoles().add(userRoleRepository.findUserRoleByType(UserRoleType.CONTENT_MANAGER));
        ru = registeredUserRepository.save(ru);
        return ru;
    }

    private Admin createAdmin(String email, String password) {
        Admin a = new Admin();
        a.setEmail(email);
        a.setEncodedPassword(passwordEncoder.encode(password));
        a.getUserRoles().add(userRoleRepository.findUserRoleByType(UserRoleType.ADMIN));
        a = adminRepository.save(a);
        return a;
    }

    private String getBasicAuthorizationHeaderValue(String userName, String password) {
        return "Basic " + new String(Base64.encode(String.format("%s:%s", userName, password).getBytes()));
    }
}
