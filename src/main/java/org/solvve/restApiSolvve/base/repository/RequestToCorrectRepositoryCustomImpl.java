package org.solvve.restapisolvve.base.repository;

import org.bitbucket.brunneng.qb.JpaQueryBuilder;
import org.bitbucket.brunneng.qb.SpringQueryBuilderUtils;
import org.solvve.restapisolvve.base.domain.RequestToCorrect;
import org.solvve.restapisolvve.base.dto.requesttocorrect.RequestToCorrectFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Set;

import static org.solvve.restapisolvve.base.domain.ObjectToCorrect.*;

public class RequestToCorrectRepositoryCustomImpl implements RequestToCorrectRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<RequestToCorrect> findByFilter(RequestToCorrectFilter filter, Pageable pageable) {
        JpaQueryBuilder qb = new JpaQueryBuilder(entityManager);
        qb.append("select r from RequestToCorrect r where 1=1");

        qb.appendIn("and r.objectType in :v", filter.getObjectTypes());
        qb.append("and r.status = :v", filter.getStatus());
        qb.appendLikeCaseInsensitive("and r.wrongData like :v", filter.getWrongData());

        if (filter.getMovieName() != null) {
            if (filter.getObjectTypes() == null || filter.getObjectTypes().isEmpty()
                    || filter.getObjectTypes().containsAll(Set.of(MOVIE, NEWS))) {
                qb.appendLikeCaseInsensitive(" and (r.objectType = 'MOVIE' and r.objectId in"
                                + " (select m.id from Movie m where m.name like :v))",
                        filter.getMovieName());
                qb.appendLikeCaseInsensitive(" or (r.objectType = 'NEWS' and r.objectId in "
                                + " (select n.id from News n join n.movies nm where nm.name like"
                                + " :v))",
                        filter.getMovieName());
            } else if (filter.getObjectTypes().contains(MOVIE)) {
                qb.appendLikeCaseInsensitive(" and (r.objectType = 'MOVIE' and r.objectId in"
                                + " (select m.id from Movie m where m.name like :v))",
                        filter.getMovieName());
            } else if (filter.getObjectTypes().contains(NEWS)) {
                qb.appendLikeCaseInsensitive(" and (r.objectType = 'NEWS' and r.objectId in "
                                + " (select n.id from News n join n.movies nm where nm.name like"
                                + " :v))",
                        filter.getMovieName());
            } else {
                qb.appendLikeCaseInsensitive(" and (r.objectType = 'PERSON' and r.objectId in"
                                + " (select m.id from Movie m where m.name like :v))",
                        filter.getMovieName());
            }
        }

        if (filter.getPersonName() != null) {
            if (filter.getObjectTypes() == null || filter.getObjectTypes().isEmpty()
                    || filter.getObjectTypes().containsAll(Set.of(PERSON, NEWS))) {
                qb.appendLikeCaseInsensitive(" and (r.objectType = 'PERSON' and r.objectId in"
                                + " (select p.id from Person p where p.name like :v))",
                        filter.getPersonName());
                qb.appendLikeCaseInsensitive(" or (r.objectType = 'NEWS' and r.objectId in "
                                + " (select n.id from News n join n.persons np where np.name "
                                + "like :v))",
                        filter.getPersonName());
            } else if (filter.getObjectTypes().contains(PERSON)) {
                qb.appendLikeCaseInsensitive(" and (r.objectType = 'PERSON' and r.objectId in"
                                + " (select p.id from Person p where p.name like :v))",
                        filter.getPersonName());
            } else if (filter.getObjectTypes().contains(NEWS)) {
                qb.appendLikeCaseInsensitive(" and (r.objectType = 'NEWS' and r.objectId in "
                        + " (select n.id from News n join n.persons np where np.name "
                        + "like :v))", filter.getPersonName());
            } else {
                qb.appendLikeCaseInsensitive(" and (r.objectType = 'MOVIE' and r.objectId in"
                                + " (select p.id from Person p where p.name like :v))",
                        filter.getPersonName());
            }
        }

        return SpringQueryBuilderUtils.loadPage(qb, pageable, "id");
    }
}