package org.solvve.restapisolvve.base.dto.movie;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Data
public class MovieFilter {
    private String movieName;
    private String country;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate releaseDate;
    private Double avgRating;
    private Set<String> genres = new HashSet<String>();
    private String actorName;
    private String company;
}
