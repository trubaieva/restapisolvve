package org.solvve.restapisolvve.base.domain;

public enum GenderType {
    FEMALE,
    MALE
}
