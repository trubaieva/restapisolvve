package org.solvve.restapisolvve.base.job.oneoff;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.client.themoviedb.TheMovieDbClient;
import org.solvve.restapisolvve.base.client.themoviedb.dto.MovieReadShortDTO;
import org.solvve.restapisolvve.base.client.themoviedb.dto.MoviesPageDTO;
import org.solvve.restapisolvve.base.exception.ImportAlreadyPerformedException;
import org.solvve.restapisolvve.base.exception.ImportedEntityAlreadyExistException;
import org.solvve.restapisolvve.base.service.importer.MovieImporterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

public class TheMovieDbImportOneOffJobTest extends BaseTest {

    @Autowired
    private TheMovieDbImportOneOffJob job;

    @MockBean
    private TheMovieDbClient client;

    @MockBean
    private MovieImporterService movieImporterService;

    @Test
    public void testDoImport() throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {
        MoviesPageDTO page = generatePageWith2Results();
        Mockito.when(client.getTopRatedMovies()).thenReturn(page);

        job.doImport();

        for (MovieReadShortDTO m : page.getResults()) {
            Mockito.verify(movieImporterService).importMovie(m.getId());
        }
    }

    @Test
    public void testDoImportNoExceptionIfGetPageFailed() {
        Mockito.when(client.getTopRatedMovies()).thenThrow(RuntimeException.class);

        job.doImport();

        Mockito.verifyNoInteractions(movieImporterService);
    }

    @Test
    public void testDoImportFirstFailedAndSecondSuccess()
            throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {
        MoviesPageDTO page = generatePageWith2Results();
        Mockito.when(client.getTopRatedMovies()).thenReturn(page);
        Mockito.when(movieImporterService.importMovie(page.getResults().get(0).getId()))
                .thenThrow(RuntimeException.class);

        job.doImport();

        for (MovieReadShortDTO m : page.getResults()) {
            Mockito.verify(movieImporterService).importMovie(m.getId());
        }
    }

    private MoviesPageDTO generatePageWith2Results() {
        MoviesPageDTO page = generator.generateRandomObject(MoviesPageDTO.class);
        page.getResults().add(generator.generateRandomObject(MovieReadShortDTO.class));
        Assert.assertEquals(2, page.getResults().size());

        return page;
    }

}
