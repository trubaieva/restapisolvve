package org.solvve.restapisolvve.base.dto.person;

import lombok.Data;
import org.solvve.restapisolvve.base.domain.CrewDepartment;
import org.solvve.restapisolvve.base.domain.GenderType;

import java.time.LocalDate;

@Data
public class PersonPatchDTO {
    private String name;
    private String biography;
    private LocalDate birthDate;
    private String birthPlace;
    private GenderType gender;
    private LocalDate deathDate;
    private CrewDepartment knownForDepartment;
}
