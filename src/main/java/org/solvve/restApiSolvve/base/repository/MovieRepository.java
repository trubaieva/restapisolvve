package org.solvve.restapisolvve.base.repository;

import org.solvve.restapisolvve.base.domain.Movie;
import org.solvve.restapisolvve.base.dto.movie.MovieInLeaderBoardReadDTO;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

@Repository
public interface MovieRepository extends CrudRepository<Movie, UUID>, MovieRepositoryCustom {

    List<Movie> findDistinctByGenresNameContainsAndAvgRatingAfterOrderByReleaseDateDesc(String genreName,
            Double rating);

    List<Movie> findDistinctByGenresNameContainsAndProductionCompaniesNameContainsOrderByAvgRating(
            String genreName, String productionCompanyName);

    @Query("select distinct m from Movie m join m.genres g where g.name = :genreName and m.avgRating >= :rating and "
            + "m.releaseDate >= :releaseFrom and m.releaseDate < :releaseTo order by m.releaseDate asc")
    List<Movie> findByGenreAndMinAvgRatingInGivenDateInterval(String genreName, Double rating,
            LocalDate releaseFrom, LocalDate releaseTo);

    @Query("select m.id from Movie m")
    Stream<UUID> getIdsFromMovies();

    @Query("select new org.solvve.restapisolvve.base.dto.movie.MovieInLeaderBoardReadDTO(m.id, m.name, m.avgRating, "
            + "(select count(r) from MovieRate r where r.movie.id = m.id and r.rate is not null)) "
            + "from Movie m order by m.avgRating desc")
    List<MovieInLeaderBoardReadDTO> getMoviesLeaderBoard();

    Movie findByName(String name);

    @Query("select m from Movie m where m.name = :name and year(m.releaseDate) = :year")
    Movie findByNameAndReleaseYear(String name, Integer year);
}
