package org.solvve.restapisolvve.base.service;

import org.solvve.restapisolvve.base.domain.Movie;
import org.solvve.restapisolvve.base.domain.ProductionCompany;
import org.solvve.restapisolvve.base.dto.productioncompany.ProductionCompanyCreateDTO;
import org.solvve.restapisolvve.base.dto.productioncompany.ProductionCompanyPutDTO;
import org.solvve.restapisolvve.base.dto.productioncompany.ProductionCompanyReadDTO;
import org.solvve.restapisolvve.base.exception.EntityNotFoundException;
import org.solvve.restapisolvve.base.exception.LinkDuplicatedException;
import org.solvve.restapisolvve.base.repository.MovieRepository;
import org.solvve.restapisolvve.base.repository.ProductionCompanyRepository;
import org.solvve.restapisolvve.base.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class ProductionCompanyService {

    @Autowired
    RepositoryHelper repositoryHelper;

    @Autowired
    private ProductionCompanyRepository productionCompanyRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private TranslationService translationService;

    public ProductionCompanyReadDTO getProductionCompany(UUID id) {
        ProductionCompany productionCompany = repositoryHelper.getEntityIfExist(ProductionCompany.class, id);
        return translationService.translate(productionCompany, ProductionCompanyReadDTO.class);
    }

    public List<ProductionCompanyReadDTO> getProductionCompanies() {
        List<ProductionCompanyReadDTO> dtos = new ArrayList<>();
        Iterable<ProductionCompany> productionCompanyIterable = productionCompanyRepository.findAll();
        for (ProductionCompany productionCompany : productionCompanyIterable) {
            dtos.add(translationService.translate(productionCompany, ProductionCompanyReadDTO.class));
        }
        return dtos;
    }

    public ProductionCompanyReadDTO createProductionCompany(ProductionCompanyCreateDTO create) {
        ProductionCompany productionCompany = translationService.translate(create, ProductionCompany.class);
        productionCompany = productionCompanyRepository.save(productionCompany);
        return translationService.translate(productionCompany, ProductionCompanyReadDTO.class);
    }

    public ProductionCompanyReadDTO updateProductionCompany(UUID id, ProductionCompanyPutDTO putDTO) {
        ProductionCompany productionCompany = repositoryHelper.getEntityIfExist(ProductionCompany.class, id);

        translationService.map(putDTO, productionCompany);

        productionCompany = productionCompanyRepository.save(productionCompany);
        return translationService.translate(productionCompany, ProductionCompanyReadDTO.class);
    }

    public void deleteProductionCompany(UUID id) {
        productionCompanyRepository.delete(repositoryHelper.getEntityIfExist(ProductionCompany.class, id));
    }

    @Transactional
    public List<ProductionCompanyReadDTO> addProductionCompanyToMovie(UUID movieId, UUID id) {
        Movie movie = repositoryHelper.getEntityIfExist(Movie.class, movieId);
        ProductionCompany productionCompany = repositoryHelper.getEntityIfExist(ProductionCompany.class, id);

        if (movie.getProductionCompanies().stream().anyMatch(c -> c.getId().equals(id))) {
            throw new LinkDuplicatedException(String.format("Movie %s already has production company %s", movieId, id));
        }
        movie.getProductionCompanies().add(productionCompany);
        movie = movieRepository.save(movie);

        return translationService.translateToList(movie.getProductionCompanies(), ProductionCompanyReadDTO.class);
    }

    @Transactional
    public List<ProductionCompanyReadDTO> removeProductionCompanyFromMovie(UUID movieId, UUID id) {
        Movie movie = repositoryHelper.getEntityIfExist(Movie.class, movieId);

        boolean removed = movie.getProductionCompanies().removeIf(c -> c.getId().equals(id));
        if (!removed) {
            throw new EntityNotFoundException("Movie " + movieId + " has no production company " + id);
        }
        movie = movieRepository.save(movie);

        return translationService.translateToList(movie.getProductionCompanies(), ProductionCompanyReadDTO.class);
    }
}
