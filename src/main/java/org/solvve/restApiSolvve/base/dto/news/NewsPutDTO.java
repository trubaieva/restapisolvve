package org.solvve.restapisolvve.base.dto.news;

import lombok.Data;

@Data
public class NewsPutDTO {
    private String header;
    private String text;
    private Integer likeCount;
}
