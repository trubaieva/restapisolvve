package org.solvve.restapisolvve.base.service;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.Movie;
import org.solvve.restapisolvve.base.domain.RegisteredUser;
import org.solvve.restapisolvve.base.dto.movie.*;
import org.solvve.restapisolvve.base.exception.EntityNotFoundException;
import org.solvve.restapisolvve.base.repository.MovieRepository;
import org.solvve.restapisolvve.base.util.DtoCreator;
import org.solvve.restapisolvve.base.util.EntityCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.support.TransactionTemplate;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.UUID;

public class MovieServiceTest extends BaseTest {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private EntityCreator entityCreator;

    @Autowired
    private DtoCreator dtoCreator;

    @Autowired
    private MovieService movieService;

    @Autowired
    private TransactionTemplate transactionTemplate;

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status -> {
            runnable.run();
        });
    }

    @Test
    public void testGetMovie() {
        Movie movie = entityCreator.createMovie();

        MovieReadDTO readDTO = movieService.getMovie(movie.getId());
        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(movie);
    }

    @Test
    public void testGetMovieExtended() {
        Movie movie = entityCreator.createMovie();
        MovieReadExtendedDTO readExtDTO = movieService.getMovieExtended(movie.getId());
        inTransaction(() -> {
            Assertions.assertThat(readExtDTO)
                    .isEqualToIgnoringGivenFields(movie, "genres", "crews", "actorRoles",
                            "productionCompanies", "movieReviews", "news");
        });
    }

    @Test
    public void testGetMoviesWithEmptyFilterPagingAndSorting() {
        Movie movie1 = new Movie();
        movie1.setAvgRating(8.8);
        movie1.setReleaseDate(LocalDate.of(2019, 1, 1));
        Movie movie2 = new Movie();
        movie2.setAvgRating(8.0);
        movie2.setReleaseDate(LocalDate.of(2019, 1, 2));
        Movie movie3 = new Movie();
        movie3.setAvgRating(9.0);
        movie3.setReleaseDate(LocalDate.of(2019, 1, 3));
        Movie movie4 = new Movie();
        movie4.setAvgRating(8.9);
        movie4.setReleaseDate(LocalDate.of(2019, 1, 4));
        movieRepository.saveAll(Arrays.asList(movie1, movie2, movie3, movie4));

        MovieFilter filter = new MovieFilter();

        PageRequest pageRequest1 = PageRequest.of(0, 2, Sort.by(Sort.Direction.ASC, "releaseDate"));
        Assertions.assertThat(movieService.getMovies(filter, pageRequest1).getData()).extracting("id")
                .isEqualTo(Arrays.asList(movie1.getId(), movie2.getId()));

        PageRequest pageRequest2 = PageRequest.of(1, 2, Sort.by(Sort.Direction.DESC, "avgRating"));
        Assertions.assertThat(movieService.getMovies(filter, pageRequest2).getData()).extracting("id")
                .isEqualTo(Arrays.asList(movie1.getId(), movie2.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetMovieWrongId() {
        movieService.getMovie(UUID.randomUUID());
    }

    @Test
    public void testCreateMovie() {
        MovieCreateDTO create = dtoCreator.createMovieCreateDTO();

        MovieReadDTO read = movieService.createMovie(create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        Movie movie = movieRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(movie);
    }

    @Test
    public void testPatchMovie() {
        Movie movie = entityCreator.createMovie();

        MoviePatchDTO patch = dtoCreator.createMoviePatchDTO();
        MovieReadDTO read = movieService.patchMovie(movie.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        movie = movieRepository.findById(read.getId()).get();
        Assertions.assertThat(movie)
                .isEqualToIgnoringGivenFields(read,
                        "genres", "crews", "actorRoles", "productionCompanies",
                        "movieReviews", "news");
    }

    @Test
    public void testPatchMovieEmptyPatch() {
        Movie movie = entityCreator.createMovie();

        MoviePatchDTO patch = new MoviePatchDTO();
        MovieReadDTO read = movieService.patchMovie(movie.getId(), patch);

        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        Movie movieAfterUpdate = movieRepository.findById(read.getId()).get();

        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        Assertions.assertThat(movie)
                .isEqualToIgnoringGivenFields(movieAfterUpdate,
                        "genres", "crews", "actorRoles", "productionCompanies",
                        "movieReviews", "news");
    }

    @Test
    public void testPutMovie() {
        Movie movie = entityCreator.createMovie();

        MoviePutDTO putDTO = dtoCreator.createMoviePutDTO();
        MovieReadDTO read = movieService.updateMovie(movie.getId(), putDTO);

        Assertions.assertThat(putDTO).isEqualToComparingFieldByField(read);

        movie = movieRepository.findById(read.getId()).get();
        Assertions.assertThat(movie)
                .isEqualToIgnoringGivenFields(read,
                        "genres", "crews", "actorRoles", "productionCompanies",
                        "movieReviews", "news");
    }

    @Test
    public void testUpdateMovieEmptyPut() {
        Movie movie = entityCreator.createMovie();

        MoviePutDTO putDTO = new MoviePutDTO();
        MovieReadDTO read = movieService.updateMovie(movie.getId(), putDTO);

        Assertions.assertThat(read)
                .hasAllNullFieldsOrPropertiesExcept("id",
                        "genres",
                        "crews",
                        "actorRoles",
                        "productionCompanies",
                        "movieReviews",
                        "news", "createdAt", "updatedAt"
                );

        Movie movieAfterUpdate = movieRepository.findById(read.getId()).get();

        Assertions.assertThat(movieAfterUpdate)
                .hasAllNullFieldsOrPropertiesExcept("id",
                        "genres",
                        "crews",
                        "actorRoles",
                        "productionCompanies",
                        "movieReviews",
                        "news", "createdAt", "updatedAt"
                );
    }

    @Test
    public void testDeleteMovie() {
        Movie movie = entityCreator.createMovie();

        movieService.deleteMovie(movie.getId());
        Assert.assertFalse(movieRepository.existsById(movie.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteMovieNotFound() {
        movieService.deleteMovie(UUID.randomUUID());
    }

    @Test
    public void testUpdateAverageRating() {
        RegisteredUser creator = entityCreator.createRegisteredUser();
        Movie movie = entityCreator.createMovie();

        entityCreator.createMovieRate(creator, movie, 3);
        entityCreator.createMovieRate(creator, movie, 5);

        movieService.updateAverageRating((movie.getId()));
        movie = movieRepository.findById(movie.getId()).get();
        Assert.assertEquals(4.0, movie.getAvgRating(), Double.MIN_NORMAL);
    }
}