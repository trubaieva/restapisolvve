package org.solvve.restapisolvve.base.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Entity
@Getter
@Setter
public class Moderator extends User {
}
