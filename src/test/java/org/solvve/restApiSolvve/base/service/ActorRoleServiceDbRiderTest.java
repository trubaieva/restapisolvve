package org.solvve.restapisolvve.base.service;

import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.api.dataset.ExpectedDataSet;
import com.github.database.rider.spring.api.DBRider;
import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

@DBRider
public class ActorRoleServiceDbRiderTest extends BaseTest {

    @Autowired
    private ActorRoleService actorRoleService;

    @Test
    @DataSet(value = "/datasets/testUpdateAverageRatingOfActorRole.xml")
    //disableConstraints = true, cleanAfter = true, skipCleaningFor = {"USER_ROLE"}
    @ExpectedDataSet(value = "/datasets/testUpdateAverageRatingOfActorRole_result.xml")
    public void testUpdateAverageRatingOfActorRole() {
        UUID actorRoleId = UUID.fromString("2d888c3b-d097-43c5-8f35-6033b36de9ed");
        actorRoleService.updateAverageRating(actorRoleId);
    }

}
