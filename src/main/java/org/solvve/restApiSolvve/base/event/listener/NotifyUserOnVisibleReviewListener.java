package org.solvve.restapisolvve.base.event.listener;

import lombok.extern.slf4j.Slf4j;
import org.solvve.restapisolvve.base.event.ReviewVisibilityChangedEvent;
import org.solvve.restapisolvve.base.service.UserNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class NotifyUserOnVisibleReviewListener {

    @Autowired
    private UserNotificationService userNotificationService;

    @Async
    @EventListener(condition = "#event.isVisible")
    public void onEvent(ReviewVisibilityChangedEvent event) {
        log.info("handling {}", event);
        userNotificationService.notifyOnReviewBecomeVisible(event.getReviewId());
    }
}
