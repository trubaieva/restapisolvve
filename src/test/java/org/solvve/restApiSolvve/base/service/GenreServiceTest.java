package org.solvve.restapisolvve.base.service;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.Genre;
import org.solvve.restapisolvve.base.domain.Movie;
import org.solvve.restapisolvve.base.dto.genre.GenreCreateDTO;
import org.solvve.restapisolvve.base.dto.genre.GenrePutDTO;
import org.solvve.restapisolvve.base.dto.genre.GenreReadDTO;
import org.solvve.restapisolvve.base.exception.EntityNotFoundException;
import org.solvve.restapisolvve.base.exception.LinkDuplicatedException;
import org.solvve.restapisolvve.base.repository.GenreRepository;
import org.solvve.restapisolvve.base.repository.MovieRepository;
import org.solvve.restapisolvve.base.util.EntityCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class GenreServiceTest extends BaseTest {

    @Autowired
    EntityCreator entityCreator;

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private GenreService genreService;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private TranslationService translationService;

    @Test
    public void testGetGenre() {
        Genre genre = entityCreator.createGenre();

        GenreReadDTO readDTO = genreService.getGenre(genre.getId());
        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(genre);
    }

    @Test
    public void testGetGenres() {
        List<Genre> genres = new ArrayList<>();
        Genre genre;
        for (int i = 0; i < 5; i++) {
            genre = entityCreator.createGenre();
            genreRepository.save(genre);
            genres.add(genre);
        }
        List<GenreReadDTO> readDTOs = genreService.getGenres();
        Assertions.assertThat(readDTOs)
                .extracting("id")
                .containsExactlyInAnyOrder(genres.stream().map(Genre::getId).toArray());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetGenreWrongId() {
        genreService.getGenre(UUID.randomUUID());
    }

    @Test
    public void testCreateGenre() {
        GenreCreateDTO create = generator.generateRandomObject(GenreCreateDTO.class);

        GenreReadDTO read = genreService.createGenre(create);
        Assertions.assertThat(create).isEqualToIgnoringGivenFields(read);
        Assert.assertNotNull(read.getId());

        Genre genre = genreRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(genre);
    }

    @Test
    public void testPutGenre() {
        Genre genre = entityCreator.createGenre();
        genreRepository.save(genre);

        GenrePutDTO putDTO = generator.generateRandomObject(GenrePutDTO.class);
        GenreReadDTO read = genreService.updateGenre(genre.getId(), putDTO);

        Assertions.assertThat(putDTO).isEqualToIgnoringGivenFields(read);

        genre = genreRepository.findById(read.getId()).get();
        Assertions.assertThat(genre).isEqualToIgnoringGivenFields(read, "movies");
    }

    @Test
    public void testUpdateGenreEmptyPut() {
        Genre genre = entityCreator.createGenre();
        genreRepository.save(genre);

        GenrePutDTO putDTO = new GenrePutDTO();
        GenreReadDTO read = genreService.updateGenre(genre.getId(), putDTO);

        Assertions.assertThat(read).hasAllNullFieldsOrPropertiesExcept("id", "createdAt", "updatedAt");

        Genre genreAfterUpdate = genreRepository.findById(read.getId()).get();

        Assertions.assertThat(genreAfterUpdate).hasAllNullFieldsOrPropertiesExcept("id", "movies", "createdAt",
                "updatedAt");
    }

    @Test
    public void testDeleteGenre() {
        Genre genre = entityCreator.createGenre();
        genreRepository.save(genre);

        genreService.deleteGenre(genre.getId());
        Assert.assertFalse(genreRepository.existsById(genre.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteGenreNotFound() {
        genreService.deleteGenre(UUID.randomUUID());
    }

    @Test
    public void testAddGenreToMovie() {
        Movie movie = entityCreator.createMovie();
        Genre genre = entityCreator.createGenre();

        List<GenreReadDTO> res = genreService.addGenreToMovie(movie.getId(), genre.getId());

        GenreReadDTO expectedRead = translationService.translate(genre, GenreReadDTO.class);
        Assertions.assertThat(res).containsExactlyInAnyOrder(expectedRead);

        transactionTemplate.executeWithoutResult(status -> {
            Movie movieAfterSave = movieRepository.findById(movie.getId()).get();
            Assertions.assertThat(movieAfterSave.getGenres()).extracting((Genre::getId))
                    .containsExactlyInAnyOrder(genre.getId());

            Genre genreAfterSave = genreRepository.findById(genre.getId()).get();
            Assertions.assertThat(genreAfterSave.getMovies()).extracting((Movie::getId))
                    .containsExactlyInAnyOrder(movie.getId());
        });
    }

    @Test
    public void testAddGenreToMovieWithGenre() {
        Movie movie = entityCreator.createMovie();
        Genre genre1 = entityCreator.createGenre();
        Genre genre2 = entityCreator.createGenre();

        genreService.addGenreToMovie(movie.getId(), genre1.getId());
        List<GenreReadDTO> res2 = genreService.addGenreToMovie(movie.getId(), genre2.getId());

        GenreReadDTO expectedRead1 = translationService.translate(genre1, GenreReadDTO.class);
        GenreReadDTO expectedRead2 = translationService.translate(genre2, GenreReadDTO.class);

        Assertions.assertThat(res2).containsExactlyInAnyOrder(expectedRead1, expectedRead2);

        transactionTemplate.executeWithoutResult(status -> {
            Movie movieAfterSave = movieRepository.findById(movie.getId()).get();
            Assertions.assertThat(movieAfterSave.getGenres()).extracting((Genre::getId))
                    .containsExactlyInAnyOrder(genre1.getId(), genre2.getId());

            Genre genreAfterSave1 = genreRepository.findById(genre1.getId()).get();
            Assertions.assertThat(genreAfterSave1.getMovies()).extracting((Movie::getId))
                    .containsExactlyInAnyOrder(movie.getId());

            Genre genreAfterSave2 = genreRepository.findById(genre1.getId()).get();
            Assertions.assertThat(genreAfterSave2.getMovies()).extracting((Movie::getId))
                    .containsExactlyInAnyOrder(movie.getId());
        });
    }

    @Test
    public void testDuplicatedGenre() {
        Movie movie = entityCreator.createMovie();
        Genre genre = entityCreator.createGenre();

        genreService.addGenreToMovie(movie.getId(), genre.getId());
        Assertions.assertThatThrownBy(() -> {
            genreService.addGenreToMovie(movie.getId(), genre.getId());
        }).isInstanceOf(LinkDuplicatedException.class);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testWrongMovieId() {
        UUID wrongMovieId = UUID.randomUUID();
        Genre genre = entityCreator.createGenre();
        genreService.addGenreToMovie(wrongMovieId, genre.getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testWrongGenreId() {
        Movie movie = entityCreator.createMovie();
        UUID wrongGenreId = UUID.randomUUID();
        genreService.addGenreToMovie(movie.getId(), wrongGenreId);
    }

    @Test
    public void testRemoveGenreFromMovie() {
        Movie movie = entityCreator.createMovie();
        Genre genre = entityCreator.createGenre();
        addGenreToMovie(movie.getId(), genre.getId());

        List<GenreReadDTO> remainingGenres = genreService.removeGenreFromMovie(movie.getId(), genre.getId());
        Assert.assertTrue(remainingGenres.isEmpty());

        transactionTemplate.executeWithoutResult(status -> {
            Movie movieAfterRemove = movieRepository.findById(movie.getId()).get();
            Assert.assertTrue(movieAfterRemove.getGenres().isEmpty());

            Genre genreAfterSave = genreRepository.findById(genre.getId()).get();
            Assert.assertTrue(genreAfterSave.getMovies().isEmpty());
        });
    }

    @Test
    public void testRemoveGenreFromMovieWithSeveralGenres() {
        Movie movie = entityCreator.createMovie();
        Genre genre1 = entityCreator.createGenre();
        Genre genre2 = entityCreator.createGenre();

        addGenreToMovie(movie.getId(), genre1.getId());
        addGenreToMovie(movie.getId(), genre2.getId());

        GenreReadDTO expectedRead2 = translationService.translate(genre2, GenreReadDTO.class);

        List<GenreReadDTO> remainingGenres = genreService.removeGenreFromMovie(movie.getId(), genre1.getId());

        Assertions.assertThat(remainingGenres).containsExactlyInAnyOrder(expectedRead2);

        transactionTemplate.executeWithoutResult(status -> {
            Movie movieAfterRemove = movieRepository.findById(movie.getId()).get();
            Assertions.assertThat(movieAfterRemove.getGenres()).extracting((Genre::getId))
                    .containsExactlyInAnyOrder(genre2.getId());

            Genre genreAfterSave1 = genreRepository.findById(genre1.getId()).get();
            Assert.assertTrue(genreAfterSave1.getMovies().isEmpty());

            Genre genreAfterSave2 = genreRepository.findById(genre2.getId()).get();
            Assertions.assertThat(genreAfterSave2.getMovies()).extracting((Movie::getId))
                    .containsExactlyInAnyOrder(movie.getId());
        });
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotAddedGenre() {
        Movie movie = entityCreator.createMovie();
        Genre genre = entityCreator.createGenre();

        genreService.removeGenreFromMovie(movie.getId(), genre.getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotExistedGenre() {
        Movie movie = entityCreator.createMovie();
        genreService.removeGenreFromMovie(movie.getId(), UUID.randomUUID());
    }

    private void addGenreToMovie(UUID movieId, UUID genreId) {
        transactionTemplate.executeWithoutResult(status -> {
            Movie movie = movieRepository.findById(movieId).get();
            Genre genre = genreRepository.findById(genreId).get();
            movie.getGenres().add(genre);
            movieRepository.save(movie);
        });
    }

}
