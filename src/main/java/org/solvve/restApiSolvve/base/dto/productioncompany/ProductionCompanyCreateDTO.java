package org.solvve.restapisolvve.base.dto.productioncompany;

import lombok.Data;

@Data
public class ProductionCompanyCreateDTO {
    private String name;
}
