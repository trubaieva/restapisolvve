package org.solvve.restapisolvve.base.dto.genre;

import lombok.Data;

@Data
public class GenrePutDTO {
    private String name;
}
