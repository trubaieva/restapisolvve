package org.solvve.restapisolvve.base.controller;

import org.solvve.restapisolvve.base.dto.genre.GenreCreateDTO;
import org.solvve.restapisolvve.base.dto.genre.GenrePutDTO;
import org.solvve.restapisolvve.base.dto.genre.GenreReadDTO;
import org.solvve.restapisolvve.base.service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class GenreController {

    @Autowired
    private GenreService genreService;

    @PreAuthorize("permitAll()")
    @GetMapping("/genres")
    public List<GenreReadDTO> getGenres() {
        return genreService.getGenres();
    }

    @PreAuthorize("permitAll()")
    @GetMapping("/genres/{id}")
    public GenreReadDTO getGenre(@PathVariable UUID id) {
        return genreService.getGenre(id);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'CONTENT_MANAGER')")
    @PostMapping("/genres")
    public GenreReadDTO createGenre(@RequestBody GenreCreateDTO createDTO) {
        return genreService.createGenre(createDTO);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'CONTENT_MANAGER')")
    @PutMapping("/genres/{id}")
    public GenreReadDTO updateGenre(@PathVariable UUID id, @RequestBody GenrePutDTO putDTO) {
        return genreService.updateGenre(id, putDTO);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'CONTENT_MANAGER')")
    @DeleteMapping("/genres/{id}")
    public void deleteGenre(@PathVariable UUID id) {
        genreService.deleteGenre(id);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'CONTENT_MANAGER')")
    @PostMapping("/movies/{movieId}/genres/{id}")
    public List<GenreReadDTO> addGenreToMovie(@PathVariable UUID movieId, @PathVariable UUID id) {
        return genreService.addGenreToMovie(movieId, id);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'CONTENT_MANAGER')")
    @DeleteMapping("/movies/{movieId}/genres/{id}")
    public List<GenreReadDTO> removeGenreFromMovie(@PathVariable UUID movieId, @PathVariable UUID id) {
        return genreService.removeGenreFromMovie(movieId, id);
    }
}
