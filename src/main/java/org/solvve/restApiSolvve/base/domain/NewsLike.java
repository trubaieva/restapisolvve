package org.solvve.restapisolvve.base.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@DiscriminatorValue("news")
public class NewsLike extends Like {

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "news_id", nullable = false, updatable = false)
    private News news;
}
