package org.solvve.restapisolvve.base.repository;

import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.ProductionCompany;
import org.solvve.restapisolvve.base.util.EntityCreator;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;

import static org.junit.Assert.*;

public class ProductionCompanyRepositoryTest extends BaseTest {

    @Autowired
    private ProductionCompanyRepository productionCompanyRepository;

    @Autowired
    private EntityCreator entityCreator;

    @Test
    public void testCreatedAtIsSet() {
        ProductionCompany productionCompany = entityCreator.createProductionCompany();

        Instant createdAtBeforeReload = productionCompany.getCreatedAt();
        assertNotNull(createdAtBeforeReload);
        productionCompany = productionCompanyRepository.findById(productionCompany.getId()).get();

        Instant createdAtAfterReload = productionCompany.getCreatedAt();
        assertNotNull(createdAtAfterReload);
        assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        ProductionCompany productionCompany = entityCreator.createProductionCompany();

        Instant updatedAtBeforeReload = productionCompany.getUpdatedAt();
        assertNotNull(updatedAtBeforeReload);
        productionCompany = productionCompanyRepository.findById(productionCompany.getId()).get();

        Instant updatedAtAfterReload = productionCompany.getUpdatedAt();
        assertNotNull(updatedAtAfterReload);
        assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        productionCompany.setName("Production Company");
        productionCompanyRepository.save(productionCompany);
        productionCompany = productionCompanyRepository.findById(productionCompany.getId()).get();
        Instant updatedAtAfterModificationAndReload = productionCompany.getUpdatedAt();
        assertTrue(updatedAtAfterModificationAndReload.isAfter(updatedAtAfterReload));
    }

}
