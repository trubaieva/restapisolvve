package org.solvve.restapisolvve.base.repository;

import org.solvve.restapisolvve.base.domain.ExternalSystemImport;
import org.solvve.restapisolvve.base.domain.ImportedEntityType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ExternalSystemImportRepository extends CrudRepository<ExternalSystemImport, UUID> {
    ExternalSystemImport findByIdInExternalSystemAndEntityType(String idInExternalSystem,
            ImportedEntityType entityType);
}
