package org.solvve.restapisolvve.base.util;

import org.bitbucket.brunneng.br.Configuration;
import org.bitbucket.brunneng.br.RandomObjectGenerator;
import org.solvve.restapisolvve.base.domain.AbstractEntity;
import org.springframework.stereotype.Component;

@Component
public abstract class AbstractCreator {

    protected RandomObjectGenerator generator = new RandomObjectGenerator();

    private RandomObjectGenerator flatGenerator;

    public AbstractCreator() {
        Configuration c = new Configuration();
        c.setFlatMode(true);
        flatGenerator = new RandomObjectGenerator(c);
    }

    protected <T extends AbstractEntity> T generateEntityWithoutId(Class<T> entityClass) {
        T entity = generator.generateRandomObject(entityClass);
        entity.setId(null);
        return entity;
    }

    protected <T extends AbstractEntity> T generateFlatEntityWithoutId(Class<T> entityClass) {
        T entity = flatGenerator.generateRandomObject(entityClass);
        entity.setId(null);
        return entity;
    }

    protected <T> T generateObject(Class<T> objectClass) {
        return generator.generateRandomObject(objectClass);
    }
}
