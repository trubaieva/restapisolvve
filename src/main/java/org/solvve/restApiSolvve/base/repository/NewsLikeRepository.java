package org.solvve.restapisolvve.base.repository;

import org.solvve.restapisolvve.base.domain.NewsLike;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface NewsLikeRepository extends CrudRepository<NewsLike, UUID> {
}
