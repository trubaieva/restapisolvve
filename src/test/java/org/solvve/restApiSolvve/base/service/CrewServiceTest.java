package org.solvve.restapisolvve.base.service;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.Crew;
import org.solvve.restapisolvve.base.domain.Movie;
import org.solvve.restapisolvve.base.domain.Person;
import org.solvve.restapisolvve.base.dto.crew.CrewCreateDTO;
import org.solvve.restapisolvve.base.dto.crew.CrewPatchDTO;
import org.solvve.restapisolvve.base.dto.crew.CrewPutDTO;
import org.solvve.restapisolvve.base.dto.crew.CrewReadDTO;
import org.solvve.restapisolvve.base.exception.EntityNotFoundException;
import org.solvve.restapisolvve.base.repository.CrewRepository;
import org.solvve.restapisolvve.base.util.EntityCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.UUID;

public class CrewServiceTest extends BaseTest {

    @Autowired
    private CrewRepository crewRepository;

    @Autowired
    private CrewService crewService;

    @Autowired
    private EntityCreator entityCreator;

    @Autowired
    private TransactionTemplate transactionTemplate;

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status -> {
            runnable.run();
        });
    }

    @Test
    public void testGetMovieCrew() {
        Crew crew = entityCreator.createCrew();

        CrewReadDTO readDTO = crewService.getMovieCrew(crew.getMovie().getId(), crew.getId());

        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(crew,
                "personId", "movieId");
        Assert.assertEquals(readDTO.getMovieId(), crew.getMovie().getId());
        Assert.assertEquals(readDTO.getPersonId(), crew.getPerson().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetCrewWrongId() {
        Movie movie = entityCreator.createMovie();
        crewService.getMovieCrew(UUID.randomUUID(), movie.getId());
    }

    @Test
    public void testCreateMovieCrew() {
        CrewCreateDTO create = generator.generateRandomObject(CrewCreateDTO.class);

        Person person = entityCreator.createPerson();
        Movie movie = entityCreator.createMovie();

        create.setPersonId(person.getId());

        CrewReadDTO read = crewService.createMovieCrew(movie.getId(), create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        Crew crew = crewRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(crew,
                "personId", "movieId");
        Assert.assertEquals(read.getMovieId(), crew.getMovie().getId());
        Assert.assertEquals(read.getPersonId(), crew.getPerson().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testCreateMovieCrewWithWrongMovie() {
        CrewCreateDTO create = generator.generateRandomObject(CrewCreateDTO.class);
        crewService.createMovieCrew(UUID.randomUUID(), create);
    }

    @Test
    public void testPatchMovieCrew() {
        Crew crew = entityCreator.createCrew();

        CrewPatchDTO patch = generator.generateRandomObject(CrewPatchDTO.class);

        Person person = entityCreator.createPerson();
        patch.setPersonId(person.getId());

        CrewReadDTO read = crewService.patchMovieCrew(crew.getMovie().getId(),
                crew.getId(),
                patch
        );

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        crew = crewRepository.findById(read.getId()).get();

        Assertions.assertThat(read).isEqualToIgnoringGivenFields(crew,
                "personId", "movieId");
        Assert.assertEquals(read.getMovieId(), crew.getMovie().getId());
        Assert.assertEquals(read.getPersonId(), patch.getPersonId());
    }

    @Test
    public void testPatchCrewEmptyPatch() {
        Crew crew = entityCreator.createCrew();

        CrewPatchDTO patch = new CrewPatchDTO();

        CrewReadDTO read = crewService.patchMovieCrew(crew.getMovie().getId(),
                crew.getId(),
                patch
        );

        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        inTransaction(() -> {
            Crew crewAfterUpdate = crewRepository.findById(read.getId()).get();
            Assertions.assertThat(read).hasNoNullFieldsOrProperties();
            Assertions.assertThat(crew)
                    .isEqualToIgnoringGivenFields(crewAfterUpdate,
                            "person", "movie");
        });
    }

    @Test
    public void testUpdateMovieCrew() {
        Crew crew = entityCreator.createCrew();

        CrewPutDTO putDTO = generator.generateRandomObject(CrewPutDTO.class);

        Person person = entityCreator.createPerson();
        putDTO.setPersonId(person.getId());

        CrewReadDTO read = crewService.updateMovieCrew(crew.getMovie().getId(),
                crew.getId(),
                putDTO
        );

        Assertions.assertThat(putDTO).isEqualToComparingFieldByField(read);

        crew = crewRepository.findById(read.getId()).get();
        Assertions.assertThat(crew)
                .isEqualToIgnoringGivenFields(read,
                        "person", "movie");
        Assert.assertEquals(read.getMovieId(), crew.getMovie().getId());
        Assert.assertEquals(read.getPersonId(), putDTO.getPersonId());
    }

    @Test
    public void testUpdateCrewEmptyPut() {
        Crew crew = entityCreator.createCrew();

        CrewPutDTO putDTO = new CrewPutDTO();

        CrewReadDTO read = crewService.updateMovieCrew(crew.getMovie().getId(),
                crew.getId(),
                putDTO
        );

        Assertions.assertThat(read).hasAllNullFieldsOrPropertiesExcept("id", "personId", "movieId",
                "createdAt", "updatedAt");

        inTransaction(() -> {
            Crew crewAfterUpdate = crewRepository.findById(read.getId()).get();
            Assertions.assertThat(crewAfterUpdate)
                    .hasAllNullFieldsOrPropertiesExcept("id", "person", "movie", "createdAt", "updatedAt");
        });
    }

    @Test
    public void testDeleteMovieCrew() {
        Crew crew = entityCreator.createCrew();

        crewService.deleteMovieCrew(crew.getMovie().getId(), crew.getId());
        Assert.assertFalse(crewRepository.existsById(crew.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteCrewNotFound() {
        crewService.deleteMovieCrew(UUID.randomUUID(), UUID.randomUUID());
    }

}
