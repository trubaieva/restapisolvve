package org.solvve.restapisolvve.base.security;

import org.assertj.core.api.Assertions;
import org.assertj.core.util.IterableUtil;
import org.junit.Assert;
import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.User;
import org.solvve.restapisolvve.base.repository.UserRepository;
import org.solvve.restapisolvve.base.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.HashSet;

public class UserDetailsServiceImplTest extends BaseTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Test
    public void testLoadUserByUsername() {
        User user = transactionTemplate.execute(status -> {
            User u = generateFlatEntityWithoutId(User.class);
            u.setUserRoles(new HashSet<>(IterableUtil.toCollection(userRoleRepository.findAll())));
            return userRepository.save(u);
        });

        UserDetails userDetails = userDetailsService.loadUserByUsername(user.getEmail());
        Assert.assertEquals(user.getEmail(), userDetails.getUsername());
        Assert.assertEquals(user.getEncodedPassword(), userDetails.getPassword());
        Assert.assertFalse(userDetails.getAuthorities().isEmpty());
        Assertions.assertThat(userDetails.getAuthorities()).extracting("authority")
                .containsExactlyInAnyOrder(user.getUserRoles().stream().map(ur -> ur.getType().toString()).toArray());

    }

    @Test(expected = UsernameNotFoundException.class)
    public void testUserNotFound() {
        userDetailsService.loadUserByUsername("wrong name");
    }
}
