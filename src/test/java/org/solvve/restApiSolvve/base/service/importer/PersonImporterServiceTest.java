package org.solvve.restapisolvve.base.service.importer;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.client.themoviedb.TheMovieDbClient;
import org.solvve.restapisolvve.base.client.themoviedb.dto.PersonReadDTO;
import org.solvve.restapisolvve.base.domain.CrewDepartment;
import org.solvve.restapisolvve.base.domain.GenderType;
import org.solvve.restapisolvve.base.domain.Person;
import org.solvve.restapisolvve.base.exception.ImportAlreadyPerformedException;
import org.solvve.restapisolvve.base.exception.ImportedEntityAlreadyExistException;
import org.solvve.restapisolvve.base.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.UUID;

import static org.junit.Assert.*;

public class PersonImporterServiceTest extends BaseTest {

    @MockBean
    private TheMovieDbClient movieDbClient;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private PersonImporterService personImporterService;

    @Test
    public void testPersonImport() throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {
        String personExternalId = "id1";

        PersonReadDTO read = generator.generateRandomObject(PersonReadDTO.class);
        Mockito.when(movieDbClient.getPerson(personExternalId)).thenReturn(read);

        UUID personId = personImporterService.importPerson(personExternalId);
        Person person = personRepository.findById(personId).get();

        assertEquals(read.getName(), person.getName());
        assertEquals(read.getBiography(), person.getBiography());
        assertEquals(read.getBirthday(), person.getBirthDate());
        assertEquals(read.getPlaceOfBirth(), person.getBirthPlace());
        assertEquals(read.getDeathday(), person.getDeathDate());
        assertEquals(CrewDepartment.OTHER, person.getKnownForDepartment());
        assertNull(person.getGender());
    }

    @Test
    public void testPersonImportWhenPersonAlreadyExist() {
        String personExternalId = "id1";

        Person existingPerson = flatGenerator.generateRandomObject(Person.class);
        existingPerson = personRepository.save(existingPerson);

        PersonReadDTO read = generator.generateRandomObject(PersonReadDTO.class);
        read.setName(existingPerson.getName());
        read.setBirthday(existingPerson.getBirthDate());

        Mockito.when(movieDbClient.getPerson(personExternalId)).thenReturn(read);

        ImportedEntityAlreadyExistException ex = Assertions
                .catchThrowableOfType(() -> personImporterService.importPerson(personExternalId),
                        ImportedEntityAlreadyExistException.class);
        Assert.assertEquals(Person.class, ex.getEntityClass());
        Assert.assertEquals(existingPerson.getId(), ex.getEntityId());
    }

    @Test
    public void testNoExceptionWhenPersonImportWithDifferentBirthday()
            throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {
        String personExternalId = "id1";

        Person existingPerson = flatGenerator.generateRandomObject(Person.class);
        existingPerson = personRepository.save(existingPerson);

        PersonReadDTO read = generator.generateRandomObject(PersonReadDTO.class);
        read.setName(existingPerson.getName());
        read.setBirthday(existingPerson.getBirthDate().minusDays(1));
        Mockito.when(movieDbClient.getPerson(personExternalId)).thenReturn(read);
        UUID importedId = personImporterService.importPerson(personExternalId);

        Person importedPerson = personRepository.findById(importedId).get();

        assertNotEquals(existingPerson.getId(), importedId);
        assertNotEquals(existingPerson.getBirthDate(), importedPerson.getBirthDate());
        assertEquals(existingPerson.getName(), importedPerson.getName());
    }

    @Test
    public void testNoCallToClientOnDuplicateImport()
            throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {
        String personExternalId = "id1";

        PersonReadDTO read = generator.generateRandomObject(PersonReadDTO.class);
        Mockito.when(movieDbClient.getPerson(personExternalId)).thenReturn(read);

        personImporterService.importPerson(personExternalId);
        Mockito.verify(movieDbClient).getPerson(personExternalId);
        Mockito.reset(movieDbClient);

        Assertions.assertThatThrownBy(() -> personImporterService.importPerson(personExternalId))
                .isInstanceOf(ImportAlreadyPerformedException.class);

        Mockito.verifyNoInteractions(movieDbClient);
    }

    @Test
    public void testPersonImportWithFemaleGender()
            throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {
        String personExternalId = "id1";

        PersonReadDTO read = generator.generateRandomObject(PersonReadDTO.class);
        read.setGender(1);
        Mockito.when(movieDbClient.getPerson(personExternalId)).thenReturn(read);

        UUID personId = personImporterService.importPerson(personExternalId);
        Person person = personRepository.findById(personId).get();

        assertEquals(read.getName(), person.getName());
        assertEquals(GenderType.FEMALE, person.getGender());
    }

    @Test
    public void testPersonImportWithNotDefinedGender()
            throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {
        String personExternalId = "id1";

        PersonReadDTO read = generator.generateRandomObject(PersonReadDTO.class);
        read.setGender(0);
        Mockito.when(movieDbClient.getPerson(personExternalId)).thenReturn(read);

        UUID personId = personImporterService.importPerson(personExternalId);
        Person person = personRepository.findById(personId).get();

        assertEquals(read.getName(), person.getName());
        assertNull(person.getGender());
    }

    @Test
    public void testGetCrewDepartmentByName() {
        CrewDepartment d1 = ImportUtil.getCrewDepartmentByName("Directing");
        CrewDepartment d2 = ImportUtil.getCrewDepartmentByName("Unknown");
        CrewDepartment d3 = ImportUtil.getCrewDepartmentByName("WRITING");

        assertEquals(CrewDepartment.DIRECTING, d1);
        assertEquals(CrewDepartment.OTHER, d2);
        assertEquals(CrewDepartment.WRITING, d3);
    }

}
