package org.solvve.restapisolvve.base.repository;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.Crew;
import org.solvve.restapisolvve.base.domain.CrewDepartment;
import org.solvve.restapisolvve.base.domain.Movie;
import org.solvve.restapisolvve.base.domain.Person;
import org.solvve.restapisolvve.base.util.EntityCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class CrewRepositoryTest extends BaseTest {

    @Autowired
    private CrewRepository crewRepository;

    @Autowired
    private EntityCreator entityCreator;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testCreatedAtIsSet() {
        Crew crew = entityCreator.createCrew();

        Instant createdAtBeforeReload = crew.getCreatedAt();
        assertNotNull(createdAtBeforeReload);
        crew = crewRepository.findById(crew.getId()).get();

        Instant createdAtAfterReload = crew.getCreatedAt();
        assertNotNull(createdAtAfterReload);
        assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        Crew crew = entityCreator.createCrew();

        Instant updatedAtBeforeReload = crew.getUpdatedAt();
        assertNotNull(updatedAtBeforeReload);
        crew = crewRepository.findById(crew.getId()).get();

        Instant updatedAtAfterReload = crew.getUpdatedAt();
        assertNotNull(updatedAtAfterReload);
        assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        crew.setDepartment(CrewDepartment.DIRECTING);
        crewRepository.save(crew);
        crew = crewRepository.findById(crew.getId()).get();
        Instant updatedAtAfterModificationAndReload = crew.getUpdatedAt();
        assertTrue(updatedAtAfterModificationAndReload.isAfter(updatedAtAfterReload));
    }

    @Test
    public void testFindByMovieNameAndPersonNameAndJob() {
        entityCreator.createCrew();
        Person person = entityCreator.createPerson();
        Movie movie = entityCreator.createMovie();
        Crew crew = generateFlatEntityWithoutId(Crew.class);
        crew.setPerson(person);
        crew.setMovie(movie);
        crew.setJob("Fantastic job");

        transactionTemplate.executeWithoutResult(status -> {
            crewRepository.save(crew);
            Crew result = crewRepository.findByMovieNameAndPersonNameAndJob(movie.getName(), person.getName(),
                    "Fantastic job");
            Assertions.assertThat(result).isEqualToIgnoringGivenFields(crew, "person", "movie");
            assertEquals(person, result.getPerson());
            assertEquals(movie, result.getMovie());
        });
    }

    @Test
    public void testFindByIdAndMovieId() {
        Movie movie1 = entityCreator.createMovie();
        Movie movie2 = entityCreator.createMovie();

        entityCreator.createCrew(movie1);
        Crew crew2 = entityCreator.createCrew(movie2);

        Crew actual = crewRepository.findByIdAndMovieId(crew2.getId(), crew2.getMovie().getId());

        Assertions.assertThat(actual.getId()).isEqualTo(crew2.getId());
        Assertions.assertThat(actual.getMovie().getId()).isEqualTo(crew2.getMovie().getId());
    }

    @Test
    public void testFindAllByMovieId() {
        Movie movie1 = entityCreator.createMovie();
        Movie movie2 = entityCreator.createMovie();

        entityCreator.createCrew(movie1);
        entityCreator.createCrew(movie1);
        Crew crew3 = entityCreator.createCrew(movie2);
        Crew crew4 = entityCreator.createCrew(movie2);

        List<Crew> actual = crewRepository.findAllByMovieId(crew3.getMovie().getId());
        Assertions.assertThat(actual)
                .extracting(Crew::getId)
                .isEqualTo(Arrays.asList(crew3.getId(), crew4.getId()));
        Assertions.assertThat(actual)
                .extracting(Crew::getMovie)
                .extracting("id")
                .isEqualTo(Arrays.asList(crew3.getMovie().getId(), crew4.getMovie().getId()));
    }
}
