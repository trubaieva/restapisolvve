package org.solvve.restapisolvve.base.service;

import org.solvve.restapisolvve.base.domain.User;
import org.solvve.restapisolvve.base.dto.user.UserCreateDTO;
import org.solvve.restapisolvve.base.dto.user.UserPatchDTO;
import org.solvve.restapisolvve.base.dto.user.UserPutDTO;
import org.solvve.restapisolvve.base.dto.user.UserReadDTO;
import org.solvve.restapisolvve.base.repository.RepositoryHelper;
import org.solvve.restapisolvve.base.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class UserService {

    @Autowired
    RepositoryHelper repositoryHelper;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TranslationService translationService;

    public UserReadDTO getUser(UUID id) {
        User user = repositoryHelper.getEntityIfExist(User.class, id);
        return translationService.translate(user, UserReadDTO.class);
    }

    public List<UserReadDTO> getUsers() {
        List<UserReadDTO> dtos = new ArrayList<>();
        Iterable<User> userIterable = userRepository.findAll();
        for (User user : userIterable) {
            dtos.add(translationService.translate(user, UserReadDTO.class));
        }
        return dtos;
    }

    public UserReadDTO createUser(UserCreateDTO create) {
        User user = translationService.translate(create, User.class);
        user = userRepository.save(user);
        return translationService.translate(user, UserReadDTO.class);
    }

    public UserReadDTO patchUser(UUID id, UserPatchDTO patchDTO) {
        User user = repositoryHelper.getEntityIfExist(User.class, id);

        translationService.map(patchDTO, user);

        user = userRepository.save(user);
        return translationService.translate(user, UserReadDTO.class);
    }

    public UserReadDTO updateUser(UUID id, UserPutDTO putDTO) {
        User user = repositoryHelper.getEntityIfExist(User.class, id);

        translationService.map(putDTO, user);

        user = userRepository.save(user);
        return translationService.translate(user, UserReadDTO.class);
    }

    public void deleteUser(UUID id) {
        userRepository.delete(repositoryHelper.getEntityIfExist(User.class, id));
    }

}
