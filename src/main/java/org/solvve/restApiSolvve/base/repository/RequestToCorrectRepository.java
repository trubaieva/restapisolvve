package org.solvve.restapisolvve.base.repository;

import org.solvve.restapisolvve.base.domain.RequestToCorrect;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface RequestToCorrectRepository extends CrudRepository<RequestToCorrect, UUID>,
        RequestToCorrectRepositoryCustom {
    RequestToCorrect findByIdAndVerifierId(UUID id, UUID cmId);
}
