package org.solvve.restapisolvve.base.repository;

import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.RegisteredUser;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;

import static org.junit.Assert.*;

public class RegisteredUserRepositoryTest extends BaseTest {

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Test
    public void testCreatedAtIsSet() {
        RegisteredUser r = new RegisteredUser();
        r = registeredUserRepository.save(r);

        Instant createdAtBeforeReload = r.getCreatedAt();
        assertNotNull(createdAtBeforeReload);
        r = registeredUserRepository.findById(r.getId()).get();

        Instant createdAtAfterReload = r.getCreatedAt();
        assertNotNull(createdAtAfterReload);
        assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSetAfterChildFiledModification() {
        RegisteredUser r = new RegisteredUser();
        r = registeredUserRepository.save(r);

        Instant updatedAtBeforeReload = r.getUpdatedAt();
        assertNotNull(updatedAtBeforeReload);
        r = registeredUserRepository.findById(r.getId()).get();

        Instant updatedAtAfterReload = r.getUpdatedAt();
        assertNotNull(updatedAtAfterReload);
        assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        r.setActivityRating(5.6);
        registeredUserRepository.save(r);
        r = registeredUserRepository.findById(r.getId()).get();
        Instant updatedAtAfterModificationAndReload = r.getUpdatedAt();
        assertTrue(updatedAtAfterModificationAndReload.isAfter(updatedAtAfterReload));
    }

    @Test
    public void testUpdatedAtIsSetAfterParentFiledModification() {
        RegisteredUser r = new RegisteredUser();
        r = registeredUserRepository.save(r);

        Instant updatedAtBeforeReload = r.getUpdatedAt();
        assertNotNull(updatedAtBeforeReload);
        r = registeredUserRepository.findById(r.getId()).get();

        Instant updatedAtAfterReload = r.getUpdatedAt();
        assertNotNull(updatedAtAfterReload);
        assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        r.setName("Sveta");
        registeredUserRepository.save(r);
        r = registeredUserRepository.findById(r.getId()).get();
        Instant updatedAtAfterModificationAndReload = r.getUpdatedAt();
        assertTrue(updatedAtAfterModificationAndReload.isAfter(updatedAtAfterReload));
    }
}
