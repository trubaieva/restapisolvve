package org.solvve.restapisolvve.base.job;

import lombok.extern.slf4j.Slf4j;
import org.solvve.restapisolvve.base.repository.ActorRoleRepository;
import org.solvve.restapisolvve.base.service.ActorRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Component
public class UpdateAverageRatingOfActorRolesJob {

    @Autowired
    private ActorRoleRepository actorRoleRepository;

    @Autowired
    private ActorRoleService actorRoleService;

    @Transactional(readOnly = true)
    @Scheduled(cron = "${update.average.rating.of.actor.roles.job.cron}")
    public void updateAverageRatingOfActorRoles() {
        log.info("Job started");

        actorRoleRepository.getIdsFromRoles().forEach(roleId -> {
            try {
                actorRoleService.updateAverageRating(roleId);
            } catch (Exception e) {
                log.error("Failed to update average rate for actor role: {}", roleId, e);
            }
        });

        log.info("Job finished");
    }
}
