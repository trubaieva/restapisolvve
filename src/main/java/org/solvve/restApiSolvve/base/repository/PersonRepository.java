package org.solvve.restapisolvve.base.repository;

import org.solvve.restapisolvve.base.domain.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.UUID;

@Repository
public interface PersonRepository extends CrudRepository<Person, UUID> {

    Person findByName(String name);

    Person findByNameAndBirthDate(String name, LocalDate birthDate);
}
