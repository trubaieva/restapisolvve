package org.solvve.restapisolvve.base.repository;

import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.CheckRequest;
import org.solvve.restapisolvve.base.domain.CheckStatus;
import org.solvve.restapisolvve.base.util.EntityCreator;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;

import static org.junit.Assert.*;

public class CheckRequestRepositoryTest extends BaseTest {

    @Autowired
    private CheckRequestRepository checkRequestRepository;

    @Autowired
    private EntityCreator entityCreator;

    @Test
    public void testCreatedAtIsSet() {
        CheckRequest checkRequest = entityCreator.createCheckRequest();

        Instant createdAtBeforeReload = checkRequest.getCreatedAt();
        assertNotNull(createdAtBeforeReload);
        checkRequest = checkRequestRepository.findById(checkRequest.getId()).get();

        Instant createdAtAfterReload = checkRequest.getCreatedAt();
        assertNotNull(createdAtAfterReload);
        assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        CheckRequest checkRequest = entityCreator.createCheckRequest();

        Instant updatedAtBeforeReload = checkRequest.getUpdatedAt();
        assertNotNull(updatedAtBeforeReload);
        checkRequest = checkRequestRepository.findById(checkRequest.getId()).get();

        Instant updatedAtAfterReload = checkRequest.getUpdatedAt();
        assertNotNull(updatedAtAfterReload);
        assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        checkRequest.setStatus(CheckStatus.CORRECTED);
        checkRequestRepository.save(checkRequest);
        checkRequest = checkRequestRepository.findById(checkRequest.getId()).get();
        Instant updatedAtAfterModificationAndReload = checkRequest.getUpdatedAt();
        assertTrue(updatedAtAfterModificationAndReload.isAfter(updatedAtAfterReload));
    }

}
