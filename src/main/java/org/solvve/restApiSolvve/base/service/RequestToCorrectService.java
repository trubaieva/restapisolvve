package org.solvve.restapisolvve.base.service;

import org.solvve.restapisolvve.base.domain.*;
import org.solvve.restapisolvve.base.dto.PageResult;
import org.solvve.restapisolvve.base.dto.requesttocorrect.RequestToCorrectCreateDTO;
import org.solvve.restapisolvve.base.dto.requesttocorrect.RequestToCorrectFilter;
import org.solvve.restapisolvve.base.dto.requesttocorrect.RequestToCorrectFixDTO;
import org.solvve.restapisolvve.base.dto.requesttocorrect.RequestToCorrectReadDTO;
import org.solvve.restapisolvve.base.exception.EntityNotFoundException;
import org.solvve.restapisolvve.base.exception.UnexpectedRequestStatusException;
import org.solvve.restapisolvve.base.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.UUID;

@Service
public class RequestToCorrectService {

    @Autowired
    RepositoryHelper repositoryHelper;

    @Autowired
    private RequestToCorrectRepository requestToCorrectRepository;

    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private TranslationService translationService;

    public PageResult<RequestToCorrectReadDTO> getRequestsToCorrect(RequestToCorrectFilter filter, Pageable pageable) {
        Page<RequestToCorrect> requests = requestToCorrectRepository.findByFilter(filter, pageable);
        return translationService.toPageResult(requests, RequestToCorrectReadDTO.class);
    }

    public RequestToCorrectReadDTO getRequestToCorrect(UUID id) {
        RequestToCorrect request = repositoryHelper.getEntityIfExist(RequestToCorrect.class, id);
        return translationService.translate(request, RequestToCorrectReadDTO.class);
    }

    public RequestToCorrectReadDTO createRequestToCorrect(UUID userId, RequestToCorrectCreateDTO create) {
        RequestToCorrect request = translationService.translate(create, RequestToCorrect.class);
        request.setStatus(CorrectStatus.NEED_TO_VERIFY);
        request.setCreator(repositoryHelper.getReferenceIfExist(RegisteredUser.class, userId));
        request = requestToCorrectRepository.save(request);
        return translationService.translate(request, RequestToCorrectReadDTO.class);
    }

    public RequestToCorrectReadDTO fixRequestToCorrect(UUID cmId, UUID id, RequestToCorrectFixDTO fix) {
        RequestToCorrect request = getRequestIfStatusIsNeedToVerify(cmId, id);
        ObjectToCorrect object = request.getObjectType();
        String fieldWithWrongData;
        if (object == ObjectToCorrect.MOVIE) {
            Movie movie = repositoryHelper.getEntityIfExist(Movie.class, request.getObjectId());
            fieldWithWrongData = movie.getStoryLine();

            if (fieldWithWrongData != null && fieldWithWrongData.contains(request.getWrongData())) {
                String fixedData = fieldWithWrongData.replaceAll(request.getWrongData(), fix.getFix());
                movie.setStoryLine(fixedData);
                movieRepository.save(movie);
                request.setCorrectData(fix.getFix());
                request.setFixedAt(Instant.now());
                request.setStatus(CorrectStatus.FIXED);
            } else {
                request.setStatus(CorrectStatus.CANCEL);
            }

        } else if (object == ObjectToCorrect.PERSON) {
            Person person = repositoryHelper.getEntityIfExist(Person.class, request.getObjectId());
            fieldWithWrongData = person.getBiography();

            if (fieldWithWrongData != null && fieldWithWrongData.contains(request.getWrongData())) {
                String fixedData = fieldWithWrongData.replaceAll(request.getWrongData(), fix.getFix());
                person.setBiography(fixedData);
                personRepository.save(person);
                request.setCorrectData(fix.getFix());
                request.setFixedAt(Instant.now());
                request.setStatus(CorrectStatus.FIXED);
            } else {
                request.setStatus(CorrectStatus.CANCEL);
            }

        } else if (object == ObjectToCorrect.NEWS) {
            News news = repositoryHelper.getEntityIfExist(News.class, request.getObjectId());
            fieldWithWrongData = news.getText();

            if (fieldWithWrongData != null && fieldWithWrongData.contains(request.getWrongData())) {
                String fixedData = fieldWithWrongData.replaceAll(request.getWrongData(), fix.getFix());
                news.setText(fixedData);
                newsRepository.save(news);
                request.setCorrectData(fix.getFix());
                request.setFixedAt(Instant.now());
                request.setStatus(CorrectStatus.FIXED);
            } else {
                request.setStatus(CorrectStatus.CANCEL);
            }
        }

        request.setVerifier(repositoryHelper.getReferenceIfExist(ContentManager.class, cmId));
        request = requestToCorrectRepository.save(request);
        return translationService.translate(request, RequestToCorrectReadDTO.class);
    }

    public RequestToCorrectReadDTO cancelRequestToCorrect(UUID cmId, UUID id) {
        RequestToCorrect request = getRequestIfStatusIsNeedToVerify(cmId, id);
        request.setStatus(CorrectStatus.CANCEL);
        request.setVerifier(repositoryHelper.getReferenceIfExist(ContentManager.class, cmId));
        request = requestToCorrectRepository.save(request);
        return translationService.translate(request, RequestToCorrectReadDTO.class);
    }

    private RequestToCorrect getRequestIfStatusIsNeedToVerify(UUID cmId, UUID id) {
        RequestToCorrect request = getCMRequestToCorrectRequired(cmId, id);
        if (request.getStatus() != CorrectStatus.NEED_TO_VERIFY) {
            throw new UnexpectedRequestStatusException(RequestToCorrect.class, id);
        }
        return request;
    }

    private RequestToCorrect getCMRequestToCorrectRequired(UUID cmId, UUID id) {
        RequestToCorrect request = requestToCorrectRepository.findByIdAndVerifierId(id, cmId);
        if (request == null) {
            throw new EntityNotFoundException(RequestToCorrect.class, id);
        }
        return request;
    }
}
