package org.solvve.restapisolvve.base.service;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.Actor;
import org.solvve.restapisolvve.base.domain.ActorRole;
import org.solvve.restapisolvve.base.domain.Movie;
import org.solvve.restapisolvve.base.domain.RegisteredUser;
import org.solvve.restapisolvve.base.dto.actorrole.ActorRoleCreateDTO;
import org.solvve.restapisolvve.base.dto.actorrole.ActorRolePatchDTO;
import org.solvve.restapisolvve.base.dto.actorrole.ActorRolePutDTO;
import org.solvve.restapisolvve.base.dto.actorrole.ActorRoleReadDTO;
import org.solvve.restapisolvve.base.exception.EntityNotFoundException;
import org.solvve.restapisolvve.base.repository.ActorRoleRepository;
import org.solvve.restapisolvve.base.util.DtoCreator;
import org.solvve.restapisolvve.base.util.EntityCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.UUID;

public class ActorRoleServiceTest extends BaseTest {

    @Autowired
    private ActorRoleRepository actorRoleRepository;

    @Autowired
    private ActorRoleService actorRoleService;

    @Autowired
    private EntityCreator entityCreator;

    @Autowired
    private DtoCreator dtoCreator;

    @Autowired
    private TransactionTemplate transactionTemplate;

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status -> {
            runnable.run();
        });
    }

    @Test
    public void testGetMovieActorRole() {
        ActorRole actorRole = entityCreator.createActorRole();

        ActorRoleReadDTO readDTO = actorRoleService.getMovieActorRole(actorRole.getMovie().getId(), actorRole.getId());

        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(actorRole,
                "actorId", "movieId");
        Assert.assertEquals(readDTO.getMovieId(), actorRole.getMovie().getId());
        Assert.assertEquals(readDTO.getActorId(), actorRole.getActor().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetActorRoleWrongId() {
        Movie movie = entityCreator.createMovie();
        actorRoleService.getMovieActorRole(UUID.randomUUID(), movie.getId());
    }

    @Test
    public void testCreateMovieActorRole() {
        ActorRoleCreateDTO create = dtoCreator.createActorRoleCreateDTO();

        Actor actor = entityCreator.createActor();
        Movie movie = entityCreator.createMovie();

        create.setActorId(actor.getId());

        ActorRoleReadDTO read = actorRoleService.createMovieActorRole(movie.getId(), create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        ActorRole actorRole = actorRoleRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(actorRole,
                "actorId", "movieId");
        Assert.assertEquals(read.getMovieId(), actorRole.getMovie().getId());
        Assert.assertEquals(read.getActorId(), actorRole.getActor().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testCreateMovieActorRoleWithWrongMovie() {
        ActorRoleCreateDTO create = dtoCreator.createActorRoleCreateDTO();

        actorRoleService.createMovieActorRole(UUID.randomUUID(), create);
    }

    @Test
    public void testPatchMovieActorRole() {
        ActorRole actorRole = entityCreator.createActorRole();

        ActorRolePatchDTO patch = dtoCreator.createActorRolePatchDTO();

        Actor actor = entityCreator.createActor();
        patch.setActorId(actor.getId());

        ActorRoleReadDTO read = actorRoleService.patchMovieActorRole(actorRole.getMovie().getId(),
                actorRole.getId(),
                patch
        );

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        actorRole = actorRoleRepository.findById(read.getId()).get();

        Assertions.assertThat(read).isEqualToIgnoringGivenFields(actorRole,
                "actorId", "movieId");
        Assert.assertEquals(read.getMovieId(), actorRole.getMovie().getId());
        Assert.assertEquals(read.getActorId(), actorRole.getActor().getId());
    }

    @Test
    public void testPatchActorRoleEmptyPatch() {
        ActorRole actorRole = entityCreator.createActorRole();

        ActorRolePatchDTO patch = new ActorRolePatchDTO();

        ActorRoleReadDTO read = actorRoleService.patchMovieActorRole(actorRole.getMovie().getId(),
                actorRole.getId(),
                patch
        );

        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        inTransaction(() -> {
            ActorRole actorRoleAfterUpdate = actorRoleRepository.findById(read.getId()).get();
            Assertions.assertThat(read).hasNoNullFieldsOrProperties();
            Assertions.assertThat(actorRole)
                    .isEqualToIgnoringGivenFields(actorRoleAfterUpdate,
                            "actor", "movie", "roleReviews");
        });
    }

    @Test
    public void testUpdateMovieActorRole() {
        ActorRole actorRole = entityCreator.createActorRole();

        ActorRolePutDTO putDTO = dtoCreator.createActorRolePutDTO();

        Actor actor = entityCreator.createActor();
        putDTO.setActorId(actor.getId());

        ActorRoleReadDTO read = actorRoleService.updateMovieActorRole(actorRole.getMovie().getId(),
                actorRole.getId(),
                putDTO
        );

        Assertions.assertThat(putDTO).isEqualToComparingFieldByField(read);

        actorRole = actorRoleRepository.findById(read.getId()).get();
        Assertions.assertThat(actorRole)
                .isEqualToIgnoringGivenFields(read,
                        "actor", "movie", "roleReviews");
        Assert.assertEquals(read.getMovieId(), actorRole.getMovie().getId());
        Assert.assertEquals(read.getActorId(), actorRole.getActor().getId());
    }

    @Test
    public void testUpdateActorRoleEmptyPut() {
        ActorRole actorRole = entityCreator.createActorRole();

        ActorRolePutDTO putDTO = new ActorRolePutDTO();

        ActorRoleReadDTO read = actorRoleService.updateMovieActorRole(actorRole.getMovie().getId(),
                actorRole.getId(),
                putDTO
        );

        Assertions.assertThat(read).hasAllNullFieldsOrPropertiesExcept("id", "actorId", "movieId",
                "createdAt", "updatedAt");

        inTransaction(() -> {
            ActorRole actorRoleAfterUpdate = actorRoleRepository.findById(read.getId()).get();
            Assertions.assertThat(actorRoleAfterUpdate)
                    .hasAllNullFieldsOrPropertiesExcept("id", "actor", "movie", "roleReviews",
                            "createdAt", "updatedAt");
        });
    }

    @Test
    public void testDeleteMovieActorRole() {
        ActorRole actorRole = entityCreator.createActorRole();

        actorRoleService.deleteMovieActorRole(actorRole.getMovie().getId(), actorRole.getId());
        Assert.assertFalse(actorRoleRepository.existsById(actorRole.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteActorRoleNotFound() {
        actorRoleService.deleteMovieActorRole(UUID.randomUUID(), UUID.randomUUID());
    }

    @Test
    public void testUpdateAverageRating() {
        RegisteredUser creator = entityCreator.createRegisteredUser();
        ActorRole actorRole = entityCreator.createActorRole();

        entityCreator.createRoleRate(creator, actorRole, 3);
        entityCreator.createRoleRate(creator, actorRole, 5);

        actorRoleService.updateAverageRating((actorRole.getId()));
        actorRole = actorRoleRepository.findById(actorRole.getId()).get();
        Assert.assertEquals(4.0, actorRole.getAvgRating(), Double.MIN_NORMAL);
    }
}
