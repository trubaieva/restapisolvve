package org.solvve.restapisolvve.base.client.themoviedb.dto;

import lombok.Data;

@Data
public class ProductionCompanyReadDTO {
    private String id;
    private String name;
}
