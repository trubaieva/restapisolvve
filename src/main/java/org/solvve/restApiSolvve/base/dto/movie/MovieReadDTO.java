package org.solvve.restapisolvve.base.dto.movie;

import lombok.Data;

import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;

@Data
public class MovieReadDTO {
    private UUID id;
    private String name;
    private String country;
    private String language;
    private String storyLine;
    private LocalDate releaseDate;
    private Integer runtime;
    private Integer budget;
    private Integer revenue;
    private Boolean isReleased;
    private Double avgRating;
    private Double forecastRating;
    private Instant createdAt;
    private Instant updatedAt;
}
