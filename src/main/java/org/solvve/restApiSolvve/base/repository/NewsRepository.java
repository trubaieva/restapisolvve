package org.solvve.restapisolvve.base.repository;

import org.solvve.restapisolvve.base.domain.News;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface NewsRepository extends CrudRepository<News, UUID> {
}
