package org.solvve.restapisolvve.base.security;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.User;
import org.solvve.restapisolvve.base.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;

public class CurrentUserValidatorTest extends BaseTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CurrentUserValidator currentUserValidator;

    @MockBean
    private AuthenticationResolver authenticationResolver;

    @Test
    public void testIsCurrentUser() {
        User user = generateFlatEntityWithoutId(User.class);
        user = userRepository.save(user);

        Authentication authentication = new TestingAuthenticationToken(user.getEmail(), null);
        Mockito.when(authenticationResolver.getCurrentAuthentication()).thenReturn(authentication);

        Assert.assertTrue(currentUserValidator.isCurrentUser(user.getId()));
    }

    @Test
    public void testIsDifferentUser() {
        User user1 = generateFlatEntityWithoutId(User.class);
        user1 = userRepository.save(user1);
        User user2 = generateFlatEntityWithoutId(User.class);
        user2 = userRepository.save(user2);

        Authentication authentication = new TestingAuthenticationToken(user1.getEmail(), null);
        Mockito.when(authenticationResolver.getCurrentAuthentication()).thenReturn(authentication);

        Assert.assertFalse(currentUserValidator.isCurrentUser(user2.getId()));
    }
}
