package org.solvve.restapisolvve.base.dto.productioncompany;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class ProductionCompanyReadDTO {
    private UUID id;
    private String name;
    private Instant createdAt;
    private Instant updatedAt;
}
