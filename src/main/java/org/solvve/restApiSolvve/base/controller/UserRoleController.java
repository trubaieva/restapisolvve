package org.solvve.restapisolvve.base.controller;

import org.solvve.restapisolvve.base.controller.security.AdminOrCurrentUser;
import org.solvve.restapisolvve.base.dto.userrole.UserRoleReadDTO;
import org.solvve.restapisolvve.base.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class UserRoleController {

    @Autowired
    private UserRoleService userRoleService;

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("/roles")
    public List<UserRoleReadDTO> getAllRoles() {
        return userRoleService.getAllRoles();
    }

    @AdminOrCurrentUser
    //@PreAuthorize("hasAuthority('ADMIN') or (hasAuthority('REGISTERED_USER') and authentication.principal.id == #id)")
    @GetMapping("/users/{id}/roles")
    public List<UserRoleReadDTO> getUserRoles(@PathVariable UUID id) {
        return userRoleService.getUserRoles(id);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping("/users/{id}/roles/{roleId}")
    public List<UserRoleReadDTO> addRoleToUser(@PathVariable UUID id, @PathVariable UUID roleId) {
        return userRoleService.addRoleToUser(id, roleId);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @DeleteMapping("/users/{id}/roles/{roleId}")
    public List<UserRoleReadDTO> removeRoleFromUser(@PathVariable UUID id, @PathVariable UUID roleId) {
        return userRoleService.removeRoleFromUser(id, roleId);
    }
}
