package org.solvve.restapisolvve.base.repository;

import org.solvve.restapisolvve.base.domain.Admin;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface AdminRepository extends CrudRepository<Admin, UUID> {
    boolean existsByIdAndEmail(UUID id, String email);
}
