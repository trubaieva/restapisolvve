package org.solvve.restapisolvve.base.service;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.User;
import org.solvve.restapisolvve.base.dto.user.UserCreateDTO;
import org.solvve.restapisolvve.base.dto.user.UserPatchDTO;
import org.solvve.restapisolvve.base.dto.user.UserPutDTO;
import org.solvve.restapisolvve.base.dto.user.UserReadDTO;
import org.solvve.restapisolvve.base.exception.EntityNotFoundException;
import org.solvve.restapisolvve.base.repository.UserRepository;
import org.solvve.restapisolvve.base.util.DtoCreator;
import org.solvve.restapisolvve.base.util.EntityCreator;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UserServiceTest extends BaseTest {

    @Autowired
    EntityCreator entityCreator;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private DtoCreator dtoCreator;

    @Test
    public void testGetUser() {
        User user = entityCreator.createUser();

        UserReadDTO readDTO = userService.getUser(user.getId());
        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(user);
    }

    @Test
    public void testGetUsers() {
        List<User> users = new ArrayList<>();
        User user;
        for (int i = 0; i < 5; i++) {
            user = entityCreator.createUser();
            userRepository.save(user);
            users.add(user);
        }
        List<UserReadDTO> readDTOs = userService.getUsers();
        Assertions.assertThat(readDTOs)
                .extracting("id")
                .containsExactlyInAnyOrder(users.stream().map(User::getId).toArray());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetUserWrongId() {
        userService.getUser(UUID.randomUUID());
    }

    @Test
    public void testCreateUser() {
        UserCreateDTO create = dtoCreator.createUserCreateDTO();

        UserReadDTO read = userService.createUser(create);
        Assertions.assertThat(create).isEqualToIgnoringGivenFields(read, "password");
        Assert.assertNotNull(read.getId());

        User user = userRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(user, "password");
    }

    @Test
    public void testPatchUser() {
        User user = entityCreator.createUser();
        userRepository.save(user);

        UserPatchDTO patch = dtoCreator.createUserPatchDTO();
        UserReadDTO read = userService.patchUser(user.getId(), patch);

        Assertions.assertThat(patch).isEqualToIgnoringGivenFields(read, "password");

        user = userRepository.findById(read.getId()).get();
        Assertions.assertThat(user).isEqualToIgnoringGivenFields(read, "userRoles", "encodedPassword");
    }

    @Test
    public void testPatchUserEmptyPatch() {
        User user = entityCreator.createUser();
        userRepository.save(user);

        UserPatchDTO patch = new UserPatchDTO();
        UserReadDTO read = userService.patchUser(user.getId(), patch);

        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        User userAfterUpdate = userRepository.findById(read.getId()).get();

        Assertions.assertThat(read).hasNoNullFieldsOrProperties();
        Assertions.assertThat(user).isEqualToIgnoringGivenFields(userAfterUpdate, "userRoles");
    }

    @Test
    public void testPutUser() {
        User user = entityCreator.createUser();
        userRepository.save(user);

        UserPutDTO putDTO = dtoCreator.createUserPutDTO();
        UserReadDTO read = userService.updateUser(user.getId(), putDTO);

        Assertions.assertThat(putDTO).isEqualToIgnoringGivenFields(read, "password");

        user = userRepository.findById(read.getId()).get();
        Assertions.assertThat(user).isEqualToIgnoringGivenFields(read, "userRoles", "encodedPassword");
    }

    @Test
    public void testUpdateUserEmptyPut() {
        User user = entityCreator.createUser();
        userRepository.save(user);

        UserPutDTO putDTO = new UserPutDTO();
        UserReadDTO read = userService.updateUser(user.getId(), putDTO);

        Assertions.assertThat(read).hasAllNullFieldsOrPropertiesExcept("id", "createdAt", "updatedAt");

        User userAfterUpdate = userRepository.findById(read.getId()).get();

        Assertions.assertThat(userAfterUpdate).hasAllNullFieldsOrPropertiesExcept("id", "userRoles", "createdAt",
                "updatedAt", "encodedPassword");
    }

    @Test
    public void testDeleteUser() {
        User user = entityCreator.createUser();
        userRepository.save(user);

        userService.deleteUser(user.getId());
        Assert.assertFalse(userRepository.existsById(user.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteUserNotFound() {
        userService.deleteUser(UUID.randomUUID());
    }

}
