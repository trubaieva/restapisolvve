package org.solvve.restapisolvve.base.repository;

import org.solvve.restapisolvve.base.domain.Movie;
import org.solvve.restapisolvve.base.dto.movie.MovieFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface MovieRepositoryCustom {
    Page<Movie> findByFilter(MovieFilter filter, Pageable pageable);
}
