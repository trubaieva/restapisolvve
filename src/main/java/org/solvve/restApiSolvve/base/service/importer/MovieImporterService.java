package org.solvve.restapisolvve.base.service.importer;

import lombok.extern.slf4j.Slf4j;
import org.solvve.restapisolvve.base.client.themoviedb.TheMovieDbClient;
import org.solvve.restapisolvve.base.client.themoviedb.dto.*;
import org.solvve.restapisolvve.base.domain.*;
import org.solvve.restapisolvve.base.exception.ImportAlreadyPerformedException;
import org.solvve.restapisolvve.base.exception.ImportedEntityAlreadyExistException;
import org.solvve.restapisolvve.base.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Slf4j
@Service
public class MovieImporterService {

    @Autowired
    private TheMovieDbClient movieDbClient;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private ProductionCompanyRepository companyRepository;

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private ActorRoleRepository actorRoleRepository;

    @Autowired
    private CrewRepository crewRepository;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private ExternalSystemImportService externalSystemImportService;

    @Autowired
    private PersonImporterService personImporterService;

    @Transactional
    public UUID importMovie(String movieExternalId)
            throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {
        log.info("Importing movie with external id={}", movieExternalId);

        externalSystemImportService.validateNotImported(Movie.class, movieExternalId);
        MovieReadDTO read = movieDbClient.getMovie(movieExternalId);
        Movie existingMovie = movieRepository.findByNameAndReleaseYear(read.getTitle(),
                read.getReleaseDate().getYear());
        if (existingMovie != null) {
            throw new ImportedEntityAlreadyExistException(Movie.class, existingMovie.getId(), "Movie with name="
                    + read.getTitle() + " and release year=" + read.getReleaseDate().getYear() + " already exist");
        }

        Movie movie = new Movie();
        movie.setName(read.getTitle());
        movie.setCountry(read.getProductionCountries().get(0).getName());
        movie.setLanguage(read.getOriginalLanguage());
        movie.setStoryLine(read.getOverview());
        movie.setReleaseDate(read.getReleaseDate());
        movie.setRuntime(read.getRuntime());
        movie.setBudget(read.getBudget());
        movie.setRevenue(read.getRevenue());
        movie.setIsReleased(read.getStatus().equals("Released"));
        mapProductionCompanies(read.getProductionCompanies(), movie);
        mapGenres(read.getGenres(), movie);
        movie = movieRepository.save(movie);

        log.info("Importing credits for movie with external id={}", movieExternalId);
        CreditReadDTO creditReadDTO = movieDbClient.getMovieCredits(movieExternalId);
        importCast(creditReadDTO.getCast(), movieExternalId, movie);
        importCrew(creditReadDTO.getCrew(), movieExternalId, movie);
        log.info("Credits for movie with external id={} imported", movieExternalId);

        externalSystemImportService.createExternalSystemImport(movie, movieExternalId);

        log.info("Movie with external id={} imported", movieExternalId);
        return movie.getId();
    }

    private void mapProductionCompanies(List<ProductionCompanyReadDTO> externalCompanies, Movie movie) {
        for (ProductionCompanyReadDTO importedCompanyDTO : externalCompanies) {
            ProductionCompany company = companyRepository.findByName(importedCompanyDTO.getName());

            if (company == null) {
                company = new ProductionCompany();
                company.setName(importedCompanyDTO.getName());
                companyRepository.save(company);
            }
            movie.getProductionCompanies().add(company);
        }
    }

    private void mapGenres(List<GenreReadDTO> externalGenres, Movie movie) {
        for (GenreReadDTO importedGenreDTO : externalGenres) {
            Genre genre = genreRepository.findByName(importedGenreDTO.getName());

            if (genre == null) {
                genre = new Genre();
                genre.setName(importedGenreDTO.getName());
                genreRepository.save(genre);
            }
            movie.getGenres().add(genre);
        }
    }

    private void importCast(List<CastReadDTO> castDTOs, String movieExternalId, Movie movie)
            throws ImportAlreadyPerformedException {
        log.info("Importing cast for movie with external id={}", movieExternalId);

        for (CastReadDTO readCast : castDTOs) {
            ActorRole actorRole = new ActorRole();
            actorRole.setCharacter(readCast.getCharacter());
            actorRole.setCastOrder(readCast.getOrder());

            UUID internalPersonId;
            try {
                internalPersonId = personImporterService.importPerson(readCast.getId());
            } catch (ImportAlreadyPerformedException ex) {
                internalPersonId = ex.getExternalSystemImport().getEntityId();
            } catch (ImportedEntityAlreadyExistException ex) {
                internalPersonId = ex.getEntityId();
            }

            Person person = personRepository.findById(internalPersonId).get();

            Actor actor = actorRepository.findByPerson(person);
            if (actor == null) {
                actor = new Actor();
                actor.setPerson(person);
                actor = actorRepository.save(actor);
            }
            actorRole.setActor(actor);
            actorRole.setMovie(movie);
            actorRole = actorRoleRepository.save(actorRole);
            externalSystemImportService.createExternalSystemImport(actorRole, readCast.getCastId());
        }
        log.info("Cast for movie with external id={} imported", movieExternalId);
    }

    private void importCrew(List<CrewReadDTO> crewDTOs, String movieExternalId, Movie movie)
            throws ImportAlreadyPerformedException {
        log.info("Importing crew for movie with external id={}", movieExternalId);

        for (CrewReadDTO readCrew : crewDTOs) {
            Crew crew = new Crew();
            crew.setJob(readCrew.getJob());
            crew.setDepartment(ImportUtil.getCrewDepartmentByName(readCrew.getJob()));

            UUID internalPersonId;
            try {
                internalPersonId = personImporterService.importPerson(readCrew.getId());
            } catch (ImportAlreadyPerformedException ex) {
                internalPersonId = ex.getExternalSystemImport().getEntityId();
            } catch (ImportedEntityAlreadyExistException ex) {
                internalPersonId = ex.getEntityId();
            }

            Person person = personRepository.findById(internalPersonId).get();
            crew.setMovie(movie);
            crew.setPerson(person);

            crew = crewRepository.save(crew);
            externalSystemImportService.createExternalSystemImport(crew, readCrew.getCreditId());
        }
        log.info("Crew for movie with external id={} imported", movieExternalId);
    }
}
