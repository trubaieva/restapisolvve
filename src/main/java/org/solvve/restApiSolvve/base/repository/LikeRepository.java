package org.solvve.restapisolvve.base.repository;

import org.solvve.restapisolvve.base.domain.Like;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface LikeRepository extends CrudRepository<Like, UUID> {
}
