package org.solvve.restapisolvve.base.client.themoviedb;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.client.themoviedb.dto.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class TheMovieDbClientTest extends BaseTest {

    @Autowired
    private TheMovieDbClient theMovieDbClient;

    @Test
    public void testGetMovieRu() {
        String movieId = "280";
        MovieReadDTO dto = theMovieDbClient.getMovie(movieId, "ru");

        assertEquals(movieId, dto.getId());
        assertEquals("Terminator 2: Judgment Day", dto.getOriginalTitle());
        assertEquals("Терминатор 2: Судный день", dto.getTitle());

        assertEquals("United States of America", dto.getProductionCountries().get(0).getName());
        assertEquals("en", dto.getOriginalLanguage());
        assertEquals("Война роботов и людей продолжается. Казалось, что человечество обречено на полное "
                        + "уничтожение. Но благодаря своему лидеру Джону Коннору у сопротивления появляется шанс "
                        + "победить. Не имея возможности убить Джона в реальном времени, роботы отправляют в прошлое "
                        + "свою самую совершенную разработку — терминатора-убийцу из жидкого металла, способного "
                        + "принимать любое обличье.",
                dto.getOverview());
        assertEquals(LocalDate.of(1991, 07, 03), dto.getReleaseDate());
        Assertions.assertThat(dto.getRuntime()).isGreaterThanOrEqualTo(137);
        assertEquals(102000000, (int) dto.getBudget());
        assertEquals(520000000, (int) dto.getRevenue());
        assertEquals("Released", dto.getStatus());

        assertThat(dto.getGenres()).extracting("id", "name").contains(tuple("28", "боевик"),
                tuple("53", "триллер"),
                tuple("878", "фантастика"));

        assertThat(dto.getProductionCompanies()).extracting("id", "name").contains(
                tuple("61409", "T2 Productions"), tuple("574", "Lightstorm Entertainment"),
                tuple("1280", "Pacific Western"),
                tuple("275", "Carolco Pictures"), tuple("559", "TriStar Pictures"), tuple("694", "StudioCanal"));
    }

    @Test
    public void testGetMovieDefaultLanguage() {
        String movieId = "280";
        MovieReadDTO dto = theMovieDbClient.getMovie(movieId, null);

        assertEquals(movieId, dto.getId());
        assertEquals("Terminator 2: Judgment Day", dto.getOriginalTitle());
        assertEquals(dto.getOriginalTitle(), dto.getTitle());

        assertEquals("United States of America", dto.getProductionCountries().get(0).getName());
        assertEquals("en", dto.getOriginalLanguage());
        assertEquals("Nearly 10 years have passed since Sarah Connor was targeted for termination by "
                + "a cyborg from the future. Now her son, John, the future leader of the resistance, is "
                + "the target for a newer, more deadly terminator. Once again, the resistance has managed to send a "
                + "protector back to attempt to save John and his mother Sarah.", dto.getOverview());
        assertEquals(LocalDate.of(1991, 07, 03), dto.getReleaseDate());
        Assertions.assertThat(dto.getRuntime()).isGreaterThanOrEqualTo(137);
        assertEquals(102000000, (int) dto.getBudget());
        assertEquals(520000000, (int) dto.getRevenue());
        assertEquals("Released", dto.getStatus());

        assertThat(dto.getGenres()).extracting("id", "name").contains(tuple("28", "Action"),
                tuple("53", "Thriller"),
                tuple("878", "Science Fiction"));

        assertThat(dto.getProductionCompanies()).extracting("id", "name").contains(
                tuple("61409", "T2 Productions"), tuple("574", "Lightstorm Entertainment"),
                tuple("1280", "Pacific Western"),
                tuple("275", "Carolco Pictures"), tuple("559", "TriStar Pictures"), tuple("694", "StudioCanal"));
    }

    @Test
    public void testGetTopRatedMovies() {
        MoviesPageDTO moviesPage = theMovieDbClient.getTopRatedMovies();
        Assert.assertTrue(moviesPage.getTotalPages() > 0);
        Assert.assertTrue(moviesPage.getTotalResults() > 0);
        Assert.assertTrue(moviesPage.getResults().size() > 0);
        for (MovieReadShortDTO read : moviesPage.getResults()) {
            Assert.assertNotNull(read.getId());
            Assert.assertNotNull(read.getTitle());
        }
    }

    @Test
    public void testGetPersonRu() {
        String personId = "1100";
        PersonReadDTO dto = theMovieDbClient.getPerson(personId, "ru");

        assertEquals(personId, dto.getId());
        assertEquals("Arnold Schwarzenegger", dto.getName());
        assertEquals(LocalDate.of(1947, 07, 30), dto.getBirthday());
        assertEquals("Thal, Styria, Austria", dto.getPlaceOfBirth());
        assertEquals(2, (int) dto.getGender());
        assertNull(dto.getDeathday());
        assertEquals("Acting", dto.getKnownForDepartment());
        assertEquals(2679, dto.getBiography().length());
    }

    @Test
    public void testGetPersonDefaultLanguage() {
        String personId = "1100";
        PersonReadDTO dto = theMovieDbClient.getPerson(personId, null);

        assertEquals(personId, dto.getId());
        assertEquals(personId, dto.getId());
        assertEquals("Arnold Schwarzenegger", dto.getName());
        assertEquals(LocalDate.of(1947, 07, 30), dto.getBirthday());
        assertEquals("Thal, Styria, Austria", dto.getPlaceOfBirth());
        assertEquals(2, (int) dto.getGender());
        assertNull(dto.getDeathday());
        assertEquals("Acting", dto.getKnownForDepartment());
        assertEquals(833, dto.getBiography().length());
    }

    @Test
    public void testGetMovieCredits() {
        String movieId = "280";
        CreditReadDTO credit = theMovieDbClient.getMovieCredits(movieId);

        List<CastReadDTO> casts = credit.getCast();
        Assertions.assertThat(casts).doesNotHaveDuplicates();
        Assertions.assertThat(casts).isNotNull().isNotEmpty();

        List<CrewReadDTO> crews = credit.getCrew();
        Assertions.assertThat(crews).doesNotHaveDuplicates();
        Assertions.assertThat(crews).isNotNull().isNotEmpty();
    }
}