package org.solvve.restapisolvve.base.job;

import lombok.extern.slf4j.Slf4j;
import org.solvve.restapisolvve.base.repository.MovieRepository;
import org.solvve.restapisolvve.base.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Component
public class UpdateAverageRatingOfMoviesJob {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private MovieService movieService;

    @Transactional(readOnly = true)
    @Scheduled(cron = "${update.average.rating.of.movies.job.cron}")
    public void updateAverageRatingOfMovies() {
        log.info("Job started");

        movieRepository.getIdsFromMovies().forEach(movieId -> {
            try {
                movieService.updateAverageRating(movieId);
            } catch (Exception e) {
                log.error("Failed to update average rate for movie: {}", movieId, e);
            }
        });

        log.info("Job finished");
    }
}
