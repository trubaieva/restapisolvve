package org.solvve.restapisolvve.base.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.solvve.restapisolvve.base.domain.Genre;
import org.solvve.restapisolvve.base.dto.genre.GenreCreateDTO;
import org.solvve.restapisolvve.base.dto.genre.GenrePutDTO;
import org.solvve.restapisolvve.base.dto.genre.GenreReadDTO;
import org.solvve.restapisolvve.base.exception.EntityNotFoundException;
import org.solvve.restapisolvve.base.exception.handler.ErrorInfo;
import org.solvve.restapisolvve.base.service.GenreService;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(controllers = GenreController.class)
public class GenreControllerTest extends BaseControllerTest {

    @MockBean
    private GenreService genreService;

    @Test
    public void testGetGenres() throws Exception {
        List<GenreReadDTO> genreReadDTOs = new ArrayList<>();
        GenreReadDTO genreReadDTO;
        for (int i = 0; i < 5; i++) {
            genreReadDTO = generateObject(GenreReadDTO.class);
            genreReadDTOs.add(genreReadDTO);
        }
        Mockito.when(genreService.getGenres()).thenReturn(genreReadDTOs);

        String resultJson = mvc.perform(get("/api/v1/genres"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<GenreReadDTO> actualGenres = objectMapper.readValue(resultJson, new TypeReference<List<GenreReadDTO>>() {
        });
        Assertions.assertThat(actualGenres).isEqualTo(genreReadDTOs);

        Mockito.verify(genreService).getGenres();
    }

    @Test
    public void testGetGenre() throws Exception {
        GenreReadDTO genre = generateObject(GenreReadDTO.class);

        Mockito.when(genreService.getGenre(genre.getId())).thenReturn(genre);

        String resultJson = mvc.perform(get("/api/v1/genres/{id}", genre.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        GenreReadDTO actualGenre = objectMapper.readValue(resultJson, GenreReadDTO.class);
        Assertions.assertThat(actualGenre).isEqualToComparingFieldByField(genre);

        Mockito.verify(genreService).getGenre(genre.getId());
    }

    @Test
    public void testGetGenreWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(Genre.class, wrongId);
        Mockito.when(genreService.getGenre(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/genres/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetGenreWrongFormatId() throws Exception {
        String wrongFormatId = "123";
        ErrorInfo expectedError = new ErrorInfo(HttpStatus.BAD_REQUEST,
                MethodArgumentTypeMismatchException.class,
                "Failed to convert value of type 'java.lang.String' to required type " +
                        "'java.util.UUID'; nested exception is java.lang" +
                        ".IllegalArgumentException: Invalid UUID string: 123"
        );
        String resultJson = mvc.perform(get("/api/v1/genres/{id}", wrongFormatId))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        ErrorInfo actualError = objectMapper.readValue(resultJson, ErrorInfo.class);
        Assertions.assertThat(actualError).isEqualToComparingFieldByField(expectedError);
    }

    @Test
    public void testCreateGenre() throws Exception {
        GenreCreateDTO create = generateObject(GenreCreateDTO.class);

        GenreReadDTO read = generateObject(GenreReadDTO.class);

        Mockito.when(genreService.createGenre(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/genres")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        GenreReadDTO actualGenre = objectMapper.readValue(resultJson, GenreReadDTO.class);
        Assertions.assertThat(actualGenre).isEqualToComparingFieldByField(read);

        Mockito.verify(genreService).createGenre(create);
    }

    @Test
    public void testUpdateGenre() throws Exception {
        GenrePutDTO putDTO = generateObject(GenrePutDTO.class);

        GenreReadDTO read = generateObject(GenreReadDTO.class);

        Mockito.when(genreService.updateGenre(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/genres/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        GenreReadDTO actualGenre = objectMapper.readValue(resultJson, GenreReadDTO.class);
        Assert.assertEquals(read, actualGenre);

        Mockito.verify(genreService).updateGenre(read.getId(), putDTO);
    }

    @Test
    public void testDeleteGenre() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/genres/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(genreService).deleteGenre(id);
    }

    @Test
    public void testAddGenreToMovie() throws Exception {
        UUID movieId = UUID.randomUUID();
        UUID genreId = UUID.randomUUID();

        GenreReadDTO read = generateObject(GenreReadDTO.class);

        List<GenreReadDTO> expectedGenres = List.of(read);
        Mockito.when(genreService.addGenreToMovie(movieId, genreId)).thenReturn(expectedGenres);

        String resultJson = mvc.perform(post("/api/v1/movies/{movieId}/genres/{id}", movieId, genreId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<GenreReadDTO> actualGenres = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(expectedGenres, actualGenres);

        Mockito.verify(genreService).addGenreToMovie(movieId, genreId);
    }

    @Test
    public void testRemoveGenreFromMovie() throws Exception {
        UUID movieId = UUID.randomUUID();
        UUID genreId = UUID.randomUUID();

        List<GenreReadDTO> expectedGenres = new ArrayList<>();
        Mockito.when(genreService.removeGenreFromMovie(movieId, genreId)).thenReturn(expectedGenres);

        String resultJson = mvc.perform(delete("/api/v1/movies/{movieId}/genres/{id}", movieId, genreId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<GenreReadDTO> actualGenres = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(expectedGenres, actualGenres);

        Mockito.verify(genreService).removeGenreFromMovie(movieId, genreId);
    }

    @Test
    public void testRemoveGenreFromMovieWithSeveralGenres() throws Exception {
        UUID movieId = UUID.randomUUID();
        UUID genreId = UUID.randomUUID();

        GenreReadDTO read = generateObject(GenreReadDTO.class);
        List<GenreReadDTO> expectedGenres = List.of(read);

        Mockito.when(genreService.removeGenreFromMovie(movieId, genreId)).thenReturn(expectedGenres);

        String resultJson = mvc.perform(delete("/api/v1/movies/{movieId}/genres/{id}", movieId, genreId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<GenreReadDTO> actualGenres = objectMapper.readValue(resultJson, new TypeReference<>() {});
        Assert.assertEquals(expectedGenres, actualGenres);

        Mockito.verify(genreService).removeGenreFromMovie(movieId, genreId);
    }
}
