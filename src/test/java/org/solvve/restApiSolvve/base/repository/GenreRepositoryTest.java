package org.solvve.restapisolvve.base.repository;

import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.Genre;
import org.solvve.restapisolvve.base.util.EntityCreator;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;

import static org.junit.Assert.*;

public class GenreRepositoryTest extends BaseTest {

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private EntityCreator entityCreator;

    @Test
    public void testCreatedAtIsSet() {
        Genre genre = entityCreator.createGenre();

        Instant createdAtBeforeReload = genre.getCreatedAt();
        assertNotNull(createdAtBeforeReload);
        genre = genreRepository.findById(genre.getId()).get();

        Instant createdAtAfterReload = genre.getCreatedAt();
        assertNotNull(createdAtAfterReload);
        assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        Genre genre = entityCreator.createGenre();

        Instant updatedAtBeforeReload = genre.getUpdatedAt();
        assertNotNull(updatedAtBeforeReload);
        genre = genreRepository.findById(genre.getId()).get();

        Instant updatedAtAfterReload = genre.getUpdatedAt();
        assertNotNull(updatedAtAfterReload);
        assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        genre.setName("Tragedy");
        genreRepository.save(genre);
        genre = genreRepository.findById(genre.getId()).get();
        Instant updatedAtAfterModificationAndReload = genre.getUpdatedAt();
        assertTrue(updatedAtAfterModificationAndReload.isAfter(updatedAtAfterReload));
    }
}
