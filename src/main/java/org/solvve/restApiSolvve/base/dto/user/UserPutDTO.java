package org.solvve.restapisolvve.base.dto.user;

import lombok.Data;

@Data
public class UserPutDTO {
    private String name;
    private String email;
    private String password;
}
