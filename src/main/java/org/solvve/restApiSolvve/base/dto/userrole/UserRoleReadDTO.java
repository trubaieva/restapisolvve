package org.solvve.restapisolvve.base.dto.userrole;

import lombok.Data;
import org.solvve.restapisolvve.base.domain.UserRoleType;

import java.util.UUID;

@Data
public class UserRoleReadDTO {
    private UUID id;
    private UserRoleType type;
}
