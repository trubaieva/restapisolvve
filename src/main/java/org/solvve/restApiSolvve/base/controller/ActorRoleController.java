package org.solvve.restapisolvve.base.controller;

import org.solvve.restapisolvve.base.dto.actorrole.ActorRoleCreateDTO;
import org.solvve.restapisolvve.base.dto.actorrole.ActorRolePatchDTO;
import org.solvve.restapisolvve.base.dto.actorrole.ActorRolePutDTO;
import org.solvve.restapisolvve.base.dto.actorrole.ActorRoleReadDTO;
import org.solvve.restapisolvve.base.service.ActorRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/movies/{movieId}/actor-roles")
public class ActorRoleController {

    @Autowired
    private ActorRoleService actorRoleService;

    @PreAuthorize("permitAll()")
    @GetMapping
    public List<ActorRoleReadDTO> getMovieActorRoles(@PathVariable UUID movieId) {
        return actorRoleService.getMovieActorRoles(movieId);
    }

    @PreAuthorize("permitAll()")
    @GetMapping("/{id}")
    public ActorRoleReadDTO getMovieActorRole(@PathVariable UUID movieId, @PathVariable UUID id) {
        return actorRoleService.getMovieActorRole(movieId, id);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'CONTENT_MANAGER')")
    @PostMapping
    public ActorRoleReadDTO createMovieActorRole(@PathVariable UUID movieId,
            @RequestBody @Valid ActorRoleCreateDTO createDTO) {
        return actorRoleService.createMovieActorRole(movieId, createDTO);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'CONTENT_MANAGER')")
    @PutMapping("/{id}")
    public ActorRoleReadDTO updateMovieActorRole(@PathVariable UUID movieId, @PathVariable UUID id,
            @RequestBody @Valid ActorRolePutDTO putDTO) {
        return actorRoleService.updateMovieActorRole(movieId, id, putDTO);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'CONTENT_MANAGER')")
    @PatchMapping("/{id}")
    public ActorRoleReadDTO patchMovieActorRole(@PathVariable UUID movieId, @PathVariable UUID id,
            @RequestBody @Valid ActorRolePatchDTO patchDTO) {
        return actorRoleService.patchMovieActorRole(movieId, id, patchDTO);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'CONTENT_MANAGER')")
    @DeleteMapping("/{id}")
    public void deleteMovieActorRole(@PathVariable UUID movieId, @PathVariable UUID id) {
        actorRoleService.deleteMovieActorRole(movieId, id);
    }
}
