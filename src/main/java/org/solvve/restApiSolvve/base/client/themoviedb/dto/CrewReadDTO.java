package org.solvve.restapisolvve.base.client.themoviedb.dto;

import lombok.Data;

@Data
public class CrewReadDTO {

    private String creditId;
    private String id; //personId
    private String name;
    private String department;
    private String job;
}
