package org.solvve.restapisolvve.base.repository;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.Person;
import org.solvve.restapisolvve.base.exception.EntityNotFoundException;
import org.solvve.restapisolvve.base.util.EntityCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.UUID;

import static org.junit.Assert.assertEquals;

public class RepositoryHelperTest extends BaseTest {

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private EntityCreator entityCreator;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private TransactionTemplate transactionTemplate;

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status -> {
            runnable.run();
        });
    }

    @Test
    public void testValidateExists() {
        Person person = entityCreator.createPerson("Anna");
        repositoryHelper.validateExists(Person.class, person.getId());
        assertEquals(1, personRepository.count());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testValidateExistsThrowException() {
        repositoryHelper.validateExists(Person.class, UUID.randomUUID());
    }

    @Test
    public void testGetReferenceIfExist() {
        Person person = entityCreator.createPerson("Anna");
        inTransaction(() -> {
            Person proxy = repositoryHelper.getReferenceIfExist(Person.class, person.getId());
            assertEquals(person.getId(), proxy.getId());
            assertEquals("Anna", proxy.getName());
            Assertions.assertThat(proxy.getClass().toString()).contains("org.solvve.restapisolvve.base.domain" +
                    ".Person$HibernateProxy$");
        });
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetReferenceIfExistWithNonExistentEntity() {
        repositoryHelper.getReferenceIfExist(Person.class, UUID.randomUUID());
    }

    @Test
    public void testGetEntityIfExist() {
        Person person = entityCreator.createPerson("Anna");
        Person entity = repositoryHelper.getEntityIfExist(Person.class, person.getId());
        Assertions.assertThat(entity).isEqualToIgnoringGivenFields(person, "news");
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetEntityIfExistWithNonExistentEntity() {
        repositoryHelper.getEntityIfExist(Person.class, UUID.randomUUID());
    }

}
