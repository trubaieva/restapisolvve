package org.solvve.restapisolvve.base.repository;

import org.solvve.restapisolvve.base.domain.Crew;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface CrewRepository extends CrudRepository<Crew, UUID> {
    Crew findByMovieNameAndPersonNameAndJob(String movieName, String name, String job);

    Crew findByIdAndMovieId(UUID id, UUID movieId);

    List<Crew> findAllByMovieId(UUID movieId);
}
