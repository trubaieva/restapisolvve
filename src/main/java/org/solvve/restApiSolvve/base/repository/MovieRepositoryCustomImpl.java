package org.solvve.restapisolvve.base.repository;

import org.bitbucket.brunneng.qb.JpaQueryBuilder;
import org.bitbucket.brunneng.qb.SpringQueryBuilderUtils;
import org.solvve.restapisolvve.base.domain.Movie;
import org.solvve.restapisolvve.base.dto.movie.MovieFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class MovieRepositoryCustomImpl implements MovieRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<Movie> findByFilter(MovieFilter filter, Pageable pageable) {
        JpaQueryBuilder qb = new JpaQueryBuilder(entityManager);
        qb.append("select distinct m from Movie m");

        if (filter.getCompany() != null) {
            qb.append(" join m.productionCompanies c");
        }
        if (filter.getGenres() != null && !filter.getGenres().isEmpty()) {
            qb.append(" join m.genres g");
        }
        if (filter.getActorName() != null) {
            qb.append(" join m.actorRoles arl join arl.actor aa join aa.person aap");
        }

        qb.append(" where 1=1");

        qb.appendLikeCaseInsensitive("and m.name like :v", filter.getMovieName());
        qb.appendLikeCaseInsensitive("and m.country like :v", filter.getCountry());
        qb.append("and m.releaseDate >= :v", filter.getReleaseDate());
        qb.append("and m.avgRating >= :v", filter.getAvgRating());
        qb.appendLikeCaseInsensitive("and aap.name like :v", filter.getActorName());
        qb.appendLikeCaseInsensitive("and c.name like :v", filter.getCompany());
        qb.appendIn("and lower(g.name) in :v", filter.getGenres());
        return SpringQueryBuilderUtils.loadPage(qb, pageable, "id");
    }
}
