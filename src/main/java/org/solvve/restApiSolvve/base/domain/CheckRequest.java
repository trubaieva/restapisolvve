package org.solvve.restapisolvve.base.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class CheckRequest extends AbstractEntity {

    @ManyToOne
    @JoinColumn(name = "creator_id", nullable = false, updatable = false)
    private RegisteredUser creator;

    @ManyToOne
    @JoinColumn(name = "review_id", nullable = false, updatable = false)
    private Review review;

    private String complainedText;

    private String correctedText;

    @Enumerated(EnumType.STRING)
    private CheckStatus status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "moderator_id", nullable = false, updatable = false)
    private Moderator moderator;

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;
}
