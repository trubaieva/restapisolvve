package org.solvve.restapisolvve.base.dto.requesttocorrect;

import lombok.Data;
import org.solvve.restapisolvve.base.domain.CorrectStatus;
import org.solvve.restapisolvve.base.domain.ObjectToCorrect;

import java.time.Instant;
import java.util.UUID;

@Data
public class RequestToCorrectReadDTO {
    private UUID id;
    private ObjectToCorrect objectType;
    private UUID objectId;
    private String wrongData;
    private String proposedFix;
    private String correctData;
    private CorrectStatus status;
    private UUID creatorId;
    private UUID verifierId;
    private Instant fixedAt;
    private Instant createdAt;
    private Instant updatedAt;
}