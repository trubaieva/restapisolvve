package org.solvve.restapisolvve.base.repository;

import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.ContentManager;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;

import static org.junit.Assert.*;

public class ContentManagerRepositoryTest extends BaseTest {

    @Autowired
    private ContentManagerRepository contentManagerRepository;

    @Test
    public void testCreatedAtIsSet() {
        ContentManager c = new ContentManager();
        c = contentManagerRepository.save(c);

        Instant createdAtBeforeReload = c.getCreatedAt();
        assertNotNull(createdAtBeforeReload);
        c = contentManagerRepository.findById(c.getId()).get();

        Instant createdAtAfterReload = c.getCreatedAt();
        assertNotNull(createdAtAfterReload);
        assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        ContentManager c = new ContentManager();
        c = contentManagerRepository.save(c);

        Instant updatedAtBeforeReload = c.getUpdatedAt();
        assertNotNull(updatedAtBeforeReload);
        c = contentManagerRepository.findById(c.getId()).get();

        Instant updatedAtAfterReload = c.getUpdatedAt();
        assertNotNull(updatedAtAfterReload);
        assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        c.setName("Sveta");
        contentManagerRepository.save(c);
        c = contentManagerRepository.findById(c.getId()).get();
        Instant updatedAtAfterModificationAndReload = c.getUpdatedAt();
        assertTrue(updatedAtAfterModificationAndReload.isAfter(updatedAtAfterReload));
    }
}
