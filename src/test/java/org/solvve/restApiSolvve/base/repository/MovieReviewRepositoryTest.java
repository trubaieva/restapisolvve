package org.solvve.restapisolvve.base.repository;

import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.MovieReview;
import org.solvve.restapisolvve.base.util.EntityCreator;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;

import static org.junit.Assert.*;

public class MovieReviewRepositoryTest extends BaseTest {

    @Autowired
    private MovieReviewRepository movieReviewRepository;

    @Autowired
    private EntityCreator entityCreator;

    @Test
    public void testCreatedAtIsSet() {
        MovieReview movieReview = entityCreator.createMovieReview();

        Instant createdAtBeforeReload = movieReview.getCreatedAt();
        assertNotNull(createdAtBeforeReload);
        movieReview = movieReviewRepository.findById(movieReview.getId()).get();

        Instant createdAtAfterReload = movieReview.getCreatedAt();
        assertNotNull(createdAtAfterReload);
        assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        MovieReview movieReview = entityCreator.createMovieReview();

        Instant updatedAtBeforeReload = movieReview.getUpdatedAt();
        assertNotNull(updatedAtBeforeReload);
        movieReview = movieReviewRepository.findById(movieReview.getId()).get();

        Instant updatedAtAfterReload = movieReview.getUpdatedAt();
        assertNotNull(updatedAtAfterReload);
        assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        movieReview.setTextReview("text");
        movieReviewRepository.save(movieReview);
        movieReview = movieReviewRepository.findById(movieReview.getId()).get();
        Instant updatedAtAfterModificationAndReload = movieReview.getUpdatedAt();
        assertTrue(updatedAtAfterModificationAndReload.isAfter(updatedAtAfterReload));
    }

}
