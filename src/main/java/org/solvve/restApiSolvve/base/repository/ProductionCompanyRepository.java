package org.solvve.restapisolvve.base.repository;

import org.solvve.restapisolvve.base.domain.ProductionCompany;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ProductionCompanyRepository extends CrudRepository<ProductionCompany, UUID> {
    ProductionCompany findByName(String name);
}
