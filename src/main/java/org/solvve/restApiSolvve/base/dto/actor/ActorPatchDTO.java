package org.solvve.restapisolvve.base.dto.actor;

import lombok.Data;

import java.util.UUID;

@Data
public class ActorPatchDTO {
    private UUID personId;
    private Double avgRatingByRoles;
    private Double avgMovieRating;
}
