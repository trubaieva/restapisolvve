package org.solvve.restapisolvve.base.client.themoviedb.dto;

import lombok.Data;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
public class MovieReadDTO {

    private String id;
    private String originalTitle;
    private String title;

    private String originalLanguage;
    private LocalDate releaseDate;
    private String status;
    private Integer budget;
    private Integer revenue;
    private List<GenreReadDTO> genres = new ArrayList<>();
    private List<ProductionCountryReadDTO> productionCountries = new ArrayList<>();
    private String overview;
    private Integer runtime;
    private List<ProductionCompanyReadDTO> productionCompanies = new ArrayList<>();

    /*private String popularity;
    private String homepage;
    private String voteCount;
    private String tagline;
    private String adult;
    private String posterPath;
    private String voteAverage;*/
}
