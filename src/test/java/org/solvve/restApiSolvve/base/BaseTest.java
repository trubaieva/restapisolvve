package org.solvve.restapisolvve.base;

import org.bitbucket.brunneng.br.Configuration;
import org.bitbucket.brunneng.br.RandomObjectGenerator;
import org.junit.runner.RunWith;
import org.solvve.restapisolvve.base.domain.AbstractEntity;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Sql(statements = {"delete from external_system_import", "delete from registered_user_movie",
        "delete from registered_user_genre",
        "delete from movie_production_company", "delete from movie_news", "delete from movie_genre",
        "delete from person_news", "delete from rate", "delete from likes", "delete from request_to_correct",
        "delete from news", "delete from check_request", "delete from review", "delete from content_manager",
        "delete from moderator", "delete from registered_user", "delete from admin", "delete from app_user",
        "delete from production_company", "delete from genre", "delete from actor_role", "delete from crew",
        "delete from movie", "delete from actor", "delete from person"},
        executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public abstract class BaseTest {

    protected RandomObjectGenerator generator = new RandomObjectGenerator();

    protected RandomObjectGenerator flatGenerator;

    public BaseTest() {
        Configuration c = new Configuration();
        c.setFlatMode(true);
        flatGenerator = new RandomObjectGenerator(c);
    }

    protected <T extends AbstractEntity> T generateEntityWithoutId(Class<T> entityClass) {
        T entity = generator.generateRandomObject(entityClass);
        entity.setId(null);
        return entity;
    }

    protected <T extends AbstractEntity> T generateFlatEntityWithoutId(Class<T> entityClass) {
        T entity = flatGenerator.generateRandomObject(entityClass);
        entity.setId(null);
        return entity;
    }
}
