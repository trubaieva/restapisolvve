package org.solvve.restapisolvve.base.repository;

import org.solvve.restapisolvve.base.domain.RequestToCorrect;
import org.solvve.restapisolvve.base.dto.requesttocorrect.RequestToCorrectFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface RequestToCorrectRepositoryCustom {
    Page<RequestToCorrect> findByFilter(RequestToCorrectFilter filter, Pageable pageable);
}
