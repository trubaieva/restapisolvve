package org.solvve.restapisolvve.base.client.themoviedb.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class CreditReadDTO {

    private List<CastReadDTO> cast = new ArrayList();
    private List<CrewReadDTO> crew = new ArrayList();
}
