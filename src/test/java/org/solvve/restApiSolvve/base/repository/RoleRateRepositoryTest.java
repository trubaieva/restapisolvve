package org.solvve.restapisolvve.base.repository;

import org.junit.Assert;
import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.ActorRole;
import org.solvve.restapisolvve.base.domain.RegisteredUser;
import org.solvve.restapisolvve.base.domain.RoleRate;
import org.solvve.restapisolvve.base.util.EntityCreator;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;

import static org.junit.Assert.*;

public class RoleRateRepositoryTest extends BaseTest {

    @Autowired
    private EntityCreator entityCreator;

    @Autowired
    private RoleRateRepository roleRateRepository;

    @Test
    public void testCalcAverageMark() {
        RegisteredUser creator = entityCreator.createRegisteredUser();
        ActorRole role1 = entityCreator.createActorRole();
        ActorRole role2 = entityCreator.createActorRole();

        entityCreator.createRoleRate(creator, role1, 4);
        entityCreator.createRoleRate(creator, role1, 5);
        entityCreator.createRoleRate(creator, role1, (Integer) null);
        entityCreator.createRoleRate(creator, role2, 2);
        Assert.assertEquals(4.5, roleRateRepository.calcAverageRateOfRole(role1.getId()), Double.MIN_NORMAL);
    }

    @Test
    public void testCreatedAtIsSet() {
        RoleRate roleRate = entityCreator.createRoleRate();

        Instant createdAtBeforeReload = roleRate.getCreatedAt();
        assertNotNull(createdAtBeforeReload);
        roleRate = roleRateRepository.findById(roleRate.getId()).get();

        Instant createdAtAfterReload = roleRate.getCreatedAt();
        assertNotNull(createdAtAfterReload);
        assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSetAfterChildFiledModification() {
        RoleRate roleRate = entityCreator.createRoleRate();

        Instant updatedAtBeforeReload = roleRate.getUpdatedAt();
        assertNotNull(updatedAtBeforeReload);
        roleRate = roleRateRepository.findById(roleRate.getId()).get();

        Instant updatedAtAfterReload = roleRate.getUpdatedAt();
        assertNotNull(updatedAtAfterReload);
        assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        roleRate.setActorRole(entityCreator.createActorRole());
        roleRateRepository.save(roleRate);
        roleRate = roleRateRepository.findById(roleRate.getId()).get();
        Instant updatedAtAfterModificationAndReload = roleRate.getUpdatedAt();
        assertTrue(updatedAtAfterModificationAndReload.isAfter(updatedAtAfterReload));
    }

    @Test
    public void testUpdatedAtIsSetAfterParentFiledModification() {
        RoleRate roleRate = entityCreator.createRoleRate();

        Instant updatedAtBeforeReload = roleRate.getUpdatedAt();
        assertNotNull(updatedAtBeforeReload);
        roleRate = roleRateRepository.findById(roleRate.getId()).get();

        Instant updatedAtAfterReload = roleRate.getUpdatedAt();
        assertNotNull(updatedAtAfterReload);
        assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        roleRate.setRate(8);
        roleRateRepository.save(roleRate);
        roleRate = roleRateRepository.findById(roleRate.getId()).get();
        Instant updatedAtAfterModificationAndReload = roleRate.getUpdatedAt();
        assertTrue(updatedAtAfterModificationAndReload.isAfter(updatedAtAfterReload));
    }

}
