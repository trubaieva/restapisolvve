package org.solvve.restapisolvve.base.service;

import org.solvve.restapisolvve.base.domain.User;
import org.solvve.restapisolvve.base.domain.UserRole;
import org.solvve.restapisolvve.base.dto.userrole.UserRoleReadDTO;
import org.solvve.restapisolvve.base.exception.EntityNotFoundException;
import org.solvve.restapisolvve.base.exception.LinkDuplicatedException;
import org.solvve.restapisolvve.base.repository.RepositoryHelper;
import org.solvve.restapisolvve.base.repository.UserRepository;
import org.solvve.restapisolvve.base.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserRoleService {

    @Autowired
    RepositoryHelper repositoryHelper;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private TranslationService translationService;

    public List<UserRoleReadDTO> getAllRoles() {
        List<UserRoleReadDTO> dtos = new ArrayList<>();
        Iterable<UserRole> userRoleIterable = userRoleRepository.findAll();
        for (UserRole userRole : userRoleIterable) {
            dtos.add(translationService.translate(userRole, UserRoleReadDTO.class));
        }
        return dtos;
    }

    public List<UserRoleReadDTO> getUserRoles(UUID userId) {
        List<UserRole> userRoles = userRoleRepository.findAllByUserId(userId);
        return userRoles.stream().map(srcObject ->
                translationService.translate(srcObject, UserRoleReadDTO.class)).collect(
                Collectors.toList());
    }

    @Transactional
    public List<UserRoleReadDTO> addRoleToUser(UUID userId, UUID id) {
        User user = repositoryHelper.getEntityIfExist(User.class, userId);
        UserRole userRole = repositoryHelper.getEntityIfExist(UserRole.class, id);

        if (user.getUserRoles().stream().anyMatch(ur -> ur.getId().equals(id))) {
            throw new LinkDuplicatedException(String.format("User %s already has role %s", userId, id));
        }
        user.getUserRoles().add(userRole);
        user = userRepository.save(user);

        return translationService.translateToList(user.getUserRoles(), UserRoleReadDTO.class);
    }

    @Transactional
    public List<UserRoleReadDTO> removeRoleFromUser(UUID userId, UUID id) {
        User user = repositoryHelper.getEntityIfExist(User.class, userId);

        boolean removed = user.getUserRoles().removeIf(ur -> ur.getId().equals(id));
        if (!removed) {
            throw new EntityNotFoundException("User " + userId + " has no role " + id);
        }
        user = userRepository.save(user);

        return translationService.translateToList(user.getUserRoles(), UserRoleReadDTO.class);
    }
}
