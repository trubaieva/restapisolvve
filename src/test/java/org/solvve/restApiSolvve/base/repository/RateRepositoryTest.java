package org.solvve.restapisolvve.base.repository;

import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.Rate;
import org.solvve.restapisolvve.base.util.EntityCreator;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;

import static org.junit.Assert.*;

public class RateRepositoryTest extends BaseTest {

    @Autowired
    private RateRepository rateRepository;

    @Autowired
    private EntityCreator entityCreator;

    @Test
    public void testCreatedAtIsSet() {
        Rate rate = entityCreator.createRate();

        Instant createdAtBeforeReload = rate.getCreatedAt();
        assertNotNull(createdAtBeforeReload);
        rate = rateRepository.findById(rate.getId()).get();

        Instant createdAtAfterReload = rate.getCreatedAt();
        assertNotNull(createdAtAfterReload);
        assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        Rate rate = entityCreator.createRate();

        Instant updatedAtBeforeReload = rate.getUpdatedAt();
        assertNotNull(updatedAtBeforeReload);
        rate = rateRepository.findById(rate.getId()).get();

        Instant updatedAtAfterReload = rate.getUpdatedAt();
        assertNotNull(updatedAtAfterReload);
        assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        rate.setRate(8);
        rateRepository.save(rate);
        rate = rateRepository.findById(rate.getId()).get();
        Instant updatedAtAfterModificationAndReload = rate.getUpdatedAt();
        assertTrue(updatedAtAfterModificationAndReload.isAfter(updatedAtAfterReload));
    }

}
