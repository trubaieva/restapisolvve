package org.solvve.restapisolvve.base.controller;

import org.solvve.restapisolvve.base.controller.security.AdminOrCurrentUser;
import org.solvve.restapisolvve.base.dto.user.UserCreateDTO;
import org.solvve.restapisolvve.base.dto.user.UserPatchDTO;
import org.solvve.restapisolvve.base.dto.user.UserPutDTO;
import org.solvve.restapisolvve.base.dto.user.UserReadDTO;
import org.solvve.restapisolvve.base.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {

    @Autowired
    private UserService userService;

    @AdminOrCurrentUser
    @GetMapping
    public List<UserReadDTO> getUsers() {
        return userService.getUsers();
    }

    @AdminOrCurrentUser
    @GetMapping("/{id}")
    public UserReadDTO getUser(@PathVariable UUID id) {
        return userService.getUser(id);
    }

    @PostMapping
    public UserReadDTO createUser(@RequestBody UserCreateDTO createDTO) {
        return userService.createUser(createDTO);
    }

    @AdminOrCurrentUser
    @PutMapping("/{id}")
    public UserReadDTO updateUser(@PathVariable UUID id, @RequestBody UserPutDTO putDTO) {
        return userService.updateUser(id, putDTO);
    }

    @AdminOrCurrentUser
    @PatchMapping("/{id}")
    public UserReadDTO patchUser(@PathVariable UUID id, @RequestBody UserPatchDTO patchDTO) {
        return userService.patchUser(id, patchDTO);
    }

    @AdminOrCurrentUser
    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable UUID id) {
        userService.deleteUser(id);
    }
}
