package org.solvve.restapisolvve.base.repository;

import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.Admin;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;

import static org.junit.Assert.*;

public class AdminRepositoryTest extends BaseTest {

    @Autowired
    private AdminRepository adminRepository;

    @Test
    public void testCreatedAtIsSet() {
        Admin a = new Admin();
        a = adminRepository.save(a);

        Instant createdAtBeforeReload = a.getCreatedAt();
        assertNotNull(createdAtBeforeReload);
        a = adminRepository.findById(a.getId()).get();

        Instant createdAtAfterReload = a.getCreatedAt();
        assertNotNull(createdAtAfterReload);
        assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        Admin a = new Admin();
        a = adminRepository.save(a);

        Instant updatedAtBeforeReload = a.getUpdatedAt();
        assertNotNull(updatedAtBeforeReload);
        a = adminRepository.findById(a.getId()).get();

        Instant updatedAtAfterReload = a.getUpdatedAt();
        assertNotNull(updatedAtAfterReload);
        assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        a.setName("Sveta");
        adminRepository.save(a);
        a = adminRepository.findById(a.getId()).get();
        Instant updatedAtAfterModificationAndReload = a.getUpdatedAt();
        assertTrue(updatedAtAfterModificationAndReload.isAfter(updatedAtAfterReload));
    }
}
