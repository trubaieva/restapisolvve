package org.solvve.restapisolvve.base.repository;

import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.ReviewLike;
import org.solvve.restapisolvve.base.util.EntityCreator;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;

import static org.junit.Assert.*;

public class ReviewLikeRepositoryTest extends BaseTest {

    @Autowired
    private ReviewLikeRepository reviewLikeRepository;

    @Autowired
    private EntityCreator entityCreator;

    @Test
    public void testCreatedAtIsSet() {
        ReviewLike reviewLike = entityCreator.createReviewLike();

        Instant createdAtBeforeReload = reviewLike.getCreatedAt();
        assertNotNull(createdAtBeforeReload);
        reviewLike = reviewLikeRepository.findById(reviewLike.getId()).get();

        Instant createdAtAfterReload = reviewLike.getCreatedAt();
        assertNotNull(createdAtAfterReload);
        assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        ReviewLike reviewLike = entityCreator.createReviewLike();

        Instant updatedAtBeforeReload = reviewLike.getUpdatedAt();
        assertNotNull(updatedAtBeforeReload);
        reviewLike = reviewLikeRepository.findById(reviewLike.getId()).get();

        Instant updatedAtAfterReload = reviewLike.getUpdatedAt();
        assertNotNull(updatedAtAfterReload);
        assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        reviewLike.setIsLiked(true);
        reviewLikeRepository.save(reviewLike);
        reviewLike = reviewLikeRepository.findById(reviewLike.getId()).get();
        Instant updatedAtAfterModificationAndReload = reviewLike.getUpdatedAt();
        assertTrue(updatedAtAfterModificationAndReload.isAfter(updatedAtAfterReload));
    }

}
