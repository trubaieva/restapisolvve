package org.solvve.restapisolvve.base.repository;

import org.solvve.restapisolvve.base.domain.MovieRate;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface MovieRateRepository extends CrudRepository<MovieRate, UUID> {

    @Query("select avg(mr.rate) from MovieRate mr where mr.movie.id = :movieId")
    Double calcAverageRateOfMovie(UUID movieId);
}
