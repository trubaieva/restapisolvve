package org.solvve.restapisolvve.base.service.importer;

import lombok.extern.slf4j.Slf4j;
import org.solvve.restapisolvve.base.client.themoviedb.TheMovieDbClient;
import org.solvve.restapisolvve.base.client.themoviedb.dto.PersonReadDTO;
import org.solvve.restapisolvve.base.domain.Person;
import org.solvve.restapisolvve.base.exception.ImportAlreadyPerformedException;
import org.solvve.restapisolvve.base.exception.ImportedEntityAlreadyExistException;
import org.solvve.restapisolvve.base.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Slf4j
@Service
public class PersonImporterService {

    @Autowired
    private TheMovieDbClient movieDbClient;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private ExternalSystemImportService externalSystemImportService;

    @Transactional
    public UUID importPerson(String personExternalId)
            throws ImportAlreadyPerformedException, ImportedEntityAlreadyExistException {
        log.info("Importing person with external id={}", personExternalId);

        externalSystemImportService.validateNotImported(Person.class, personExternalId);

        PersonReadDTO read = movieDbClient.getPerson(personExternalId);
        Person existingPerson = personRepository.findByNameAndBirthDate(read.getName(), read.getBirthday());
        if (existingPerson != null) {
            throw new ImportedEntityAlreadyExistException(Person.class, existingPerson.getId(), "Person with name="
                    + read.getName() + " and birthday=" + read.getBirthday() + " already exist");
        }

        Person person = new Person();
        person.setName(read.getName());
        person.setBiography(read.getBiography());
        person.setBirthDate(read.getBirthday());
        person.setBirthPlace(read.getPlaceOfBirth());
        person.setDeathDate(read.getDeathday());
        person.setKnownForDepartment(ImportUtil.getCrewDepartmentByName(read.getKnownForDepartment()));
        person.setGender(ImportUtil.getGenderTypeById(read.getGender()));
        person = personRepository.save(person);
        externalSystemImportService.createExternalSystemImport(person, personExternalId);

        log.info("Person with external id={} imported", personExternalId);
        return person.getId();
    }
}
