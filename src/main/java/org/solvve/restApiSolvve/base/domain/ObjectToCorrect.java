package org.solvve.restapisolvve.base.domain;

public enum ObjectToCorrect {
    MOVIE,
    NEWS,
    PERSON
}
