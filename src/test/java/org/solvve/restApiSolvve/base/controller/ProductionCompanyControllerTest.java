package org.solvve.restapisolvve.base.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.solvve.restapisolvve.base.domain.ProductionCompany;
import org.solvve.restapisolvve.base.dto.productioncompany.ProductionCompanyCreateDTO;
import org.solvve.restapisolvve.base.dto.productioncompany.ProductionCompanyPutDTO;
import org.solvve.restapisolvve.base.dto.productioncompany.ProductionCompanyReadDTO;
import org.solvve.restapisolvve.base.exception.EntityNotFoundException;
import org.solvve.restapisolvve.base.exception.handler.ErrorInfo;
import org.solvve.restapisolvve.base.service.ProductionCompanyService;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(controllers = ProductionCompanyController.class)
public class ProductionCompanyControllerTest extends BaseControllerTest {

    @MockBean
    private ProductionCompanyService productionCompanyService;

    @Test
    public void testGetProductionCompanies() throws Exception {
        List<ProductionCompanyReadDTO> productionCompanyReadDTOs = new ArrayList<>();
        ProductionCompanyReadDTO productionCompanyReadDTO;
        for (int i = 0; i < 5; i++) {
            productionCompanyReadDTO = generateObject(ProductionCompanyReadDTO.class);
            productionCompanyReadDTOs.add(productionCompanyReadDTO);
        }
        Mockito.when(productionCompanyService.getProductionCompanies()).thenReturn(productionCompanyReadDTOs);

        String resultJson = mvc.perform(get("/api/v1/production-companies"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<ProductionCompanyReadDTO> actualProductionCompanies = objectMapper.readValue(resultJson,
                new TypeReference<List<ProductionCompanyReadDTO>>() {
                });
        Assertions.assertThat(actualProductionCompanies).isEqualTo(productionCompanyReadDTOs);

        Mockito.verify(productionCompanyService).getProductionCompanies();
    }

    @Test
    public void testGetProductionCompany() throws Exception {
        ProductionCompanyReadDTO productionCompany = generateObject(ProductionCompanyReadDTO.class);

        Mockito.when(productionCompanyService.getProductionCompany(productionCompany.getId())).thenReturn(
                productionCompany);

        String resultJson = mvc.perform(get("/api/v1/production-companies/{id}", productionCompany.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ProductionCompanyReadDTO actualProductionCompany = objectMapper.readValue(resultJson,
                ProductionCompanyReadDTO.class);
        Assertions.assertThat(actualProductionCompany).isEqualToComparingFieldByField(productionCompany);

        Mockito.verify(productionCompanyService).getProductionCompany(productionCompany.getId());
    }

    @Test
    public void testGetProductionCompanyWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(ProductionCompany.class, wrongId);
        Mockito.when(productionCompanyService.getProductionCompany(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/production-companies/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetProductionCompanyWrongFormatId() throws Exception {
        String wrongFormatId = "123";
        ErrorInfo expectedError = new ErrorInfo(HttpStatus.BAD_REQUEST,
                MethodArgumentTypeMismatchException.class,
                "Failed to convert value of type 'java.lang.String' to required type " +
                        "'java.util.UUID'; nested exception is java.lang" +
                        ".IllegalArgumentException: Invalid UUID string: 123"
        );
        String resultJson = mvc.perform(get("/api/v1/production-companies/{id}", wrongFormatId))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        ErrorInfo actualError = objectMapper.readValue(resultJson, ErrorInfo.class);
        Assertions.assertThat(actualError).isEqualToComparingFieldByField(expectedError);
    }

    @Test
    public void testCreateProductionCompany() throws Exception {
        ProductionCompanyCreateDTO create = generateObject(ProductionCompanyCreateDTO.class);

        ProductionCompanyReadDTO read = generateObject(ProductionCompanyReadDTO.class);

        Mockito.when(productionCompanyService.createProductionCompany(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/production-companies")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ProductionCompanyReadDTO actualProductionCompany = objectMapper.readValue(resultJson,
                ProductionCompanyReadDTO.class);
        Assertions.assertThat(actualProductionCompany).isEqualToComparingFieldByField(read);

        Mockito.verify(productionCompanyService).createProductionCompany(create);
    }

    @Test
    public void testUpdateProductionCompany() throws Exception {
        ProductionCompanyPutDTO putDTO = generateObject(ProductionCompanyPutDTO.class);

        ProductionCompanyReadDTO read = generateObject(ProductionCompanyReadDTO.class);

        Mockito.when(productionCompanyService.updateProductionCompany(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/production-companies/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ProductionCompanyReadDTO actualProductionCompany = objectMapper.readValue(resultJson,
                ProductionCompanyReadDTO.class);
        Assert.assertEquals(read, actualProductionCompany);

        Mockito.verify(productionCompanyService).updateProductionCompany(read.getId(), putDTO);
    }

    @Test
    public void testDeleteProductionCompany() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/production-companies/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(productionCompanyService).deleteProductionCompany(id);
    }

    @Test
    public void testAddProductionCompanyToMovie() throws Exception {
        UUID movieId = UUID.randomUUID();
        UUID productionCompanyId = UUID.randomUUID();

        ProductionCompanyReadDTO read = generateObject(ProductionCompanyReadDTO.class);

        List<ProductionCompanyReadDTO> expectedProductionCompanies = List.of(read);
        Mockito.when(productionCompanyService.addProductionCompanyToMovie(movieId, productionCompanyId)).thenReturn(
                expectedProductionCompanies);

        String resultJson = mvc.perform(
                post("/api/v1/movies/{movieId}/production-companies/{id}", movieId, productionCompanyId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<ProductionCompanyReadDTO> actualProductionCompanies = objectMapper.readValue(resultJson,
                new TypeReference<>() {
                });
        Assert.assertEquals(expectedProductionCompanies, actualProductionCompanies);

        Mockito.verify(productionCompanyService).addProductionCompanyToMovie(movieId, productionCompanyId);
    }

    @Test
    public void testRemoveProductionCompanyFromMovie() throws Exception {
        UUID movieId = UUID.randomUUID();
        UUID productionCompanyId = UUID.randomUUID();

        List<ProductionCompanyReadDTO> expectedProductionCompanies = new ArrayList<>();
        Mockito.when(productionCompanyService.removeProductionCompanyFromMovie(movieId, productionCompanyId))
                .thenReturn(expectedProductionCompanies);

        String resultJson = mvc.perform(
                delete("/api/v1/movies/{movieId}/production-companies/{id}", movieId, productionCompanyId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<ProductionCompanyReadDTO> actualProductionCompanies = objectMapper.readValue(resultJson,
                new TypeReference<>() {
                });
        Assert.assertEquals(expectedProductionCompanies, actualProductionCompanies);

        Mockito.verify(productionCompanyService).removeProductionCompanyFromMovie(movieId, productionCompanyId);
    }

    @Test
    public void testRemoveProductionCompanyFromMovieWithSeveralProductionCompanies() throws Exception {
        UUID movieId = UUID.randomUUID();
        UUID productionCompanyId = UUID.randomUUID();

        ProductionCompanyReadDTO read = generateObject(ProductionCompanyReadDTO.class);
        List<ProductionCompanyReadDTO> expectedProductionCompanies = List.of(read);

        Mockito.when(productionCompanyService.removeProductionCompanyFromMovie(movieId, productionCompanyId))
                .thenReturn(expectedProductionCompanies);

        String resultJson = mvc.perform(
                delete("/api/v1/movies/{movieId}/production-companies/{id}", movieId, productionCompanyId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<ProductionCompanyReadDTO> actualProductionCompanies = objectMapper.readValue(resultJson,
                new TypeReference<>() {});
        Assert.assertEquals(expectedProductionCompanies, actualProductionCompanies);

        Mockito.verify(productionCompanyService).removeProductionCompanyFromMovie(movieId, productionCompanyId);
    }
}
