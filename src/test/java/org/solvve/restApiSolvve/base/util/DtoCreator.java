package org.solvve.restapisolvve.base.util;

import org.bitbucket.brunneng.br.Configuration;
import org.bitbucket.brunneng.br.RandomObjectGenerator;
import org.solvve.restapisolvve.base.domain.UserRoleType;
import org.solvve.restapisolvve.base.dto.actorrole.ActorRoleCreateDTO;
import org.solvve.restapisolvve.base.dto.actorrole.ActorRolePatchDTO;
import org.solvve.restapisolvve.base.dto.actorrole.ActorRolePutDTO;
import org.solvve.restapisolvve.base.dto.actorrole.ActorRoleReadDTO;
import org.solvve.restapisolvve.base.dto.contentmanager.ContentManagerReadDTO;
import org.solvve.restapisolvve.base.dto.movie.*;
import org.solvve.restapisolvve.base.dto.registereduser.RegisteredUserReadDTO;
import org.solvve.restapisolvve.base.dto.requesttocorrect.RequestToCorrectCreateDTO;
import org.solvve.restapisolvve.base.dto.requesttocorrect.RequestToCorrectFilter;
import org.solvve.restapisolvve.base.dto.requesttocorrect.RequestToCorrectFixDTO;
import org.solvve.restapisolvve.base.dto.requesttocorrect.RequestToCorrectReadDTO;
import org.solvve.restapisolvve.base.dto.user.UserCreateDTO;
import org.solvve.restapisolvve.base.dto.user.UserPatchDTO;
import org.solvve.restapisolvve.base.dto.user.UserPutDTO;
import org.solvve.restapisolvve.base.dto.user.UserReadDTO;
import org.solvve.restapisolvve.base.dto.userrole.UserRoleReadDTO;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class DtoCreator {

    protected RandomObjectGenerator generator;

    public DtoCreator() {
        Configuration c = new Configuration();
        Configuration.Bean bean1 = c.beanOfClass(ActorRolePutDTO.class);
        Configuration.Bean.Property p1 = bean1.property("avgRating");
        p1.setMaxValue(10);
        Configuration.Bean bean2 = c.beanOfClass(ActorRolePatchDTO.class);
        Configuration.Bean.Property p2 = bean2.property("avgRating");
        p2.setMaxValue(10);
        generator = new RandomObjectGenerator(c);
    }

    public UserReadDTO createUserRead() {
        return generator.generateRandomObject(UserReadDTO.class);
    }

    public UserCreateDTO createUserCreateDTO() {
        return generator.generateRandomObject(UserCreateDTO.class);
    }

    public UserPatchDTO createUserPatchDTO() {
        return generator.generateRandomObject(UserPatchDTO.class);
    }

    public UserPutDTO createUserPutDTO() {
        return generator.generateRandomObject(UserPutDTO.class);
    }

    public UserRoleReadDTO createUserRoleRead(UUID id, UserRoleType type) {
        UserRoleReadDTO read = new UserRoleReadDTO();
        read.setId(id);
        read.setType(type);
        return read;
    }

    public MovieReadDTO createMovieRead() {
        return generator.generateRandomObject(MovieReadDTO.class);
    }

    public MovieReadExtendedDTO createMovieReadExtended() {
        return generator.generateRandomObject(MovieReadExtendedDTO.class);
    }

    public MovieCreateDTO createMovieCreateDTO() {
        return generator.generateRandomObject(MovieCreateDTO.class);
    }

    public MoviePatchDTO createMoviePatchDTO() {
        return generator.generateRandomObject(MoviePatchDTO.class);
    }

    public MoviePutDTO createMoviePutDTO() {
        return generator.generateRandomObject(MoviePutDTO.class);
    }

    public MovieFilter createMovieFilter() {
        return generator.generateRandomObject(MovieFilter.class);
    }

    public ActorRoleReadDTO createActorRoleRead() {
        return generator.generateRandomObject(ActorRoleReadDTO.class);
    }

    public ActorRoleCreateDTO createActorRoleCreateDTO() {
        return generator.generateRandomObject(ActorRoleCreateDTO.class);
    }

    public ActorRolePatchDTO createActorRolePatchDTO() {
        return generator.generateRandomObject(ActorRolePatchDTO.class);
    }

    public ActorRolePutDTO createActorRolePutDTO() {
        return generator.generateRandomObject(ActorRolePutDTO.class);
    }

    public RequestToCorrectReadDTO createRequestToCorrectRead() {
        return generator.generateRandomObject(RequestToCorrectReadDTO.class);
    }

    public RequestToCorrectCreateDTO createRequestToCorrectCreateDTO() {
        return generator.generateRandomObject(RequestToCorrectCreateDTO.class);
    }

    public RequestToCorrectFilter createRequestToCorrectFilter() {
        return generator.generateRandomObject(RequestToCorrectFilter.class);
    }

    public RequestToCorrectFixDTO createRequestToCorrectFixDTO() {
        return generator.generateRandomObject(RequestToCorrectFixDTO.class);
    }

    private ContentManagerReadDTO createContentManagerReadDTO() {
        return generator.generateRandomObject(ContentManagerReadDTO.class);
    }

    private RegisteredUserReadDTO createRegisteredUserReadDTO() {
        return generator.generateRandomObject(RegisteredUserReadDTO.class);
    }
}
