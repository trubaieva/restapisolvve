package org.solvve.restapisolvve.base.repository;

import org.solvve.restapisolvve.base.domain.ActorRole;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

@Repository
public interface ActorRoleRepository extends CrudRepository<ActorRole, UUID> {
    ActorRole findByIdAndMovieId(UUID id, UUID movieId);

    List<ActorRole> findAllByMovieId(UUID movieId);

    @Query("select a.id from ActorRole a")
    Stream<UUID> getIdsFromRoles();

    ActorRole findByMovieNameAndActorPersonNameAndCharacter(String movieName, String actorName, String character);
}
