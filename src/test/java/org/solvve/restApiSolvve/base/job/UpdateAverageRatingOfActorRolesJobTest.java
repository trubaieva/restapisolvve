package org.solvve.restapisolvve.base.job;

import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.solvve.restapisolvve.base.domain.ActorRole;
import org.solvve.restapisolvve.base.domain.RegisteredUser;
import org.solvve.restapisolvve.base.repository.ActorRoleRepository;
import org.solvve.restapisolvve.base.service.ActorRoleService;
import org.solvve.restapisolvve.base.util.EntityCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
@Import(ScheduledConfigurationsTest.ScheduledTestConfig.class)
@ActiveProfiles("test")
@Sql(statements = {"delete from rate", "delete from registered_user", "delete from app_user", "delete from " +
        "actor_role", "delete from actor", "delete from person"},
        executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class UpdateAverageRatingOfActorRolesJobTest {

    @Autowired
    private UpdateAverageRatingOfActorRolesJob updateAverageRatingOfActorRolesJob;

    @Autowired
    private EntityCreator entityCreator;

    @Autowired
    private ActorRoleRepository actorRoleRepository;

    @SpyBean
    private ActorRoleService actorRoleService;

    @Test
    public void testUpdateAverageRatingOfActorRoles() {
        RegisteredUser creator = entityCreator.createRegisteredUser();
        ActorRole actorRole = entityCreator.createActorRoleWithoutAvgRating();

        entityCreator.createRoleRate(creator, actorRole, 3);
        entityCreator.createRoleRate(creator, actorRole, 5);
        updateAverageRatingOfActorRolesJob.updateAverageRatingOfActorRoles();

        actorRole = actorRoleRepository.findById(actorRole.getId()).get();
        Assert.assertEquals(4.0, actorRole.getAvgRating(), Double.MIN_NORMAL);
    }

    @Test
    public void testActorRolesUpdatedIndependently() {
        RegisteredUser creator = entityCreator.createRegisteredUser();
        ActorRole actorRole1 = entityCreator.createActorRoleWithoutAvgRating();
        ActorRole actorRole2 = entityCreator.createActorRoleWithoutAvgRating();

        entityCreator.createRoleRate(creator, actorRole1, 4);
        entityCreator.createRoleRate(creator, actorRole2, 5);
        UUID[] failedId = new UUID[1];
        Mockito.doAnswer(invocationOnMock -> {
            if (failedId[0] == null) {
                failedId[0] = invocationOnMock.getArgument(0);
                throw new RuntimeException();
            }
            return invocationOnMock.callRealMethod();
        }).when(actorRoleService).updateAverageRating(Mockito.any());

        updateAverageRatingOfActorRolesJob.updateAverageRatingOfActorRoles();

        for (ActorRole a : actorRoleRepository.findAll()) {
            if (a.getId().equals(failedId[0])) {
                Assert.assertNull(a.getAvgRating());
            } else {
                Assert.assertNotNull(a.getAvgRating());
            }
        }
    }

}
