package org.solvve.restapisolvve.base.service.importer;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.client.themoviedb.TheMovieDbClient;
import org.solvve.restapisolvve.base.client.themoviedb.dto.*;
import org.solvve.restapisolvve.base.domain.ActorRole;
import org.solvve.restapisolvve.base.domain.Crew;
import org.solvve.restapisolvve.base.domain.Movie;
import org.solvve.restapisolvve.base.exception.ImportAlreadyPerformedException;
import org.solvve.restapisolvve.base.exception.ImportedEntityAlreadyExistException;
import org.solvve.restapisolvve.base.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

public class MovieImporterServiceWithTheMovieDbClientNoMockTest extends BaseTest {

    @Autowired
    private TheMovieDbClient movieDbClient;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private MovieImporterService movieImporterService;

    @Autowired
    private TransactionTemplate transactionTemplate;

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status -> {
            runnable.run();
        });
    }

    @Test
    public void testMovieImportFromDbClient()
            throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {
        String movieId = "280";

        MovieReadDTO movieReadDTO = movieDbClient.getMovie(movieId, null);
        CreditReadDTO creditReadDTO = movieDbClient.getMovieCredits(movieId);

        UUID importedMovieId = movieImporterService.importMovie(movieId);
        inTransaction(() -> {
            Movie movie = movieRepository.findById(importedMovieId).get();
            assertEquals(movieReadDTO.getProductionCountries().get(0).getName(), movie.getCountry());
            assertEquals(movieReadDTO.getOriginalLanguage(), movie.getLanguage());
            assertEquals(movieReadDTO.getOverview(), movie.getStoryLine());
            assertEquals(movieReadDTO.getReleaseDate(), movie.getReleaseDate());
            assertEquals(movieReadDTO.getRuntime(), movie.getRuntime());
            assertEquals(movieReadDTO.getBudget(), movie.getBudget());
            assertEquals(movieReadDTO.getRevenue(), movie.getRevenue());
            assertEquals(true, movie.getIsReleased());

            assertThat(movie.getGenres()).extracting("name").containsExactlyElementsOf(
                    movieReadDTO.getGenres().stream().map(GenreReadDTO::getName).collect(Collectors.toList()));

            assertThat(movie.getGenres()).flatExtracting("movies").extracting("id")
                    .contains(movie.getId());

            assertThat(movie.getProductionCompanies()).extracting("name").containsExactlyElementsOf(
                    movieReadDTO.getProductionCompanies().stream().map(ProductionCompanyReadDTO::getName)
                            .collect(Collectors.toList()));

            assertThat(movie.getProductionCompanies()).flatExtracting("movies").extracting("id")
                    .contains(movie.getId());

            List<CastReadDTO> castReadDTOS = creditReadDTO.getCast();
            List<ActorRole> actorRoles = movie.getActorRoles();
            assertEquals(castReadDTOS.size(), actorRoles.size());

            Assertions.assertThat(actorRoles).extracting("actor").extracting("person")
                    .extracting("name").containsExactlyElementsOf(castReadDTOS.stream()
                    .map(CastReadDTO::getName)
                    .collect(Collectors.toList()));

            Assertions.assertThat(actorRoles).extracting("movie").extracting("id")
                    .containsOnly(movie.getId());

            List<CrewReadDTO> crewReadDTOS = creditReadDTO.getCrew();
            List<Crew> crews = movie.getCrews();
            assertEquals(crewReadDTOS.size(), crews.size());

            Assertions.assertThat(crews).extracting("person").extracting("name")
                    .containsExactlyElementsOf(crewReadDTOS.stream().map(CrewReadDTO::getName)
                            .collect(Collectors.toList()));

            Assertions.assertThat(crews).extracting("movie").extracting("id")
                    .containsOnly(movie.getId());
        });
    }
}
