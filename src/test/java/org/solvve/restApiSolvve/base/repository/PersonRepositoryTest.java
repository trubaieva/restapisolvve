package org.solvve.restapisolvve.base.repository;

import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.Person;
import org.solvve.restapisolvve.base.util.EntityCreator;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;
import java.time.LocalDate;

import static org.junit.Assert.*;

public class PersonRepositoryTest extends BaseTest {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private EntityCreator entityCreator;

    @Test
    public void testCreatedAtIsSet() {
        Person person = entityCreator.createPerson();

        Instant createdAtBeforeReload = person.getCreatedAt();
        assertNotNull(createdAtBeforeReload);
        person = personRepository.findById(person.getId()).get();

        Instant createdAtAfterReload = person.getCreatedAt();
        assertNotNull(createdAtAfterReload);
        assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        Person person = entityCreator.createPerson();

        Instant updatedAtBeforeReload = person.getUpdatedAt();
        assertNotNull(updatedAtBeforeReload);
        person = personRepository.findById(person.getId()).get();

        Instant updatedAtAfterReload = person.getUpdatedAt();
        assertNotNull(updatedAtAfterReload);
        assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        person.setName("Anka Pulemet");
        personRepository.save(person);
        person = personRepository.findById(person.getId()).get();
        Instant updatedAtAfterModificationAndReload = person.getUpdatedAt();
        assertTrue(updatedAtAfterModificationAndReload.isAfter(updatedAtAfterReload));
    }

    @Test
    public void getPersonByNameAndBirthDate() {
        Person existingPerson = flatGenerator.generateRandomObject(Person.class);
        existingPerson.setName("Jennifer Aniston");
        existingPerson.setBirthDate(LocalDate.of(1969, 02, 11));
        existingPerson = personRepository.save(existingPerson);

        Person foundPerson = personRepository
                .findByNameAndBirthDate(existingPerson.getName(), existingPerson.getBirthDate());
        assertEquals(existingPerson.getId(), foundPerson.getId());
    }

    @Test
    public void getNoPersonJustByBirthDate() {
        Person existingPerson = flatGenerator.generateRandomObject(Person.class);
        existingPerson.setName("Jennifer Aniston");
        existingPerson.setBirthDate(LocalDate.of(1969, 02, 11));
        existingPerson = personRepository.save(existingPerson);

        Person foundPerson = personRepository.findByNameAndBirthDate("Another Name", existingPerson.getBirthDate());
        assertNull(foundPerson);
    }

    @Test
    public void getNoPersonJustByName() {
        Person existingPerson = flatGenerator.generateRandomObject(Person.class);
        existingPerson.setName("Jennifer Aniston");
        existingPerson.setBirthDate(LocalDate.of(1969, 02, 11));
        existingPerson = personRepository.save(existingPerson);

        Person foundPerson = personRepository.findByNameAndBirthDate(existingPerson.getName(), LocalDate.of(2000, 02,
                11));
        assertNull(foundPerson);
    }

}
