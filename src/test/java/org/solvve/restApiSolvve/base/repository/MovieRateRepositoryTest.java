package org.solvve.restapisolvve.base.repository;

import org.junit.Assert;
import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.Movie;
import org.solvve.restapisolvve.base.domain.MovieRate;
import org.solvve.restapisolvve.base.domain.RegisteredUser;
import org.solvve.restapisolvve.base.util.EntityCreator;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;

import static org.junit.Assert.*;

public class MovieRateRepositoryTest extends BaseTest {

    @Autowired
    private EntityCreator entityCreator;

    @Autowired
    private MovieRateRepository movieRateRepository;

    @Test
    public void testCalcAverageMark() {
        RegisteredUser creator = entityCreator.createRegisteredUser();
        Movie movie1 = entityCreator.createMovie();
        Movie movie2 = entityCreator.createMovie();

        entityCreator.createMovieRate(creator, movie1, 4);
        entityCreator.createMovieRate(creator, movie1, 5);
        entityCreator.createMovieRate(creator, movie1, (Integer) null);
        entityCreator.createMovieRate(creator, movie2, 2);
        Assert.assertEquals(4.5, movieRateRepository.calcAverageRateOfMovie(movie1.getId()), Double.MIN_NORMAL);
    }

    @Test
    public void testCreatedAtIsSet() {
        MovieRate movieRate = entityCreator.createMovieRate();

        Instant createdAtBeforeReload = movieRate.getCreatedAt();
        assertNotNull(createdAtBeforeReload);
        movieRate = movieRateRepository.findById(movieRate.getId()).get();

        Instant createdAtAfterReload = movieRate.getCreatedAt();
        assertNotNull(createdAtAfterReload);
        assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSetAfterChildFiledModification() {
        MovieRate movieRate = entityCreator.createMovieRate();

        Instant updatedAtBeforeReload = movieRate.getUpdatedAt();
        assertNotNull(updatedAtBeforeReload);
        movieRate = movieRateRepository.findById(movieRate.getId()).get();

        Instant updatedAtAfterReload = movieRate.getUpdatedAt();
        assertNotNull(updatedAtAfterReload);
        assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        movieRate.setMovie(entityCreator.createMovie());
        movieRateRepository.save(movieRate);
        movieRate = movieRateRepository.findById(movieRate.getId()).get();
        Instant updatedAtAfterModificationAndReload = movieRate.getUpdatedAt();
        assertTrue(updatedAtAfterModificationAndReload.isAfter(updatedAtAfterReload));
    }

    @Test
    public void testUpdatedAtIsSetAfterParentFiledModification() {
        MovieRate movieRate = entityCreator.createMovieRate();

        Instant updatedAtBeforeReload = movieRate.getUpdatedAt();
        assertNotNull(updatedAtBeforeReload);
        movieRate = movieRateRepository.findById(movieRate.getId()).get();

        Instant updatedAtAfterReload = movieRate.getUpdatedAt();
        assertNotNull(updatedAtAfterReload);
        assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        movieRate.setRate(8);
        movieRateRepository.save(movieRate);
        movieRate = movieRateRepository.findById(movieRate.getId()).get();
        Instant updatedAtAfterModificationAndReload = movieRate.getUpdatedAt();
        assertTrue(updatedAtAfterModificationAndReload.isAfter(updatedAtAfterReload));
    }

}
