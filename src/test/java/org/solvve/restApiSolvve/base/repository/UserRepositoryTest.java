package org.solvve.restapisolvve.base.repository;

import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.User;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;

import static org.junit.Assert.*;

public class UserRepositoryTest extends BaseTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void testSave() {
        User u = new User();
        u = userRepository.save(u);
        assertNotNull(u.getId());
        assertTrue(userRepository.findById(u.getId()).isPresent());
    }

    @Test
    public void testCreatedAtIsSet() {
        User u = new User();
        u = userRepository.save(u);

        Instant createdAtBeforeReload = u.getCreatedAt();
        assertNotNull(createdAtBeforeReload);
        u = userRepository.findById(u.getId()).get();

        Instant createdAtAfterReload = u.getCreatedAt();
        assertNotNull(createdAtAfterReload);
        assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        User u = new User();
        u = userRepository.save(u);

        Instant updatedAtBeforeReload = u.getUpdatedAt();
        assertNotNull(updatedAtBeforeReload);
        u = userRepository.findById(u.getId()).get();

        Instant updatedAtAfterReload = u.getUpdatedAt();
        assertNotNull(updatedAtAfterReload);
        assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        u.setName("Anya");
        userRepository.save(u);
        u = userRepository.findById(u.getId()).get();
        Instant updatedAtAfterModificationAndReload = u.getUpdatedAt();
        assertTrue(updatedAtAfterModificationAndReload.isAfter(updatedAtAfterReload));
    }
}
