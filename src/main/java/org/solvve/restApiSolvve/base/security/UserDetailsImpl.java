package org.solvve.restapisolvve.base.security;

import lombok.Getter;
import lombok.Setter;
import org.solvve.restapisolvve.base.domain.User;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.UUID;
import java.util.stream.Collectors;

@Getter
@Setter
public class UserDetailsImpl extends org.springframework.security.core.userdetails.User {

    private UUID id;

    public UserDetailsImpl(User user) {
        super(user.getEmail(), user.getEncodedPassword(), user.getUserRoles().stream()
                .map(r -> new SimpleGrantedAuthority(r.getType().toString())).collect(Collectors.toList()));
        id = user.getId();
    }

}
