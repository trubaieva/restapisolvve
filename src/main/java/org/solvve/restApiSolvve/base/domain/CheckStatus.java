package org.solvve.restapisolvve.base.domain;

public enum CheckStatus {
    OK,
    NEED_TO_CHECK,
    CORRECTED,
    CANCEL
}
