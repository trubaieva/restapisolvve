package org.solvve.restapisolvve.base.dto.user;

import lombok.Data;

@Data
public class UserCreateDTO {
    private String name;
    private String email;
    private String password;
}
