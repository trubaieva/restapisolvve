package org.solvve.restapisolvve.base.service;

import org.solvve.restapisolvve.base.domain.Genre;
import org.solvve.restapisolvve.base.domain.Movie;
import org.solvve.restapisolvve.base.dto.genre.GenreCreateDTO;
import org.solvve.restapisolvve.base.dto.genre.GenrePutDTO;
import org.solvve.restapisolvve.base.dto.genre.GenreReadDTO;
import org.solvve.restapisolvve.base.exception.EntityNotFoundException;
import org.solvve.restapisolvve.base.exception.LinkDuplicatedException;
import org.solvve.restapisolvve.base.repository.GenreRepository;
import org.solvve.restapisolvve.base.repository.MovieRepository;
import org.solvve.restapisolvve.base.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class GenreService {

    @Autowired
    RepositoryHelper repositoryHelper;

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private TranslationService translationService;

    public GenreReadDTO getGenre(UUID id) {
        Genre genre = repositoryHelper.getEntityIfExist(Genre.class, id);
        return translationService.translate(genre, GenreReadDTO.class);
    }

    public List<GenreReadDTO> getGenres() {
        List<GenreReadDTO> dtos = new ArrayList<>();
        Iterable<Genre> genreIterable = genreRepository.findAll();
        for (Genre genre : genreIterable) {
            dtos.add(translationService.translate(genre, GenreReadDTO.class));
        }
        return dtos;
    }

    public GenreReadDTO createGenre(GenreCreateDTO create) {
        Genre genre = translationService.translate(create, Genre.class);
        genre = genreRepository.save(genre);
        return translationService.translate(genre, GenreReadDTO.class);
    }

    public GenreReadDTO updateGenre(UUID id, GenrePutDTO putDTO) {
        Genre genre = repositoryHelper.getEntityIfExist(Genre.class, id);

        translationService.map(putDTO, genre);

        genre = genreRepository.save(genre);
        return translationService.translate(genre, GenreReadDTO.class);
    }

    public void deleteGenre(UUID id) {
        genreRepository.delete(repositoryHelper.getEntityIfExist(Genre.class, id));
    }

    @Transactional
    public List<GenreReadDTO> addGenreToMovie(UUID movieId, UUID id) {
        Movie movie = repositoryHelper.getEntityIfExist(Movie.class, movieId);
        Genre genre = repositoryHelper.getEntityIfExist(Genre.class, id);

        if (movie.getGenres().stream().anyMatch(g -> g.getId().equals(id))) {
            throw new LinkDuplicatedException(String.format("Movie %s already has genre %s", movieId, id));
        }
        movie.getGenres().add(genre);
        movie = movieRepository.save(movie);

        return translationService.translateToList(movie.getGenres(), GenreReadDTO.class);
    }

    @Transactional
    public List<GenreReadDTO> removeGenreFromMovie(UUID movieId, UUID id) {
        Movie movie = repositoryHelper.getEntityIfExist(Movie.class, movieId);

        boolean removed = movie.getGenres().removeIf(g -> g.getId().equals(id));
        if (!removed) {
            throw new EntityNotFoundException("Movie " + movieId + " has no genre " + id);
        }
        movie = movieRepository.save(movie);

        return translationService.translateToList(movie.getGenres(), GenreReadDTO.class);
    }
}
