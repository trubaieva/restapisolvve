package org.solvve.restapisolvve.base.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.solvve.restapisolvve.base.domain.Crew;
import org.solvve.restapisolvve.base.dto.crew.CrewCreateDTO;
import org.solvve.restapisolvve.base.dto.crew.CrewPatchDTO;
import org.solvve.restapisolvve.base.dto.crew.CrewPutDTO;
import org.solvve.restapisolvve.base.dto.crew.CrewReadDTO;
import org.solvve.restapisolvve.base.exception.EntityNotFoundException;
import org.solvve.restapisolvve.base.exception.handler.ErrorInfo;
import org.solvve.restapisolvve.base.service.CrewService;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(controllers = CrewController.class)
public class CrewControllerTest extends BaseControllerTest {

    @MockBean
    private CrewService crewService;

    @Test
    public void testGetMovieCrews() throws Exception {
        UUID movieId = UUID.randomUUID();
        List<CrewReadDTO> crewReadDTOs = new ArrayList<>();
        CrewReadDTO crewReadDTO;
        for (int i = 0; i < 5; i++) {
            crewReadDTO = generateObject(CrewReadDTO.class);
            crewReadDTO.setMovieId(movieId);
            crewReadDTOs.add(crewReadDTO);
        }

        Mockito.when(crewService.getMovieCrews(movieId)).thenReturn(crewReadDTOs);

        String resultJson = mvc.perform(get("/api/v1/movies/{movieId}/crews", movieId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<CrewReadDTO> actualCrews = objectMapper.readValue(resultJson,
                new TypeReference<>() {
                });
        Assert.assertEquals(crewReadDTOs, actualCrews);

        Mockito.verify(crewService).getMovieCrews(movieId);
    }

    @Test
    public void testGetMovieCrew() throws Exception {
        CrewReadDTO crew = generateObject(CrewReadDTO.class);

        Mockito.when(crewService.getMovieCrew(crew.getMovieId(), crew.getId()))
                .thenReturn(crew);

        String resultJson = mvc.perform(get("/api/v1/movies/{movieId}/crews/{id}",
                crew.getMovieId(),
                crew.getId()
        ))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        CrewReadDTO actualCrew = objectMapper.readValue(resultJson, CrewReadDTO.class);
        Assertions.assertThat(actualCrew).isEqualToComparingFieldByField(crew);

        Mockito.verify(crewService).getMovieCrew(crew.getMovieId(), crew.getId());
    }

    @Test
    public void testGetCrewWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(Crew.class, wrongId);
        Mockito.when(crewService.getMovieCrew(wrongId, wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/movies/{movieId}/crews/{id}", wrongId, wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetCrewWrongFormatId() throws Exception {
        String wrongFormatId = "123";
        UUID randomId = UUID.randomUUID();
        ErrorInfo expectedError = new ErrorInfo(HttpStatus.BAD_REQUEST,
                MethodArgumentTypeMismatchException.class,
                "Failed to convert value of type 'java.lang.String' to required type " +
                        "'java.util.UUID'; nested exception is java.lang" +
                        ".IllegalArgumentException: Invalid UUID string: 123"
        );
        String resultJson = mvc.perform(get("/api/v1/movies/{movieId}/crews/{id}", randomId, wrongFormatId))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        ErrorInfo actualError = objectMapper.readValue(resultJson, ErrorInfo.class);
        Assertions.assertThat(actualError).isEqualToComparingFieldByField(expectedError);
    }

    @Test
    public void testCreateMovieCrew() throws Exception {
        CrewCreateDTO create = generateObject(CrewCreateDTO.class);
        UUID movieId = UUID.randomUUID();
        CrewReadDTO read = generateObject(CrewReadDTO.class);

        Mockito.when(crewService.createMovieCrew(movieId, create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/movies/{movieId}/crews", movieId)
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        CrewReadDTO actualCrew = objectMapper.readValue(resultJson, CrewReadDTO.class);
        Assertions.assertThat(actualCrew).isEqualToComparingFieldByField(read);

        Mockito.verify(crewService).createMovieCrew(movieId, create);
    }

    @Test
    public void testCreateMovieCrewValidationFailed() throws Exception {
        CrewCreateDTO create = new CrewCreateDTO();
        UUID movieId = UUID.randomUUID();

        String resultJson = mvc.perform(post("/api/v1/movies/{movieId}/crews", movieId)
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(crewService, Mockito.never())
                .createMovieCrew(ArgumentMatchers.any(), ArgumentMatchers.any());
    }

    @Test
    public void testPatchMovieCrew() throws Exception {
        CrewPatchDTO patchDTO = generateObject(CrewPatchDTO.class);

        CrewReadDTO read = generateObject(CrewReadDTO.class);

        Mockito.when(crewService.patchMovieCrew(read.getMovieId(), read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/movies/{movieId}/crews/{id}",
                read.getMovieId(),
                read.getId().toString()
        )
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        CrewReadDTO actualCrew = objectMapper.readValue(resultJson, CrewReadDTO.class);
        Assert.assertEquals(read, actualCrew);

        Mockito.verify(crewService).patchMovieCrew(read.getMovieId(), read.getId(), patchDTO);
    }

    @Test
    public void testUpdateMovieCrew() throws Exception {
        CrewPutDTO putDTO = generateObject(CrewPutDTO.class);

        CrewReadDTO read = generateObject(CrewReadDTO.class);

        Mockito.when(crewService.updateMovieCrew(read.getMovieId(), read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/movies/{movieId}/crews/{id}",
                read.getMovieId(),
                read.getId().toString()
        )
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        CrewReadDTO actualCrew = objectMapper.readValue(resultJson, CrewReadDTO.class);
        Assert.assertEquals(read, actualCrew);

        Mockito.verify(crewService).updateMovieCrew(read.getMovieId(), read.getId(), putDTO);
    }

    @Test
    public void testDeleteMovieCrew() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/movies/{movieId}/crews/{id}", id, id.toString())).andExpect(status().isOk());

        Mockito.verify(crewService).deleteMovieCrew(id, id);
    }

}
