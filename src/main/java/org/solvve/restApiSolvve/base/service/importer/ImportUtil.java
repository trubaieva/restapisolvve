package org.solvve.restapisolvve.base.service.importer;

import org.solvve.restapisolvve.base.domain.CrewDepartment;
import org.solvve.restapisolvve.base.domain.GenderType;

public class ImportUtil {

    public static CrewDepartment getCrewDepartmentByName(String name) {
        CrewDepartment[] departments = CrewDepartment.values();
        for (CrewDepartment department : departments) {
            if (department.toString().equalsIgnoreCase(name)) {
                return CrewDepartment.valueOf(name.toUpperCase());
            }
        }
        return CrewDepartment.OTHER;
    }

    public static GenderType getGenderTypeById(Integer id) {
        if (id == 1) {
            return GenderType.FEMALE;
        } else if (id == 2) {
            return GenderType.MALE;
        } else {
            return null;
        }
    }

}
