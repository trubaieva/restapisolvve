package org.solvve.restapisolvve.base.service;

import lombok.extern.slf4j.Slf4j;
import org.solvve.restapisolvve.base.domain.Crew;
import org.solvve.restapisolvve.base.domain.Movie;
import org.solvve.restapisolvve.base.dto.crew.CrewCreateDTO;
import org.solvve.restapisolvve.base.dto.crew.CrewPatchDTO;
import org.solvve.restapisolvve.base.dto.crew.CrewPutDTO;
import org.solvve.restapisolvve.base.dto.crew.CrewReadDTO;
import org.solvve.restapisolvve.base.exception.EntityNotFoundException;
import org.solvve.restapisolvve.base.repository.CrewRepository;
import org.solvve.restapisolvve.base.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
public class CrewService {

    @Autowired
    RepositoryHelper repositoryHelper;

    @Autowired
    private CrewRepository crewRepository;

    @Autowired
    private TranslationService translationService;

    public CrewReadDTO getMovieCrew(UUID movieId, UUID id) {
        Crew crew = getMovieCrewRequired(movieId, id);
        return translationService.translate(crew, CrewReadDTO.class);
    }

    public List<CrewReadDTO> getMovieCrews(UUID movieId) {
        List<Crew> crews = crewRepository.findAllByMovieId(movieId);
        return crews.stream().map(srcObject ->
                translationService.translate(srcObject, CrewReadDTO.class)).collect(
                Collectors.toList());
    }

    public CrewReadDTO createMovieCrew(UUID movieId, CrewCreateDTO create) {
        Crew crew = translationService.translate(create, Crew.class);
        crew.setMovie(repositoryHelper.getReferenceIfExist(Movie.class, movieId));
        crew = crewRepository.save(crew);

        return translationService.translate(crew, CrewReadDTO.class);
    }

    public CrewReadDTO patchMovieCrew(UUID movieId, UUID id, CrewPatchDTO patchDTO) {
        Crew crew = getMovieCrewRequired(movieId, id);

        translationService.map(patchDTO, crew);

        crew = crewRepository.save(crew);
        return translationService.translate(crew, CrewReadDTO.class);
    }

    public CrewReadDTO updateMovieCrew(UUID movieId, UUID id, CrewPutDTO putDTO) {
        Crew crew = getMovieCrewRequired(movieId, id);

        translationService.map(putDTO, crew);

        crew = crewRepository.save(crew);
        return translationService.translate(crew, CrewReadDTO.class);
    }

    public void deleteMovieCrew(UUID movieId, UUID id) {
        Crew crew = crewRepository.findByIdAndMovieId(id, movieId);
        if (crew == null) {
            throw new EntityNotFoundException(Crew.class, id);
        }
        crewRepository.delete(crew);
    }

    private Crew getMovieCrewRequired(UUID movieId, UUID id) {
        Crew crew = crewRepository.findByIdAndMovieId(id, movieId);
        if (crew == null) {
            throw new EntityNotFoundException(Crew.class, id);
        }
        return crew;
    }
}
