package org.solvve.restapisolvve.base.service;

import lombok.extern.slf4j.Slf4j;
import org.bitbucket.brunneng.ot.Configuration;
import org.bitbucket.brunneng.ot.ObjectTranslator;
import org.bitbucket.brunneng.ot.exceptions.TranslationException;
import org.solvve.restapisolvve.base.domain.*;
import org.solvve.restapisolvve.base.dto.PageResult;
import org.solvve.restapisolvve.base.dto.actorrole.ActorRoleCreateDTO;
import org.solvve.restapisolvve.base.dto.actorrole.ActorRolePatchDTO;
import org.solvve.restapisolvve.base.dto.actorrole.ActorRolePutDTO;
import org.solvve.restapisolvve.base.dto.actorrole.ActorRoleReadDTO;
import org.solvve.restapisolvve.base.dto.crew.CrewCreateDTO;
import org.solvve.restapisolvve.base.dto.crew.CrewPatchDTO;
import org.solvve.restapisolvve.base.dto.crew.CrewPutDTO;
import org.solvve.restapisolvve.base.dto.crew.CrewReadDTO;
import org.solvve.restapisolvve.base.dto.movie.MoviePatchDTO;
import org.solvve.restapisolvve.base.dto.requesttocorrect.RequestToCorrectReadDTO;
import org.solvve.restapisolvve.base.dto.user.UserPatchDTO;
import org.solvve.restapisolvve.base.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
public class TranslationService {

    @Autowired
    private RepositoryHelper repositoryHelper;

    private ObjectTranslator objectTranslator;

    public TranslationService() {
        objectTranslator = new ObjectTranslator(createConfiguration());
    }

    private Configuration createConfiguration() {
        Configuration c = new Configuration();
        configureForUser(c);
        configureForMovie(c);
        configureForActorRole(c);
        configureForCrew(c);
        configureForRequestToCorrect(c);
        configureForAbstractEntity(c);

        return c;
    }

    private void configureForUser(Configuration c) {
        c.beanOfClass(UserPatchDTO.class).translationTo(User.class).mapOnlyNotNullProperties();
    }

    private void configureForMovie(Configuration c) {
        c.beanOfClass(MoviePatchDTO.class).translationTo(Movie.class).mapOnlyNotNullProperties();
    }

    private void configureForActorRole(Configuration c) {
        Configuration.Translation t = c.beanOfClass(ActorRole.class).translationTo(ActorRoleReadDTO.class);
        t.srcProperty("actor.id").translatesTo("actorId");
        t.srcProperty("movie.id").translatesTo("movieId");

        Configuration.Translation fromCreateToEntity =
                c.beanOfClass(ActorRoleCreateDTO.class).translationTo(ActorRole.class);
        fromCreateToEntity.srcProperty("actorId").translatesTo("actor.id");

        c.beanOfClass(ActorRolePatchDTO.class).translationTo(ActorRole.class).mapOnlyNotNullProperties();

        Configuration.Translation fromPatchToEntity =
                c.beanOfClass(ActorRolePatchDTO.class).translationTo(ActorRole.class);
        fromPatchToEntity.srcProperty("actorId").translatesTo("actor.id");

        Configuration.Translation fromPutToEntity =
                c.beanOfClass(ActorRolePutDTO.class).translationTo(ActorRole.class);
        fromPutToEntity.srcProperty("actorId").translatesTo("actor.id");
    }

    private void configureForCrew(Configuration c) {
        Configuration.Translation t = c.beanOfClass(Crew.class).translationTo(CrewReadDTO.class);
        t.srcProperty("person.id").translatesTo("personId");
        t.srcProperty("movie.id").translatesTo("movieId");

        Configuration.Translation fromCreateToEntity =
                c.beanOfClass(CrewCreateDTO.class).translationTo(Crew.class);
        fromCreateToEntity.srcProperty("personId").translatesTo("person.id");

        c.beanOfClass(CrewPatchDTO.class).translationTo(Crew.class).mapOnlyNotNullProperties();

        Configuration.Translation fromPatchToEntity =
                c.beanOfClass(CrewPatchDTO.class).translationTo(Crew.class);
        fromPatchToEntity.srcProperty("personId").translatesTo("person.id");

        Configuration.Translation fromPutToEntity =
                c.beanOfClass(CrewPutDTO.class).translationTo(Crew.class);
        fromPutToEntity.srcProperty("personId").translatesTo("person.id");
    }

    private void configureForRequestToCorrect(Configuration c) {
        Configuration.Translation t = c.beanOfClass(RequestToCorrect.class)
                .translationTo(RequestToCorrectReadDTO.class);
        t.srcProperty("creator.id").translatesTo("creatorId");
        t.srcProperty("verifier.id").translatesTo("verifierId");
    }

    private void configureForAbstractEntity(Configuration c) {
        c.beanOfClass(AbstractEntity.class).setIdentifierProperty("id");
        c.beanOfClass(AbstractEntity.class).setBeanFinder((beanClass, id) ->
                repositoryHelper
                        .getReferenceIfExist(beanClass, (UUID) id));
    }

    public <T> T translate(Object srcObject, Class<T> targetClass) {
        try {
            return objectTranslator.translate(srcObject, targetClass);
        } catch (TranslationException e) {
            log.warn(e.getMessage());
            throw (RuntimeException) e.getCause();
        }
    }

    public <T> List<T> translateToList(Collection<?> objects, Class<T> targetClass) {
        return objects.stream().map(o -> translate(o, targetClass)).collect(Collectors.toList());
    }

    public <T> void map(Object srcObject, Object destObject) {
        try {
            objectTranslator.mapBean(srcObject, destObject);
        } catch (TranslationException e) {
            log.warn(e.getMessage());
            throw (RuntimeException) e.getCause();
        }
    }

    public <E, T> PageResult<T> toPageResult(Page<E> page, Class<T> dtoType) {
        PageResult<T> res = new PageResult<>();
        res.setPage(page.getNumber());
        res.setPageSize(page.getSize());
        res.setTotalPages(page.getTotalPages());
        res.setTotalElements(page.getTotalElements());
        res.setData(page.getContent().stream().map(e -> translate(e, dtoType)).collect(Collectors.toList()));
        return res;
    }
}
