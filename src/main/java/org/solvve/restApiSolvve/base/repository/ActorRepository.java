package org.solvve.restapisolvve.base.repository;

import org.solvve.restapisolvve.base.domain.Actor;
import org.solvve.restapisolvve.base.domain.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ActorRepository extends CrudRepository<Actor, UUID> {
    Actor findByPerson(Person person);
}
