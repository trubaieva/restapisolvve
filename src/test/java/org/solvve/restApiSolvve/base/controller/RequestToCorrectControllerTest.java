package org.solvve.restapisolvve.base.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.solvve.restapisolvve.base.domain.ObjectToCorrect;
import org.solvve.restapisolvve.base.domain.RequestToCorrect;
import org.solvve.restapisolvve.base.dto.PageResult;
import org.solvve.restapisolvve.base.dto.requesttocorrect.RequestToCorrectCreateDTO;
import org.solvve.restapisolvve.base.dto.requesttocorrect.RequestToCorrectFilter;
import org.solvve.restapisolvve.base.dto.requesttocorrect.RequestToCorrectFixDTO;
import org.solvve.restapisolvve.base.dto.requesttocorrect.RequestToCorrectReadDTO;
import org.solvve.restapisolvve.base.exception.EntityNotFoundException;
import org.solvve.restapisolvve.base.exception.handler.ErrorInfo;
import org.solvve.restapisolvve.base.service.RequestToCorrectService;
import org.solvve.restapisolvve.base.util.DtoCreator;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(controllers = RequestToCorrectController.class)
public class RequestToCorrectControllerTest extends BaseControllerTest {

    @MockBean
    private RequestToCorrectService requestToCorrectService;

    private DtoCreator dtoCreator = new DtoCreator();

    @Test
    public void testGetRequestsToCorrect() throws Exception {
        RequestToCorrectFilter filter = dtoCreator.createRequestToCorrectFilter();
        filter.setObjectTypes(new HashSet<>(List.of(ObjectToCorrect.MOVIE, ObjectToCorrect.NEWS)));
        List<RequestToCorrectReadDTO> requestReadDTOs = List.of(dtoCreator.createRequestToCorrectRead(),
                dtoCreator.createRequestToCorrectRead(),
                dtoCreator.createRequestToCorrectRead());
        PageResult<RequestToCorrectReadDTO> resultPage = new PageResult<>();
        resultPage.setData(requestReadDTOs);

        Mockito.when(requestToCorrectService.getRequestsToCorrect(filter, PageRequest.of(0, defaultPageSize)))
                .thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/requests-to-correct")
                .param("movieName", filter.getMovieName())
                .param("personName", filter.getPersonName())
                .param("objectTypes", "MOVIE, NEWS")
                .param("status", filter.getStatus().toString())
                .param("wrongData", filter.getWrongData()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PageResult<RequestToCorrectReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);

        Mockito.verify(requestToCorrectService).getRequestsToCorrect(filter, PageRequest.of(0, defaultPageSize));
    }

    @Test
    public void testGetRequestsWithPagingAndSorting() throws Exception {
        RequestToCorrectFilter requestFilter = new RequestToCorrectFilter();

        RequestToCorrectReadDTO requestReadDTO = dtoCreator.createRequestToCorrectRead();

        int page = 1;
        int size = 25;

        PageResult<RequestToCorrectReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(page);
        resultPage.setPageSize(size);
        resultPage.setTotalElements(100);
        resultPage.setTotalPages(4);
        resultPage.setData(List.of(requestReadDTO));

        PageRequest pageRequest = PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "fixedAt"));
        Mockito.when(requestToCorrectService.getRequestsToCorrect(requestFilter, pageRequest)).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/requests-to-correct")
                .param("page", Integer.toString(page))
                .param("size", Integer.toString(size))
                .param("sort", "fixedAt,asc"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PageResult<RequestToCorrectReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);

        Mockito.verify(requestToCorrectService).getRequestsToCorrect(requestFilter, pageRequest);
    }

    @Test
    public void testGetRequestToCorrect() throws Exception {
        RequestToCorrectReadDTO requestToCorrect = dtoCreator.createRequestToCorrectRead();

        Mockito.when(requestToCorrectService.getRequestToCorrect(requestToCorrect.getId()))
                .thenReturn(requestToCorrect);

        String resultJson = mvc.perform(get("/api/v1/requests-to-correct/{id}", requestToCorrect.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RequestToCorrectReadDTO actualRequestToCorrect = objectMapper.readValue(resultJson,
                RequestToCorrectReadDTO.class);
        Assertions.assertThat(actualRequestToCorrect).isEqualToComparingFieldByField(requestToCorrect);

        Mockito.verify(requestToCorrectService).getRequestToCorrect(requestToCorrect.getId());
    }

    @Test
    public void testGetRequestToCorrectWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(RequestToCorrect.class, wrongId);
        Mockito.when(requestToCorrectService.getRequestToCorrect(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/requests-to-correct/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetRequestToCorrectWrongFormatId() throws Exception {
        String wrongFormatId = "123";
        ErrorInfo expectedError = new ErrorInfo(HttpStatus.BAD_REQUEST,
                MethodArgumentTypeMismatchException.class,
                "Failed to convert value of type 'java.lang.String' to required type " +
                        "'java.util.UUID'; nested exception is java.lang" +
                        ".IllegalArgumentException: Invalid UUID string: 123"
        );
        String resultJson = mvc.perform(get("/api/v1/requests-to-correct/{id}", wrongFormatId))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        ErrorInfo actualError = objectMapper.readValue(resultJson, ErrorInfo.class);
        Assertions.assertThat(actualError).isEqualToComparingFieldByField(expectedError);
    }

    @Test
    public void testCreateRequestToCorrect() throws Exception {
        RequestToCorrectCreateDTO create = dtoCreator.createRequestToCorrectCreateDTO();
        UUID userId = UUID.randomUUID();
        RequestToCorrectReadDTO read = dtoCreator.createRequestToCorrectRead();

        Mockito.when(requestToCorrectService.createRequestToCorrect(userId, create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/users/{userId}/requests-to-correct", userId)
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RequestToCorrectReadDTO actualRequestToCorrect = objectMapper
                .readValue(resultJson, RequestToCorrectReadDTO.class);
        Assertions.assertThat(actualRequestToCorrect).isEqualToComparingFieldByField(read);

        Mockito.verify(requestToCorrectService).createRequestToCorrect(userId, create);
    }

    @Test
    public void testCreateRequestToCorrectValidationFailed() throws Exception {
        RequestToCorrectCreateDTO create = new RequestToCorrectCreateDTO();
        UUID userId = UUID.randomUUID();

        String resultJson = mvc.perform(post("/api/v1/users/{userId}/requests-to-correct", userId)
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(requestToCorrectService, Mockito.never()).createRequestToCorrect(ArgumentMatchers.any(),
                ArgumentMatchers.any());
    }

    @Test
    public void testFixRequestToCorrect() throws Exception {
        UUID cmId = UUID.randomUUID();
        UUID id = UUID.randomUUID();
        RequestToCorrectFixDTO fixDTO = dtoCreator.createRequestToCorrectFixDTO();
        RequestToCorrectReadDTO read = dtoCreator.createRequestToCorrectRead();

        Mockito.when(requestToCorrectService.fixRequestToCorrect(cmId, id, fixDTO)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/content-managers/{cmId}/requests-to-correct/{id}/fix", cmId, id)
                .content(objectMapper.writeValueAsString(fixDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RequestToCorrectReadDTO actualRequestToCorrect = objectMapper
                .readValue(resultJson, RequestToCorrectReadDTO.class);
        Assertions.assertThat(actualRequestToCorrect).isEqualToComparingFieldByField(read);

        Mockito.verify(requestToCorrectService).fixRequestToCorrect(cmId, id, fixDTO);
    }

    @Test
    public void testCancelRequestToCorrect() throws Exception {
        UUID cmId = UUID.randomUUID();
        UUID id = UUID.randomUUID();
        RequestToCorrectReadDTO read = dtoCreator.createRequestToCorrectRead();

        Mockito.when(requestToCorrectService.cancelRequestToCorrect(cmId, id)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/content-managers/{cmId}/requests-to-correct/{id}/cancel",
                cmId, id))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RequestToCorrectReadDTO actualRequestToCorrect = objectMapper
                .readValue(resultJson, RequestToCorrectReadDTO.class);
        Assertions.assertThat(actualRequestToCorrect).isEqualToComparingFieldByField(read);

        Mockito.verify(requestToCorrectService).cancelRequestToCorrect(cmId, id);
    }
}
