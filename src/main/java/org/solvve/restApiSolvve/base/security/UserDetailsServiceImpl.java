package org.solvve.restapisolvve.base.security;

import org.solvve.restapisolvve.base.domain.User;
import org.solvve.restapisolvve.base.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userRepository.findByEmail(username);
        if (user == null) {
            throw new UsernameNotFoundException("User " + username + " is not found!");
        }
        return new UserDetailsImpl(user);

        //return  userRepository.findByEmail(username).map(UserDetailsImpl::new)
        //.orElseThrow(() -> new UsernameNotFoundException(String.format("User %s is not found!", username)));
    }
}
