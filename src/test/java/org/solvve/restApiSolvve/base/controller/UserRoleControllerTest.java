package org.solvve.restapisolvve.base.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.solvve.restapisolvve.base.domain.UserRoleType;
import org.solvve.restapisolvve.base.dto.userrole.UserRoleReadDTO;
import org.solvve.restapisolvve.base.service.UserRoleService;
import org.solvve.restapisolvve.base.util.DtoCreator;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(controllers = UserRoleController.class)
public class UserRoleControllerTest extends BaseControllerTest {

    @MockBean
    private UserRoleService userRoleService;

    private DtoCreator dtoCreator = new DtoCreator();

    @Test
    public void testGetAllRoles() throws Exception {
        UserRoleReadDTO expectedRead1 = dtoCreator.createUserRoleRead(UUID.randomUUID(), UserRoleType.REGISTERED_USER);
        UserRoleReadDTO expectedRead2 = dtoCreator.createUserRoleRead(UUID.randomUUID(), UserRoleType.MODERATOR);

        List<UserRoleReadDTO> userRoleReadDTOs = new ArrayList<>(List.of(expectedRead1, expectedRead2));

        Mockito.when(userRoleService.getAllRoles()).thenReturn(userRoleReadDTOs);

        String resultJson = mvc.perform(get("/api/v1/roles"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<UserRoleReadDTO> actualUsers = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assertions.assertThat(actualUsers).isEqualTo(userRoleReadDTOs);

        Mockito.verify(userRoleService).getAllRoles();
    }

    @Test
    public void testGetUserRoles() throws Exception {
        UUID userId = UUID.randomUUID();

        UserRoleReadDTO expectedRead1 = dtoCreator.createUserRoleRead(UUID.randomUUID(), UserRoleType.REGISTERED_USER);
        UserRoleReadDTO expectedRead2 = dtoCreator.createUserRoleRead(UUID.randomUUID(), UserRoleType.MODERATOR);

        List<UserRoleReadDTO> userRoleReadDTOs = new ArrayList<>(List.of(expectedRead1, expectedRead2));

        Mockito.when(userRoleService.getUserRoles(userId)).thenReturn(userRoleReadDTOs);

        String resultJson = mvc.perform(get("/api/v1/users/{userId}/roles", userId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<UserRoleReadDTO> actualUsers = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assertions.assertThat(actualUsers).isEqualTo(userRoleReadDTOs);

        Mockito.verify(userRoleService).getUserRoles(userId);
    }

    @Test
    public void testAddRoleToUser() throws Exception {
        UUID userId = UUID.randomUUID();
        UUID userRoleId = UUID.randomUUID();

        UserRoleReadDTO read = dtoCreator.createUserRoleRead(userRoleId, UserRoleType.REGISTERED_USER);

        List<UserRoleReadDTO> expectedRoles = List.of(read);
        Mockito.when(userRoleService.addRoleToUser(userId, userRoleId)).thenReturn(expectedRoles);

        String resultJson = mvc.perform(post("/api/v1/users/{userId}/roles/{id}", userId, userRoleId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<UserRoleReadDTO> actualRoles = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(expectedRoles, actualRoles);

        Mockito.verify(userRoleService).addRoleToUser(userId, userRoleId);
    }

    @Test
    public void testRemoveRoleFromUser() throws Exception {
        UUID userId = UUID.randomUUID();
        UUID userRoleId = UUID.randomUUID();

        List<UserRoleReadDTO> expectedRoles = new ArrayList<>();
        Mockito.when(userRoleService.removeRoleFromUser(userId, userRoleId)).thenReturn(expectedRoles);

        String resultJson = mvc.perform(delete("/api/v1/users/{userId}/roles/{id}", userId, userRoleId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<UserRoleReadDTO> actualRoles = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(expectedRoles, actualRoles);

        Mockito.verify(userRoleService).removeRoleFromUser(userId, userRoleId);
    }

    @Test
    public void testRemoveRoleFromUserWithSeveralRoles() throws Exception {
        UUID userId = UUID.randomUUID();
        UUID userRoleId = UUID.randomUUID();

        UserRoleReadDTO read = dtoCreator.createUserRoleRead(userRoleId, UserRoleType.REGISTERED_USER);
        List<UserRoleReadDTO> expectedRoles = List.of(read);

        Mockito.when(userRoleService.removeRoleFromUser(userId, userRoleId)).thenReturn(expectedRoles);

        String resultJson = mvc.perform(delete("/api/v1/users/{userId}/roles/{id}", userId, userRoleId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<UserRoleReadDTO> actualRoles = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(expectedRoles, actualRoles);

        Mockito.verify(userRoleService).removeRoleFromUser(userId, userRoleId);
    }
}
