package org.solvve.restapisolvve.base.dto.news;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class NewsReadDTO {
    private UUID id;
    private String header;
    private String text;
    private Integer likeCount;
    private Instant createdAt;
    private Instant updatedAt;
}
