package org.solvve.restapisolvve.base.dto.requesttocorrect;

import lombok.Data;

@Data
public class RequestToCorrectFixDTO {
    private String fix;
}