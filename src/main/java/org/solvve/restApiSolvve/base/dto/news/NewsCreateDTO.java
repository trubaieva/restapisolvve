package org.solvve.restapisolvve.base.dto.news;

import lombok.Data;

@Data
public class NewsCreateDTO {
    private String header;
    private String text;
}
