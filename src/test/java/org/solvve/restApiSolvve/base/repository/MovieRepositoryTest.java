package org.solvve.restapisolvve.base.repository;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.*;
import org.solvve.restapisolvve.base.dto.movie.MovieFilter;
import org.solvve.restapisolvve.base.dto.movie.MovieInLeaderBoardReadDTO;
import org.solvve.restapisolvve.base.util.EntityCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.support.TransactionTemplate;

import java.time.Instant;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class MovieRepositoryTest extends BaseTest {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private EntityCreator entityCreator;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testGetMovieByGenreAndProductionCompany() {
        List<Movie> movies = createTestMovies();

        List<Movie> result = movieRepository
                .findDistinctByGenresNameContainsAndProductionCompaniesNameContainsOrderByAvgRating(
                        "Drama",
                        "Walt Disney Pictures"
                );
        Assertions.assertThat(result)
                .extracting(Movie::getId)
                .isEqualTo(Arrays.asList(movies.get(2).getId(), movies.get(0).getId()));
    }

    @Test
    public void testGetMovieByGenreAndAvgRating() {
        List<Movie> movies = createTestMovies();

        List<Movie> result = movieRepository
                .findDistinctByGenresNameContainsAndAvgRatingAfterOrderByReleaseDateDesc("Drama",
                        9.0
                );
        Assertions.assertThat(result)
                .extracting(Movie::getId)
                .isEqualTo(Arrays.asList(movies.get(2).getId(), movies.get(0).getId()));
    }

    @Test
    public void testGetMovieByGenreAndMinAvgRatingInGivenDateInterval() {
        List<Movie> movies = createTestMovies();

        List<Movie> result = movieRepository.findByGenreAndMinAvgRatingInGivenDateInterval("Drama",
                9.0,
                LocalDate.of(2019, 1, 1),
                LocalDate.of(2020, 1, 1)
        );
        Assertions.assertThat(result)
                .extracting(Movie::getId)
                .isEqualTo(Arrays.asList(movies.get(0).getId(), movies.get(3).getId()));
    }

    @Test
    public void testGetMoviesWithEmptyFilter() {
        List<Movie> movies = createTestMovies();

        MovieFilter filter = new MovieFilter();
        Assertions.assertThat(movieRepository.findByFilter(filter, Pageable.unpaged()))
                .extracting("id")
                .containsExactlyInAnyOrderElementsOf(movies.stream().map(Movie::getId).collect(Collectors.toList()));
    }

    @Test
    public void testGetMoviesByName() {
        List<Movie> movies = createTestMovies();

        MovieFilter filter = new MovieFilter();
        filter.setMovieName("Lion King");

        Assertions.assertThat(movieRepository.findByFilter(filter, Pageable.unpaged()))
                .extracting("id")
                .containsExactlyInAnyOrderElementsOf(Arrays.asList(movies.get(0).getId(),
                        movies.get(1).getId(),
                        movies.get(3).getId()
                ));
    }

    @Test
    public void testGetMoviesByCountry() {
        List<Movie> movies = createTestMovies();

        MovieFilter filter = new MovieFilter();
        filter.setCountry("us");

        Assertions.assertThat(movieRepository.findByFilter(filter, Pageable.unpaged()))
                .extracting("id")
                .containsExactlyInAnyOrderElementsOf(movies.stream().map(Movie::getId).collect(Collectors.toList()));
    }

    @Test
    public void testGetMoviesByReleaseDate() {
        List<Movie> movies = createTestMovies();

        MovieFilter filter = new MovieFilter();
        filter.setReleaseDate(LocalDate.of(2019, 11, 1));

        Assertions.assertThat(movieRepository.findByFilter(filter, Pageable.unpaged()))
                .extracting("id")
                .containsExactlyInAnyOrderElementsOf(Arrays.asList(movies.get(2).getId(), movies.get(3).getId()));
    }

    @Test
    public void testGetMoviesByAvgRating() {
        List<Movie> movies = createTestMovies();

        MovieFilter filter = new MovieFilter();
        filter.setAvgRating(9.1);

        Assertions.assertThat(movieRepository.findByFilter(filter, Pageable.unpaged()))
                .extracting("id")
                .containsExactlyInAnyOrderElementsOf(Arrays.asList(movies.get(0).getId(),
                        movies.get(1).getId(),
                        movies.get(2).getId()
                ));
    }

    @Test
    public void testGetMoviesByActorName() {
        List<Movie> movies = createTestMovies();

        MovieFilter filter = new MovieFilter();
        filter.setActorName("JD McCrary");

        Assertions.assertThat(movieRepository.findByFilter(filter, Pageable.unpaged()))
                .extracting("id")
                .containsExactlyInAnyOrderElementsOf(Arrays.asList(movies.get(0).getId()));
    }

    @Test
    public void testGetMoviesByProductionCompany() {
        List<Movie> movies = createTestMovies();

        MovieFilter filter = new MovieFilter();
        filter.setCompany("Fantasy Films");

        Assertions.assertThat(movieRepository.findByFilter(filter, Pageable.unpaged()))
                .extracting("id")
                .containsExactlyInAnyOrderElementsOf(Arrays.asList(movies.get(0).getId(),
                        movies.get(1).getId(),
                        movies.get(3).getId()
                ));
    }

    @Test
    public void testGetMoviesByGenre() {
        List<Movie> movies = createTestMovies();

        MovieFilter filter = new MovieFilter();
        filter.getGenres().add("comedy");

        Assertions.assertThat(movieRepository.findByFilter(filter, Pageable.unpaged())).extracting("id")
                .containsExactlyInAnyOrderElementsOf(Arrays.asList(movies.get(0).getId(), movies.get(1).getId()));
    }

    @Test
    public void testGetMoviesByAllFilters() {
        List<Movie> movies = createTestMovies();

        MovieFilter filter = new MovieFilter();
        filter.setMovieName("Lion");
        filter.setCountry("USA");
        filter.setReleaseDate(LocalDate.of(2019, 1, 1));
        filter.setAvgRating(9.0);
        filter.setActorName("John Oliver");
        filter.setCompany("Walt Disney Pictures");
        filter.getGenres().add("Drama");
    }

    @Test
    public void testGetIdsFromMovies() {
        Set<UUID> expectedIdsOfMovies = new HashSet<>();
        expectedIdsOfMovies.add(entityCreator.createMovie().getId());
        expectedIdsOfMovies.add(entityCreator.createMovie().getId());

        transactionTemplate.executeWithoutResult(status -> {
            Assert.assertEquals(expectedIdsOfMovies, movieRepository.getIdsFromMovies().collect(Collectors.toSet()));
        });
    }

    @Test
    public void testCreatedAtIsSet() {
        Movie movie = entityCreator.createMovie();

        Instant createdAtBeforeReload = movie.getCreatedAt();
        assertNotNull(createdAtBeforeReload);
        movie = movieRepository.findById(movie.getId()).get();

        Instant createdAtAfterReload = movie.getCreatedAt();
        assertNotNull(createdAtAfterReload);
        assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        Movie movie = entityCreator.createMovie();

        Instant updatedAtBeforeReload = movie.getUpdatedAt();
        assertNotNull(updatedAtBeforeReload);
        movie = movieRepository.findById(movie.getId()).get();

        Instant updatedAtAfterReload = movie.getUpdatedAt();
        assertNotNull(updatedAtAfterReload);
        assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        movie.setCountry("Spain");
        movieRepository.save(movie);
        movie = movieRepository.findById(movie.getId()).get();
        Instant updatedAtAfterModificationAndReload = movie.getUpdatedAt();
        assertTrue(updatedAtAfterModificationAndReload.isAfter(updatedAtAfterReload));
    }

    @Test
    public void testGetMoviesLeaderBoard() {
        RegisteredUser c = entityCreator.createRegisteredUser();

        int moviesCount = 100;
        Set<UUID> movieIds = new HashSet<>();
        for (int i = 0; i < moviesCount; i++) {
            Movie m = entityCreator.createMovie();
            movieIds.add(m.getId());

            entityCreator.createMovieRate(c, m, (int) (Math.random() * 10));
            entityCreator.createMovieRate(c, m, (int) (Math.random() * 10));
            entityCreator.createMovieRate(c, m, null);
        }

        List<MovieInLeaderBoardReadDTO> moviesLeaderBoard = movieRepository.getMoviesLeaderBoard();
        Assertions.assertThat(moviesLeaderBoard)
                .isSortedAccordingTo(Comparator.comparing((MovieInLeaderBoardReadDTO::getAvgRating)).reversed());

        Assert.assertEquals(movieIds, moviesLeaderBoard.stream().map(MovieInLeaderBoardReadDTO::getId).collect(
                Collectors.toSet()));

        for (MovieInLeaderBoardReadDTO m : moviesLeaderBoard) {
            Assert.assertNotNull(m.getName());
            Assert.assertNotNull(m.getAvgRating());
            Assertions.assertThat(m.getRatesCount()).isEqualTo(2);
        }
    }

    @Test
    public void getMovieByName() {
        Movie existingMovie = flatGenerator.generateRandomObject(Movie.class);
        existingMovie.setName("The Lion King");
        existingMovie = movieRepository.save(existingMovie);

        Movie foundMovie = movieRepository.findByName(existingMovie.getName());
        assertEquals(existingMovie.getId(), foundMovie.getId());
    }

    @Test
    public void getMovieByNameAndReleaseDate() {
        entityCreator.createMovie();
        Movie existingMovie = flatGenerator.generateRandomObject(Movie.class);
        existingMovie.setName("The Lion King");
        existingMovie.setReleaseDate(LocalDate.of(2020, 3, 8));
        existingMovie = movieRepository.save(existingMovie);

        Movie foundMovie = movieRepository.findByNameAndReleaseYear(existingMovie.getName(), 2020);
        assertEquals(existingMovie.getId(), foundMovie.getId());
    }

    @Test
    public void testGetMovieByNameAndReleaseYearWithWrongYear() {
        Movie existingMovie = flatGenerator.generateRandomObject(Movie.class);
        existingMovie.setName("The Lion King");
        existingMovie.setReleaseDate(LocalDate.of(2020, 3, 8));
        existingMovie = movieRepository.save(existingMovie);

        Movie foundMovie = movieRepository.findByNameAndReleaseYear(existingMovie.getName(), 2019);
        assertNull(foundMovie);
    }

    private List<Movie> createTestMovies() {

        Movie movie1 = movieRepository.save(new Movie());
        Movie movie2 = movieRepository.save(new Movie());
        Movie movie3 = movieRepository.save(new Movie());
        Movie movie4 = movieRepository.save(new Movie());

        Genre genre1 = entityCreator.createGenre("Comedy");
        Genre genre2 = entityCreator.createGenre("Drama");
        Genre genre3 = entityCreator.createGenre("Adventure");

        ProductionCompany company1 = entityCreator.createProductionCompany("Fantasy Films");
        ProductionCompany company2 = entityCreator.createProductionCompany("Walt Disney Pictures");

        Actor actor1 = entityCreator.createActor("JD McCrary");
        ActorRole actorRole1 = entityCreator.createActorRole(actor1, movie1, "Simba");

        Actor actor2 = entityCreator.createActor("Chiwetel Ejiofor");
        ActorRole actorRole2 = entityCreator.createActorRole(actor2, movie1, "Scar");

        Actor actor3 = entityCreator.createActor("John Oliver");
        ActorRole actorRole3 = entityCreator.createActorRole(actor3, movie1, "Zazu");

        Actor actor4 = entityCreator.createActor("Seth Rogen");
        ActorRole actorRole4 = entityCreator.createActorRole(actor4, movie1, "Pumbaa");

        Actor actor5 = entityCreator.createActor("Billy Eichner");
        ActorRole actorRole5 = entityCreator.createActorRole(actor4, movie1, "Timon");

        ActorRole actorRole6 = entityCreator.createActorRole(actor3, movie3, "Zazu");

        movie1.setName("The Lion King");
        movie1.setCountry("USA");
        entityCreator.linkMovieAndCompany(movie1, company1);
        entityCreator.linkMovieAndCompany(movie1, company2);
        movie1.setAvgRating(9.3);
        movie1.setReleaseDate(LocalDate.of(2019, 1, 2));
        entityCreator.linkMovieAndGenre(movie1, genre1);
        entityCreator.linkMovieAndGenre(movie1, genre2);
        entityCreator.linkMovieAndGenre(movie1, genre3);
        movie1.getActorRoles().addAll(Arrays.asList(actorRole1, actorRole2, actorRole3, actorRole4, actorRole5));

        movie2.setName("The Lion King New");
        movie2.setCountry("The US of America");
        entityCreator.linkMovieAndCompany(movie2, company1);
        entityCreator.linkMovieAndGenre(movie2, genre1);
        movie2.setAvgRating(9.3);
        movie2.setReleaseDate(LocalDate.of(2019, 1, 1));
        movie2.getActorRoles().add(actorRole2);

        movie3.setName("Lion True King");
        movie3.setCountry("usa");
        entityCreator.linkMovieAndCompany(movie3, company2);
        entityCreator.linkMovieAndGenre(movie3, genre2);
        movie3.setAvgRating(9.1);
        movie3.setReleaseDate(LocalDate.of(2020, 1, 1));
        movie3.getActorRoles().add(actorRole6);

        movie4.setName("THE LION KING");
        movie4.setCountry("US");
        entityCreator.linkMovieAndCompany(movie4, company1);
        entityCreator.linkMovieAndGenre(movie4, genre2);
        entityCreator.linkMovieAndGenre(movie4, genre3);
        movie4.setAvgRating(9.0);
        movie4.setReleaseDate(LocalDate.of(2019, 11, 1));
        movie4.getActorRoles().addAll(Arrays.asList(actorRole4, actorRole5));

        movieRepository.saveAll(Arrays.asList(movie1, movie2, movie3, movie4));
        List<Movie> movies = new ArrayList<>(Arrays.asList(movie1, movie2, movie3, movie4));
        return movies;
    }
}
