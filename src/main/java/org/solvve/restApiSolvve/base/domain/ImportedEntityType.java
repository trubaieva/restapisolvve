package org.solvve.restapisolvve.base.domain;

public enum ImportedEntityType {
    MOVIE,
    PERSON,
    CREW,
    CAST
}
