package org.solvve.restapisolvve.base.repository;

import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.NewsLike;
import org.solvve.restapisolvve.base.util.EntityCreator;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;

import static org.junit.Assert.*;

public class NewsLikeRepositoryTest extends BaseTest {

    @Autowired
    private NewsLikeRepository newsLikeRepository;

    @Autowired
    private EntityCreator entityCreator;

    @Test
    public void testCreatedAtIsSet() {
        NewsLike newsLike = entityCreator.createNewsLike();

        Instant createdAtBeforeReload = newsLike.getCreatedAt();
        assertNotNull(createdAtBeforeReload);
        newsLike = newsLikeRepository.findById(newsLike.getId()).get();

        Instant createdAtAfterReload = newsLike.getCreatedAt();
        assertNotNull(createdAtAfterReload);
        assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        NewsLike newsLike = entityCreator.createNewsLike();

        Instant updatedAtBeforeReload = newsLike.getUpdatedAt();
        assertNotNull(updatedAtBeforeReload);
        newsLike = newsLikeRepository.findById(newsLike.getId()).get();

        Instant updatedAtAfterReload = newsLike.getUpdatedAt();
        assertNotNull(updatedAtAfterReload);
        assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        newsLike.setIsLiked(true);
        newsLikeRepository.save(newsLike);
        newsLike = newsLikeRepository.findById(newsLike.getId()).get();
        Instant updatedAtAfterModificationAndReload = newsLike.getUpdatedAt();
        assertTrue(updatedAtAfterModificationAndReload.isAfter(updatedAtAfterReload));
    }

}
