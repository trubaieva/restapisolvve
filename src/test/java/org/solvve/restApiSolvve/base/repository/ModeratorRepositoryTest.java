package org.solvve.restapisolvve.base.repository;

import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.Moderator;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;

import static org.junit.Assert.*;

public class ModeratorRepositoryTest extends BaseTest {

    @Autowired
    private ModeratorRepository moderatorRepository;

    @Test
    public void testCreatedAtIsSet() {
        Moderator m = new Moderator();
        m = moderatorRepository.save(m);

        Instant createdAtBeforeReload = m.getCreatedAt();
        assertNotNull(createdAtBeforeReload);
        m = moderatorRepository.findById(m.getId()).get();

        Instant createdAtAfterReload = m.getCreatedAt();
        assertNotNull(createdAtAfterReload);
        assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        Moderator m = new Moderator();
        m = moderatorRepository.save(m);

        Instant updatedAtBeforeReload = m.getUpdatedAt();
        assertNotNull(updatedAtBeforeReload);
        m = moderatorRepository.findById(m.getId()).get();

        Instant updatedAtAfterReload = m.getUpdatedAt();
        assertNotNull(updatedAtAfterReload);
        assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        m.setName("Sveta");
        moderatorRepository.save(m);
        m = moderatorRepository.findById(m.getId()).get();
        Instant updatedAtAfterModificationAndReload = m.getUpdatedAt();
        assertTrue(updatedAtAfterModificationAndReload.isAfter(updatedAtAfterReload));
    }
}
