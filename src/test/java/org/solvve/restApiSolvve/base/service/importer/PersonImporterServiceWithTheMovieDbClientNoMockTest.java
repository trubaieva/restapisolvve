package org.solvve.restapisolvve.base.service.importer;

import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.client.themoviedb.TheMovieDbClient;
import org.solvve.restapisolvve.base.client.themoviedb.dto.PersonReadDTO;
import org.solvve.restapisolvve.base.domain.Person;
import org.solvve.restapisolvve.base.exception.ImportAlreadyPerformedException;
import org.solvve.restapisolvve.base.exception.ImportedEntityAlreadyExistException;
import org.solvve.restapisolvve.base.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

import static org.junit.Assert.assertEquals;

public class PersonImporterServiceWithTheMovieDbClientNoMockTest extends BaseTest {

    @Autowired
    private TheMovieDbClient movieDbClient;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private PersonImporterService personImporterService;

    @Test
    public void testPersonImportFromDbClient()
            throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {
        String personId = "1100";
        PersonReadDTO dto = movieDbClient.getPerson(personId, null);

        UUID importedPersonId = personImporterService.importPerson(personId);
        Person person = personRepository.findById(importedPersonId).get();

        assertEquals(personId, dto.getId());
        assertEquals(person.getName(), dto.getName());
        assertEquals(person.getBirthDate(), dto.getBirthday());
        assertEquals(person.getBirthPlace(), dto.getPlaceOfBirth());
        assertEquals(2, (int) dto.getGender());
        assertEquals(person.getDeathDate(), dto.getDeathday());
        assertEquals(person.getKnownForDepartment().toString(), dto.getKnownForDepartment().toUpperCase());
        assertEquals(person.getBiography(), dto.getBiography());
    }
}
