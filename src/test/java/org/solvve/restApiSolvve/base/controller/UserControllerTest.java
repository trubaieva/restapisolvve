package org.solvve.restapisolvve.base.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.solvve.restapisolvve.base.domain.User;
import org.solvve.restapisolvve.base.dto.user.UserCreateDTO;
import org.solvve.restapisolvve.base.dto.user.UserPatchDTO;
import org.solvve.restapisolvve.base.dto.user.UserPutDTO;
import org.solvve.restapisolvve.base.dto.user.UserReadDTO;
import org.solvve.restapisolvve.base.exception.EntityNotFoundException;
import org.solvve.restapisolvve.base.exception.handler.ErrorInfo;
import org.solvve.restapisolvve.base.service.UserService;
import org.solvve.restapisolvve.base.util.DtoCreator;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = UserController.class)
public class UserControllerTest extends BaseControllerTest {

    @MockBean
    private UserService userService;

    private DtoCreator dtoCreator = new DtoCreator();

    @WithMockUser
    @Test
    public void testGetUsers() throws Exception {
        List<UserReadDTO> userReadDTOs = new ArrayList<>();
        UserReadDTO userReadDTO;
        for (int i = 0; i < 5; i++) {
            userReadDTO = dtoCreator.createUserRead();
            userReadDTOs.add(userReadDTO);
        }
        Mockito.when(userService.getUsers()).thenReturn(userReadDTOs);

        String resultJson = mvc.perform(get("/api/v1/users"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<UserReadDTO> actualUsers = objectMapper.readValue(resultJson, new TypeReference<List<UserReadDTO>>() {
        });
        Assertions.assertThat(actualUsers).isEqualTo(userReadDTOs);

        Mockito.verify(userService).getUsers();
    }

    @WithMockUser
    @Test
    public void testGetUser() throws Exception {
        UserReadDTO user = dtoCreator.createUserRead();

        Mockito.when(userService.getUser(user.getId())).thenReturn(user);

        String resultJson = mvc.perform(get("/api/v1/users/{id}", user.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        UserReadDTO actualUser = objectMapper.readValue(resultJson, UserReadDTO.class);
        Assertions.assertThat(actualUser).isEqualToComparingFieldByField(user);

        Mockito.verify(userService).getUser(user.getId());
    }

    @WithMockUser
    @Test
    public void testGetUserWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(User.class, wrongId);
        Mockito.when(userService.getUser(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/users/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @WithMockUser
    @Test
    public void testGetUserWrongFormatId() throws Exception {
        String wrongFormatId = "123";
        ErrorInfo expectedError = new ErrorInfo(HttpStatus.BAD_REQUEST,
                MethodArgumentTypeMismatchException.class,
                "Failed to convert value of type 'java.lang.String' to required type " +
                        "'java.util.UUID'; nested exception is java.lang" +
                        ".IllegalArgumentException: Invalid UUID string: 123"
        );
        String resultJson = mvc.perform(get("/api/v1/users/{id}", wrongFormatId))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        ErrorInfo actualError = objectMapper.readValue(resultJson, ErrorInfo.class);
        Assertions.assertThat(actualError).isEqualToComparingFieldByField(expectedError);
    }

    @Test
    public void testCreateUser() throws Exception {
        UserCreateDTO create = dtoCreator.createUserCreateDTO();

        UserReadDTO read = dtoCreator.createUserRead();

        Mockito.when(userService.createUser(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/users")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        UserReadDTO actualUser = objectMapper.readValue(resultJson, UserReadDTO.class);
        Assertions.assertThat(actualUser).isEqualToComparingFieldByField(read);

        Mockito.verify(userService).createUser(create);
    }

    @WithMockUser
    @Test
    public void testPatchUser() throws Exception {
        UserPatchDTO patchDTO = dtoCreator.createUserPatchDTO();

        UserReadDTO read = dtoCreator.createUserRead();

        Mockito.when(userService.patchUser(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/users/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        UserReadDTO actualUser = objectMapper.readValue(resultJson, UserReadDTO.class);
        Assert.assertEquals(read, actualUser);

        Mockito.verify(userService).patchUser(read.getId(), patchDTO);
    }

    @WithMockUser
    @Test
    public void testUpdateUser() throws Exception {
        UserPutDTO putDTO = dtoCreator.createUserPutDTO();

        UserReadDTO read = dtoCreator.createUserRead();

        Mockito.when(userService.updateUser(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/users/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        UserReadDTO actualUser = objectMapper.readValue(resultJson, UserReadDTO.class);
        Assert.assertEquals(read, actualUser);

        Mockito.verify(userService).updateUser(read.getId(), putDTO);
    }

    @WithMockUser
    @Test
    public void testDeleteUser() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/users/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(userService).deleteUser(id);
    }

    @WithMockUser
    @Test
    public void testNoSession() throws Exception {
        UUID wrongId = UUID.randomUUID();

        Mockito.when(userService.getUser(wrongId)).thenReturn(new UserReadDTO());

        MvcResult mvcResult = mvc.perform(get("/api/v1/users/{id}", wrongId))
                .andExpect(status().isOk())
                .andReturn();
        Assert.assertNull(mvcResult.getRequest().getSession(false));
    }

}
