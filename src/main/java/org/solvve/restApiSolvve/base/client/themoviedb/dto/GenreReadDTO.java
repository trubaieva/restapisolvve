package org.solvve.restapisolvve.base.client.themoviedb.dto;

import lombok.Data;

@Data
public class GenreReadDTO {
    private String id;
    private String name;
}
