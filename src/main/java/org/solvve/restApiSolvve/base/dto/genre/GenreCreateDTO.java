package org.solvve.restapisolvve.base.dto.genre;

import lombok.Data;

@Data
public class GenreCreateDTO {
    private String name;
}
