package org.solvve.restapisolvve.base.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.UUID;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class UnexpectedRequestStatusException extends RuntimeException {
    public UnexpectedRequestStatusException(Class entityClass, UUID id) {
        super(String.format("Entity %s with id=%s has invalid status for processing. Expected status is "
                        + "NEED_TO_VERIFY.",
                entityClass.getSimpleName(), id));
    }
}
