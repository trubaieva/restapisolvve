package org.solvve.restapisolvve.base.dto.genre;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class GenreReadDTO {
    private UUID id;
    private String name;
    private Instant createdAt;
    private Instant updatedAt;
}
