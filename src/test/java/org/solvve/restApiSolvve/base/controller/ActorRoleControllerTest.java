package org.solvve.restapisolvve.base.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.solvve.restapisolvve.base.domain.ActorRole;
import org.solvve.restapisolvve.base.dto.actorrole.ActorRoleCreateDTO;
import org.solvve.restapisolvve.base.dto.actorrole.ActorRolePatchDTO;
import org.solvve.restapisolvve.base.dto.actorrole.ActorRolePutDTO;
import org.solvve.restapisolvve.base.dto.actorrole.ActorRoleReadDTO;
import org.solvve.restapisolvve.base.exception.EntityNotFoundException;
import org.solvve.restapisolvve.base.exception.handler.ErrorInfo;
import org.solvve.restapisolvve.base.service.ActorRoleService;
import org.solvve.restapisolvve.base.util.DtoCreator;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(controllers = ActorRoleController.class)
public class ActorRoleControllerTest extends BaseControllerTest {

    @MockBean
    private ActorRoleService actorRoleService;

    private DtoCreator dtoCreator = new DtoCreator();

    @Test
    public void testGetMovieActorRoles() throws Exception {
        UUID movieId = UUID.randomUUID();
        List<ActorRoleReadDTO> actorRoleReadDTOs = new ArrayList<>();
        ActorRoleReadDTO actorRoleReadDTO;
        for (int i = 0; i < 5; i++) {
            actorRoleReadDTO = dtoCreator.createActorRoleRead();
            actorRoleReadDTO.setMovieId(movieId);
            actorRoleReadDTOs.add(actorRoleReadDTO);
        }

        Mockito.when(actorRoleService.getMovieActorRoles(movieId)).thenReturn(actorRoleReadDTOs);

        String resultJson = mvc.perform(get("/api/v1/movies/{movieId}/actor-roles", movieId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<ActorRoleReadDTO> actualActorRoles = objectMapper.readValue(resultJson,
                new TypeReference<>() {
                });
        Assert.assertEquals(actorRoleReadDTOs, actualActorRoles);

        Mockito.verify(actorRoleService).getMovieActorRoles(movieId);
    }

    @Test
    public void testGetMovieActorRole() throws Exception {
        ActorRoleReadDTO actorRole = dtoCreator.createActorRoleRead();

        Mockito.when(actorRoleService.getMovieActorRole(actorRole.getMovieId(), actorRole.getId()))
                .thenReturn(actorRole);

        String resultJson = mvc.perform(get("/api/v1/movies/{movieId}/actor-roles/{id}",
                actorRole.getMovieId(),
                actorRole.getId()
        ))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ActorRoleReadDTO actualActorRole = objectMapper.readValue(resultJson, ActorRoleReadDTO.class);
        Assertions.assertThat(actualActorRole).isEqualToComparingFieldByField(actorRole);

        Mockito.verify(actorRoleService).getMovieActorRole(actorRole.getMovieId(), actorRole.getId());
    }

    @Test
    public void testGetActorRoleWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(ActorRole.class, wrongId);
        Mockito.when(actorRoleService.getMovieActorRole(wrongId, wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/movies/{movieId}/actor-roles/{id}", wrongId, wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetActorRoleWrongFormatId() throws Exception {
        String wrongFormatId = "123";
        UUID randomId = UUID.randomUUID();
        ErrorInfo expectedError = new ErrorInfo(HttpStatus.BAD_REQUEST,
                MethodArgumentTypeMismatchException.class,
                "Failed to convert value of type 'java.lang.String' to required type " +
                        "'java.util.UUID'; nested exception is java.lang" +
                        ".IllegalArgumentException: Invalid UUID string: 123"
        );
        String resultJson = mvc.perform(get("/api/v1/movies/{movieId}/actor-roles/{id}", randomId, wrongFormatId))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        ErrorInfo actualError = objectMapper.readValue(resultJson, ErrorInfo.class);
        Assertions.assertThat(actualError).isEqualToComparingFieldByField(expectedError);
    }

    @Test
    public void testCreateMovieActorRole() throws Exception {
        ActorRoleCreateDTO create = dtoCreator.createActorRoleCreateDTO();
        UUID movieId = UUID.randomUUID();
        ActorRoleReadDTO read = dtoCreator.createActorRoleRead();

        Mockito.when(actorRoleService.createMovieActorRole(movieId, create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/movies/{movieId}/actor-roles", movieId)
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ActorRoleReadDTO actualActorRole = objectMapper.readValue(resultJson, ActorRoleReadDTO.class);
        Assertions.assertThat(actualActorRole).isEqualToComparingFieldByField(read);

        Mockito.verify(actorRoleService).createMovieActorRole(movieId, create);
    }

    @Test
    public void testCreateMovieActorRoleValidationFailed() throws Exception {
        ActorRoleCreateDTO create = new ActorRoleCreateDTO();
        UUID movieId = UUID.randomUUID();

        String resultJson = mvc.perform(post("/api/v1/movies/{movieId}/actor-roles", movieId)
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(actorRoleService, Mockito.never())
                .createMovieActorRole(ArgumentMatchers.any(), ArgumentMatchers.any());
    }

    @Test
    public void testPatchMovieActorRole() throws Exception {
        ActorRolePatchDTO patchDTO = dtoCreator.createActorRolePatchDTO();

        ActorRoleReadDTO read = dtoCreator.createActorRoleRead();

        Mockito.when(actorRoleService.patchMovieActorRole(read.getMovieId(), read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/movies/{movieId}/actor-roles/{id}",
                read.getMovieId(),
                read.getId().toString()
        )
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ActorRoleReadDTO actualActorRole = objectMapper.readValue(resultJson, ActorRoleReadDTO.class);
        Assert.assertEquals(read, actualActorRole);

        Mockito.verify(actorRoleService).patchMovieActorRole(read.getMovieId(), read.getId(), patchDTO);
    }

    @Test
    public void testUpdateMovieActorRole() throws Exception {
        ActorRolePutDTO putDTO = dtoCreator.createActorRolePutDTO();

        ActorRoleReadDTO read = dtoCreator.createActorRoleRead();

        Mockito.when(actorRoleService.updateMovieActorRole(read.getMovieId(), read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/movies/{movieId}/actor-roles/{id}",
                read.getMovieId(),
                read.getId().toString()
        )
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ActorRoleReadDTO actualActorRole = objectMapper.readValue(resultJson, ActorRoleReadDTO.class);
        Assert.assertEquals(read, actualActorRole);

        Mockito.verify(actorRoleService).updateMovieActorRole(read.getMovieId(), read.getId(), putDTO);
    }

    @Test
    public void testDeleteMovieActorRole() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/movies/{movieId}/actor-roles/{id}", id, id.toString())).andExpect(status().isOk());

        Mockito.verify(actorRoleService).deleteMovieActorRole(id, id);
    }

}
