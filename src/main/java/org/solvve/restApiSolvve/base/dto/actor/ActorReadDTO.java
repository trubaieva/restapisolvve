package org.solvve.restapisolvve.base.dto.actor;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class ActorReadDTO {
    private UUID id;
    private UUID personId;
    private Double avgRatingByRoles;
    private Double avgMovieRating;
    private Instant createdAt;
    private Instant updatedAt;
}
