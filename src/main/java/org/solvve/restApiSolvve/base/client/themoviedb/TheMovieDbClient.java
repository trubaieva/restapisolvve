package org.solvve.restapisolvve.base.client.themoviedb;

import org.solvve.restapisolvve.base.client.themoviedb.dto.CreditReadDTO;
import org.solvve.restapisolvve.base.client.themoviedb.dto.MovieReadDTO;
import org.solvve.restapisolvve.base.client.themoviedb.dto.MoviesPageDTO;
import org.solvve.restapisolvve.base.client.themoviedb.dto.PersonReadDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "api.themoviedb.org", url = "${themoviedb.api.url}", configuration = TheMovieDbClientConfig.class)
public interface TheMovieDbClient {

    @RequestMapping(method = RequestMethod.GET, value = "/movie/{movieId}")
    MovieReadDTO getMovie(@PathVariable("movieId") String movieId, @RequestParam String language);

    @RequestMapping(method = RequestMethod.GET, value = "/movie/{movieId}")
    MovieReadDTO getMovie(@PathVariable("movieId") String movieId);

    @RequestMapping(method = RequestMethod.GET, value = "/movie/top_rated")
    MoviesPageDTO getTopRatedMovies();

    @RequestMapping(method = RequestMethod.GET, value = "/person/{personId}")
    PersonReadDTO getPerson(@PathVariable("personId") String personId, @RequestParam String language);

    @RequestMapping(method = RequestMethod.GET, value = "/person/{personId}")
    PersonReadDTO getPerson(@PathVariable("personId") String personId);

    @RequestMapping(method = RequestMethod.GET, value = "/movie/{movieId}/credits")
    CreditReadDTO getMovieCredits(@PathVariable("movieId") String movieId);
}
