package org.solvve.restapisolvve.base.event;

import lombok.Data;

import java.util.UUID;

@Data
public class ReviewVisibilityChangedEvent {
    private UUID reviewId;
    private Boolean isVisible;
}
