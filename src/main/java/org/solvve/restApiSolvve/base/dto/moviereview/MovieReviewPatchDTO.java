package org.solvve.restapisolvve.base.dto.moviereview;

import lombok.Data;

import java.util.UUID;

@Data
public class MovieReviewPatchDTO {
    private String textReview;
    private Boolean isVisible;
    private Integer likeCount;
    private UUID movieId;
}
