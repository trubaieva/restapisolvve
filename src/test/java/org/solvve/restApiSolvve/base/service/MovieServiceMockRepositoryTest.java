package org.solvve.restapisolvve.base.service;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.mockito.Mockito;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.Movie;
import org.solvve.restapisolvve.base.dto.movie.MovieFilter;
import org.solvve.restapisolvve.base.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MovieServiceMockRepositoryTest extends BaseTest {

    @MockBean
    private MovieRepository movieRepository;

    @Autowired
    private MovieService movieService;

    @Test
    public void testGetMoviesByFilter() {
        List<Movie> movies = Arrays.asList(new Movie());
        Page<Movie> moviePage = new PageImpl(movies);

        MovieFilter filter = generator.generateRandomObject(MovieFilter.class);

        Mockito.when(movieRepository.findByFilter(filter, Pageable.unpaged())).thenReturn(moviePage);

        Assertions.assertThat(movieService.getMovies(filter, Pageable.unpaged()).getData())
                .extracting("id")
                .containsExactlyInAnyOrderElementsOf(movies.stream().map(Movie::getId).collect(Collectors.toList()));

        Mockito.verify(movieRepository).findByFilter(filter, Pageable.unpaged());
    }

}
