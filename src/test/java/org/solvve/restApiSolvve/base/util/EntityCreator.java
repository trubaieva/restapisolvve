package org.solvve.restapisolvve.base.util;

import org.solvve.restapisolvve.base.domain.*;
import org.solvve.restapisolvve.base.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

@Component
public class EntityCreator extends AbstractCreator {

    @Autowired
    UserRoleRepository userRoleRepository;

    @PersistenceContext
    EntityManager entityManager;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private CrewRepository crewRepository;

    @Autowired
    private ActorRoleRepository actorRoleRepository;

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private ProductionCompanyRepository productionCompanyRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private ModeratorRepository moderatorRepository;

    @Autowired
    private ContentManagerRepository contentManagerRepository;

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private CheckRequestRepository checkRequestRepository;

    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private RequestToCorrectRepository requestToCorrectRepository;

    @Autowired
    private LikeRepository likeRepository;

    @Autowired
    private RateRepository rateRepository;

    @Autowired
    private RoleRateRepository roleRateRepository;

    @Autowired
    private RoleReviewRepository roleReviewRepository;

    @Autowired
    private ReviewLikeRepository reviewLikeRepository;

    @Autowired
    private NewsLikeRepository newsLikeRepository;

    @Autowired
    private MovieRateRepository movieRateRepository;

    @Autowired
    private MovieReviewRepository movieReviewRepository;

    public User createUser() {
        User user = generateFlatEntityWithoutId(User.class);
        return userRepository.save(user);
    }

    public Movie createMovie() {
        Movie movie = generateFlatEntityWithoutId(Movie.class);
        return movieRepository.save(movie);
    }

    public Movie createMovieWithoutAvgRating() {
        Movie movie = generateFlatEntityWithoutId(Movie.class);
        movie.setAvgRating(null);
        return movieRepository.save(movie);
    }

    public RegisteredUser createRegisteredUser() {
        RegisteredUser creator = generateFlatEntityWithoutId(RegisteredUser.class);
        return registeredUserRepository.save(creator);
    }

    public MovieRate createMovieRate(RegisteredUser creator, Movie movie, Integer rate) {
        MovieRate movieRate = generateFlatEntityWithoutId(MovieRate.class);
        movieRate.setCreator(creator);
        movieRate.setMovie(movie);
        movieRate.setRate(rate);
        return movieRateRepository.save(movieRate);
    }

    public Movie createMovie(String name) {
        Movie movie = generateFlatEntityWithoutId(Movie.class);
        movie.setName(name);
        return movieRepository.save(movie);
    }

    public Crew createCrew() {
        Crew crew = generateFlatEntityWithoutId(Crew.class);
        crew.setPerson(createPerson());
        crew.setMovie(createMovie());
        return crewRepository.save(crew);
    }

    public Crew createCrew(Movie movie) {
        Crew crew = generateFlatEntityWithoutId(Crew.class);
        crew.setPerson(createPerson());
        crew.setMovie(movie);
        return crewRepository.save(crew);
    }

    public Person createPerson() {
        Person person = generateFlatEntityWithoutId(Person.class);
        personRepository.save(person);
        return person;
    }

    public Person createPerson(String name) {
        Person person = generateFlatEntityWithoutId(Person.class);
        person.setName(name);
        return personRepository.save(person);
    }

    public ActorRole createActorRole() {
        ActorRole actorRole = generateFlatEntityWithoutId(ActorRole.class);
        actorRole.setActor(createActor());
        actorRole.setMovie(createMovie());
        return actorRoleRepository.save(actorRole);
    }

    public ActorRole createActorRoleWithoutAvgRating() {
        ActorRole actorRole = generateFlatEntityWithoutId(ActorRole.class);
        actorRole.setActor(createActor());
        actorRole.setMovie(createMovie());
        actorRole.setAvgRating(null);
        return actorRoleRepository.save(actorRole);
    }

    public ActorRole createActorRole(Actor actor, Movie movie) {
        ActorRole actorRole = generateFlatEntityWithoutId(ActorRole.class);
        actorRole.setActor(actor);
        actorRole.setMovie(movie);
        return actorRoleRepository.save(actorRole);
    }

    public ActorRole createActorRole(Actor actor, Movie movie, String name) {
        ActorRole actorRole = generateFlatEntityWithoutId(ActorRole.class);
        actorRole.setActor(actor);
        actorRole.setMovie(movie);
        actorRole.setCharacter(name);
        return actorRoleRepository.save(actorRole);
    }

    public Actor createActor() {
        Actor actor = generateFlatEntityWithoutId(Actor.class);
        actor.setPerson(createPerson());
        return actorRepository.save(actor);
    }

    public Actor createActor(Person person) {
        Actor actor = generateFlatEntityWithoutId(Actor.class);
        actor.setPerson(person);
        return actorRepository.save(actor);
    }

    public Actor createActor(String name) {
        Actor actor = generateFlatEntityWithoutId(Actor.class);
        actor.setPerson(createPerson(name));
        return actorRepository.save(actor);
    }

    public MovieReview createMovieReview(RegisteredUser creator, Movie movie) {
        MovieReview movieReview = generateFlatEntityWithoutId(MovieReview.class);
        movieReview.setCreator(creator);
        movieReview.setMovie(movie);
        return movieReviewRepository.save(movieReview);
    }

    public Genre createGenre() {
        Genre genre = generateFlatEntityWithoutId(Genre.class);
        return genreRepository.save(genre);
    }

    public Genre createGenre(String name) {
        Genre genre = generateFlatEntityWithoutId(Genre.class);
        genre.setName(name);
        return genreRepository.save(genre);
    }

    public void linkMovieAndGenre(Movie movie, Genre genre) {
        if (!genre.getMovies().contains(movie)) {
            genre.getMovies().add(movie);
        }
        if (!movie.getGenres().contains(genre)) {
            movie.getGenres().add(genre);
            movieRepository.save(movie);
        }
    }

    public ProductionCompany createProductionCompany() {
        ProductionCompany productionCompany = generateFlatEntityWithoutId(ProductionCompany.class);
        return productionCompanyRepository.save(productionCompany);
    }

    public ProductionCompany createProductionCompany(String name) {
        ProductionCompany productionCompany = generateFlatEntityWithoutId(ProductionCompany.class);
        productionCompany.setName(name);
        return productionCompanyRepository.save(productionCompany);
    }

    public void linkMovieAndCompany(Movie movie, ProductionCompany company) {
        if (!company.getMovies().contains(movie)) {
            company.getMovies().add(movie);
        }
        if (!movie.getProductionCompanies().contains(company)) {
            movie.getProductionCompanies().add(company);
            movieRepository.save(movie);
        }
    }

    public News createNews() {
        News news = generateFlatEntityWithoutId(News.class);
        return newsRepository.save(news);
    }

    public void linkMovieAndNews(Movie movie, News news) {
        if (!news.getMovies().contains(movie)) {
            news.getMovies().add(movie);
        }
        if (!movie.getNews().contains(news)) {
            movie.getNews().add(news);
            movieRepository.save(movie);
        }
    }

    public void linkPersonAndNews(Person person, News news) {
        if (!news.getPersons().contains(person)) {
            news.getPersons().add(person);
        }
        if (!person.getNews().contains(news)) {
            person.getNews().add(news);
            personRepository.save(person);
        }
    }

    public Rate createRate() {
        Rate rate = generateFlatEntityWithoutId(Rate.class);
        rate.setCreator(createRegisteredUser());
        return rateRepository.save(rate);
    }

    public RoleRate createRoleRate() {
        RoleRate roleRate = generateFlatEntityWithoutId(RoleRate.class);
        roleRate.setCreator(createRegisteredUser());
        roleRate.setActorRole(createActorRole());
        return roleRateRepository.save(roleRate);
    }

    public MovieRate createMovieRate() {
        MovieRate movieRate = generateFlatEntityWithoutId(MovieRate.class);
        movieRate.setCreator(createRegisteredUser());
        movieRate.setMovie(createMovie());
        return movieRateRepository.save(movieRate);
    }

    public RoleRate createRoleRate(RegisteredUser creator, ActorRole actorRole, Integer rate) {
        RoleRate roleRate = generateFlatEntityWithoutId(RoleRate.class);
        roleRate.setCreator(creator);
        roleRate.setActorRole(actorRole);
        roleRate.setRate(rate);
        return roleRateRepository.save(roleRate);
    }

    public Like createLike() {
        Like like = generateFlatEntityWithoutId(Like.class);
        like.setCreator(createRegisteredUser());
        return likeRepository.save(like);
    }

    public ReviewLike createReviewLike() {
        ReviewLike reviewLike = generateFlatEntityWithoutId(ReviewLike.class);
        reviewLike.setCreator(createRegisteredUser());
        reviewLike.setReview(createReview());
        return reviewLikeRepository.save(reviewLike);
    }

    public NewsLike createNewsLike() {
        NewsLike newsLike = generateFlatEntityWithoutId(NewsLike.class);
        newsLike.setCreator(createRegisteredUser());
        newsLike.setNews(createNews());
        return newsLikeRepository.save(newsLike);
    }

    public Review createReview() {
        Review review = generateFlatEntityWithoutId(Review.class);
        review.setCreator(createRegisteredUser());
        return reviewRepository.save(review);
    }

    public RoleReview createRoleReview() {
        RoleReview roleReview = generateFlatEntityWithoutId(RoleReview.class);
        roleReview.setCreator(createRegisteredUser());
        roleReview.setActorRole(createActorRole());
        return roleReviewRepository.save(roleReview);
    }

    public MovieReview createMovieReview() {
        MovieReview movieReview = generateFlatEntityWithoutId(MovieReview.class);
        movieReview.setCreator(createRegisteredUser());
        movieReview.setMovie(createMovie());
        return movieReviewRepository.save(movieReview);
    }

    public ContentManager createContentManager() {
        ContentManager verifier = generateFlatEntityWithoutId(ContentManager.class);
        return contentManagerRepository.save(verifier);
    }

    public Moderator createModerator() {
        Moderator verifier = generateFlatEntityWithoutId(Moderator.class);
        return moderatorRepository.save(verifier);
    }

    public CheckRequest createCheckRequest() {
        CheckRequest checkRequest = generateFlatEntityWithoutId(CheckRequest.class);
        checkRequest.setCreator(createRegisteredUser());
        checkRequest.setModerator(createModerator());
        checkRequest.setReview(createReview());
        return checkRequestRepository.save(checkRequest);
    }

    public RequestToCorrect createRequestToCorrect() {
        RequestToCorrect requestToCorrect = generateFlatEntityWithoutId(RequestToCorrect.class);
        requestToCorrect.setCreator(createRegisteredUser());
        requestToCorrect.setVerifier(createContentManager());
        return requestToCorrectRepository.save(requestToCorrect);
    }

    public RequestToCorrect createRequestToCorrectNews() {
        RequestToCorrect requestToCorrect = new RequestToCorrect();
        requestToCorrect.setCreator(createRegisteredUser());
        News news = createNews();
        requestToCorrect.setObjectId(news.getId());
        requestToCorrect.setWrongData("Lily");
        requestToCorrect.setObjectType(ObjectToCorrect.NEWS);
        requestToCorrect.setStatus(CorrectStatus.CANCEL);
        requestToCorrectRepository.save(requestToCorrect);
        return requestToCorrect;
    }

    public RequestToCorrect createRequestToCorrectNewsMoviePerson() {
        RequestToCorrect requestToCorrect = new RequestToCorrect();
        requestToCorrect.setCreator(createRegisteredUser());
        News news = createNews();
        Movie movie = createMovie("Cinderella");
        linkMovieAndNews(movie, news);
        Person person = createPerson("Lily James");
        linkPersonAndNews(person, news);
        requestToCorrect.setObjectId(news.getId());
        requestToCorrect.setWrongData("Alla");
        requestToCorrect.setObjectType(ObjectToCorrect.NEWS);
        requestToCorrect.setStatus(CorrectStatus.FIXED);
        requestToCorrect.setFixedAt(Instant.now().truncatedTo(ChronoUnit.MICROS));
        requestToCorrect.setVerifier(createContentManager());
        requestToCorrectRepository.save(requestToCorrect);
        return requestToCorrect;
    }

    public RequestToCorrect createRequestToCorrectNewsMovie() {
        RequestToCorrect requestToCorrect = new RequestToCorrect();
        requestToCorrect.setCreator(createRegisteredUser());
        News news = createNews();
        Movie movie = createMovie("Cinderella");
        linkMovieAndNews(movie, news);
        requestToCorrect.setObjectId(news.getId());
        requestToCorrect.setObjectType(ObjectToCorrect.NEWS);
        requestToCorrect.setStatus(CorrectStatus.NEED_TO_VERIFY);
        requestToCorrectRepository.save(requestToCorrect);
        return requestToCorrect;
    }

    public RequestToCorrect createRequestToCorrectNewsPerson() {
        RequestToCorrect requestToCorrect = new RequestToCorrect();
        requestToCorrect.setCreator(createRegisteredUser());
        News news = createNews();
        Person person = createPerson("Lily James");
        linkPersonAndNews(person, news);
        requestToCorrect.setObjectId(news.getId());
        requestToCorrect.setObjectType(ObjectToCorrect.NEWS);
        requestToCorrect.setWrongData("wrong data");
        requestToCorrect.setStatus(CorrectStatus.NEED_TO_VERIFY);
        requestToCorrect.setVerifier(createContentManager());
        requestToCorrectRepository.save(requestToCorrect);
        return requestToCorrect;
    }

    public RequestToCorrect createRequestToCorrectMovie() {
        RequestToCorrect requestToCorrect = new RequestToCorrect();
        requestToCorrect.setCreator(createRegisteredUser());
        Movie movie = createMovie("Cinderella");
        movie.setStoryLine("A girl named Alla (Cinderella) has the purest heart living in a cruel world filled with "
                + "evil stepsisters and an evil stepmother out to ruin Alla's life.");
        movieRepository.save(movie);
        requestToCorrect.setObjectId(movie.getId());
        requestToCorrect.setObjectType(ObjectToCorrect.MOVIE);
        requestToCorrect.setStatus(CorrectStatus.FIXED);
        requestToCorrect.setFixedAt(Instant.now().truncatedTo(ChronoUnit.MICROS));
        requestToCorrect.setVerifier(createContentManager());
        requestToCorrect.setWrongData("Alla");
        requestToCorrect.setProposedFix("Ella");
        requestToCorrect.setCorrectData("Ella");
        requestToCorrectRepository.save(requestToCorrect);
        return requestToCorrect;
    }

    public RequestToCorrect createRequestToCorrectPerson() {
        RequestToCorrect requestToCorrect = new RequestToCorrect();
        requestToCorrect.setCreator(createRegisteredUser());
        requestToCorrect.setObjectId(createPerson("Lily James").getId());
        requestToCorrect.setObjectType(ObjectToCorrect.PERSON);
        requestToCorrect.setWrongData("Lily");
        requestToCorrect.setStatus(CorrectStatus.CANCEL);
        requestToCorrect.setVerifier(createContentManager());
        requestToCorrectRepository.save(requestToCorrect);
        return requestToCorrect;
    }
}
