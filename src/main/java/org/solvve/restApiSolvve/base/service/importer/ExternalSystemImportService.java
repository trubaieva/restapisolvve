package org.solvve.restapisolvve.base.service.importer;

import lombok.extern.slf4j.Slf4j;
import org.solvve.restapisolvve.base.domain.*;
import org.solvve.restapisolvve.base.exception.ImportAlreadyPerformedException;
import org.solvve.restapisolvve.base.repository.ExternalSystemImportRepository;
import org.solvve.restapisolvve.base.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@Service
public class ExternalSystemImportService {

    @Autowired
    private ExternalSystemImportRepository externalSystemImportRepository;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public void validateNotImported(Class<? extends AbstractEntity> entityToImportClass, String idInExternalSystem)
            throws ImportAlreadyPerformedException {

        ImportedEntityType importedEntityType = getImportedEntityType(entityToImportClass);
        ExternalSystemImport esi = externalSystemImportRepository
                .findByIdInExternalSystemAndEntityType(idInExternalSystem, importedEntityType);

        if (esi != null && repositoryHelper.exists(entityToImportClass, esi.getEntityId())) {
            log.warn(String.format("Entity not found, but ExternalSystemImport exists for class=%s, id=%s and id "
                            + "in external system = %s", esi.getEntityType(), esi.getEntityId(),
                    esi.getIdInExternalSystem()));
            throw new ImportAlreadyPerformedException(esi);
        }
    }

    public UUID getEntityIfAlreadyImported(Class<? extends AbstractEntity> entityToImport, String idInExternalSystem) {
        ImportedEntityType importedEntityType = getImportedEntityType(entityToImport);
        ExternalSystemImport esi = externalSystemImportRepository
                .findByIdInExternalSystemAndEntityType(idInExternalSystem, importedEntityType);

        if (esi == null) {
            return null;
        }
        return esi.getEntityId();
    }

    public <T extends AbstractEntity> UUID createExternalSystemImport(T entity, String idInExternalSystem)
            throws ImportAlreadyPerformedException {

        ImportedEntityType importedEntityType = getImportedEntityType(entity.getClass());
        ExternalSystemImport existentEsi = externalSystemImportRepository
                .findByIdInExternalSystemAndEntityType(idInExternalSystem, importedEntityType);

        if (existentEsi != null) {
            if (repositoryHelper.exists(entity.getClass(), existentEsi.getEntityId())) {
                throw new ImportAlreadyPerformedException(existentEsi);
            }

            log.info(String.format("External System Import for type=%s and id=%s was found. It will be deleted "
                    + "before creating new one.", idInExternalSystem, importedEntityType));
            externalSystemImportRepository.delete(existentEsi);
        }

        ExternalSystemImport esi = new ExternalSystemImport();
        esi.setEntityType(importedEntityType);
        esi.setEntityId(entity.getId());
        esi.setIdInExternalSystem(idInExternalSystem);
        esi = externalSystemImportRepository.save(esi);
        return esi.getId();
    }

    private ImportedEntityType getImportedEntityType(Class<? extends AbstractEntity> entityToImport) {

        if (Movie.class.equals(entityToImport)) {
            return ImportedEntityType.MOVIE;
        } else if (Person.class.equals(entityToImport)) {
            return ImportedEntityType.PERSON;
        } else if (Crew.class.equals(entityToImport)) {
            return ImportedEntityType.CREW;
        } else if (ActorRole.class.equals(entityToImport)) {
            return ImportedEntityType.CAST;
        }

        throw new IllegalArgumentException("Importing of entities " + entityToImport + " is not supported");
    }
}
