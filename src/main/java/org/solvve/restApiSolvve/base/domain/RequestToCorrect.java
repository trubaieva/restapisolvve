package org.solvve.restapisolvve.base.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.UUID;

@Entity
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class RequestToCorrect extends AbstractEntity {

    @NotNull
    @Enumerated(EnumType.STRING)
    private ObjectToCorrect objectType;

    @NotNull
    private UUID objectId;

    private String wrongData;

    private String proposedFix;

    private String correctData;

    @NotNull
    @Enumerated(EnumType.STRING)
    private CorrectStatus status;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "creator_id")
    private RegisteredUser creator;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "verifier_id")
    private ContentManager verifier;

    private Instant fixedAt;

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;
}
