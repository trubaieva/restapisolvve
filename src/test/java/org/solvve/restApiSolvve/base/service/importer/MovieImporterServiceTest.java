package org.solvve.restapisolvve.base.service.importer;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.client.themoviedb.TheMovieDbClient;
import org.solvve.restapisolvve.base.client.themoviedb.dto.*;
import org.solvve.restapisolvve.base.domain.*;
import org.solvve.restapisolvve.base.exception.ImportAlreadyPerformedException;
import org.solvve.restapisolvve.base.exception.ImportedEntityAlreadyExistException;
import org.solvve.restapisolvve.base.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.transaction.support.TransactionTemplate;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.junit.Assert.assertEquals;

public class MovieImporterServiceTest extends BaseTest {

    @MockBean
    private TheMovieDbClient movieDbClient;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private ActorRoleRepository actorRoleRepository;

    @Autowired
    private CrewRepository crewRepository;

    @Autowired
    private MovieImporterService movieImporterService;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private ProductionCompanyRepository productionCompanyRepository;

    @Autowired
    private GenreRepository genreRepository;

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status -> {
            runnable.run();
        });
    }

    @Test
    public void testMovieImport() throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {
        String movieExternalId = "id1";

        MovieReadDTO movieReadDTO = generator.generateRandomObject(MovieReadDTO.class);
        Mockito.when(movieDbClient.getMovie(movieExternalId)).thenReturn(movieReadDTO);

        CreditReadDTO creditReadDTO = generator.generateRandomObject(CreditReadDTO.class);
        creditReadDTO.getCast().forEach(x -> x.setName("Test Name"));
        creditReadDTO.getCrew().forEach(x -> x.setName("Test Name"));
        Mockito.when(movieDbClient.getMovieCredits(movieExternalId)).thenReturn(creditReadDTO);

        PersonReadDTO personReadDTO = generator.generateRandomObject(PersonReadDTO.class);
        personReadDTO.setName("Test Name");
        Mockito.when(movieDbClient.getPerson(ArgumentMatchers.any())).thenReturn(personReadDTO);

        UUID movieId = movieImporterService.importMovie(movieExternalId);

        inTransaction(() -> {
            Movie movie = movieRepository.findById(movieId).get();

            assertEquals(movieReadDTO.getTitle(), movie.getName());
            assertEquals(movieReadDTO.getProductionCountries().get(0).getName(), movie.getCountry());
            assertEquals(movieReadDTO.getOriginalLanguage(), movie.getLanguage());
            assertEquals(movieReadDTO.getOverview(), movie.getStoryLine());
            assertEquals(movieReadDTO.getReleaseDate(), movie.getReleaseDate());
            assertEquals(movieReadDTO.getRuntime(), movie.getRuntime());
            assertEquals(movieReadDTO.getBudget(), movie.getBudget());
            assertEquals(movieReadDTO.getRevenue(), movie.getRevenue());
            assertEquals(false, movie.getIsReleased());

            assertThat(movie.getGenres()).extracting("name").containsExactlyElementsOf(
                    movieReadDTO.getGenres().stream().map(GenreReadDTO::getName).collect(Collectors.toList()));

            assertThat(movie.getGenres()).flatExtracting("movies").extracting("id")
                    .contains(movie.getId());

            assertThat(movie.getProductionCompanies()).extracting("name").containsExactlyElementsOf(
                    movieReadDTO.getProductionCompanies().stream().map(ProductionCompanyReadDTO::getName)
                            .collect(Collectors.toList()));

            assertThat(movie.getProductionCompanies()).flatExtracting("movies").extracting("id")
                    .contains(movie.getId());

            List<CastReadDTO> castReadDTOS = creditReadDTO.getCast();
            List<ActorRole> actorRoles = movie.getActorRoles();
            assertEquals(castReadDTOS.size(), actorRoles.size());

            Assertions.assertThat(actorRoles).extracting("actor").extracting("person")
                    .extracting("name").containsExactlyElementsOf(castReadDTOS.stream()
                    .map(CastReadDTO::getName)
                    .collect(Collectors.toList()));

            Assertions.assertThat(actorRoles).extracting("movie").extracting("id")
                    .containsOnly(movie.getId());

            List<CrewReadDTO> crewReadDTOS = creditReadDTO.getCrew();
            List<Crew> crews = movie.getCrews();
            assertEquals(crewReadDTOS.size(), crews.size());

            Assertions.assertThat(crews).extracting("person").extracting("name")
                    .containsExactlyElementsOf(crewReadDTOS.stream().map(CrewReadDTO::getName)
                            .collect(Collectors.toList()));

            Assertions.assertThat(crews).extracting("movie").extracting("id")
                    .containsOnly(movie.getId());
        });
    }

    @Test
    public void testMovieImportWithExistentProductionCompanyAndGenre()
            throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {
        String movieExternalId = "id1";
        String companyName = "Paramount Pictures";
        String genreName = "Comedy";

        ProductionCompany existentCompany = new ProductionCompany();
        existentCompany.setName(companyName);
        productionCompanyRepository.save(existentCompany);

        Genre existentGenre = new Genre();
        existentGenre.setName(genreName);
        genreRepository.save(existentGenre);

        MovieReadDTO movieReadDTO = generator.generateRandomObject(MovieReadDTO.class);

        GenreReadDTO g = new GenreReadDTO();
        g.setName(genreName);
        movieReadDTO.getGenres().add(g);

        ProductionCompanyReadDTO p = new ProductionCompanyReadDTO();
        p.setName(companyName);
        movieReadDTO.getProductionCompanies().add(p);

        Mockito.when(movieDbClient.getMovie(movieExternalId)).thenReturn(movieReadDTO);

        CreditReadDTO creditReadDTO = generator.generateRandomObject(CreditReadDTO.class);
        Mockito.when(movieDbClient.getMovieCredits(movieExternalId)).thenReturn(creditReadDTO);

        PersonReadDTO personReadDTO = generator.generateRandomObject(PersonReadDTO.class);
        Mockito.when(movieDbClient.getPerson(ArgumentMatchers.any())).thenReturn(personReadDTO);

        UUID movieId = movieImporterService.importMovie(movieExternalId);

        inTransaction(() -> {
            Movie movie = movieRepository.findById(movieId).get();

            List<Genre> genres = movie.getGenres();
            assertThat(genres).extracting("name").containsExactlyElementsOf(
                    movieReadDTO.getGenres().stream().map(GenreReadDTO::getName).collect(Collectors.toList()));
            assertThat(genres).extracting("id", "name").contains(
                    tuple(existentGenre.getId(), genreName));
            assertThat(genres).flatExtracting("movies").extracting("id")
                    .contains(movie.getId());

            List<ProductionCompany> productionCompanies = movie.getProductionCompanies();
            assertThat(productionCompanies).extracting("name").containsExactlyElementsOf(
                    movieReadDTO.getProductionCompanies().stream().map(ProductionCompanyReadDTO::getName)
                            .collect(Collectors.toList()));
            assertThat(productionCompanies).extracting("id", "name").contains(
                    tuple(existentCompany.getId(), companyName));
            assertThat(productionCompanies).flatExtracting("movies").extracting("id")
                    .contains(movie.getId());
        });
    }

    @Test
    public void testImportMovieWhenMovieAlreadyExist() {
        String movieExternalId = "id1";

        Movie existingMovie = flatGenerator.generateRandomObject(Movie.class);
        existingMovie = movieRepository.save(existingMovie);

        MovieReadDTO read = generator.generateRandomObject(MovieReadDTO.class);
        read.setTitle(existingMovie.getName());
        read.setReleaseDate(existingMovie.getReleaseDate());

        Mockito.when(movieDbClient.getMovie(movieExternalId)).thenReturn(read);

        ImportedEntityAlreadyExistException ex = Assertions
                .catchThrowableOfType(() -> movieImporterService.importMovie(movieExternalId),
                        ImportedEntityAlreadyExistException.class);
        Assert.assertEquals(Movie.class, ex.getEntityClass());
        Assert.assertEquals(existingMovie.getId(), ex.getEntityId());
    }

    @Test
    public void testNoExceptionWhenImportMovieWithDifferentReleaseYear()
            throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {
        String movieExternalId = "id1";

        Movie existingMovie = flatGenerator.generateRandomObject(Movie.class);
        movieRepository.save(existingMovie);

        MovieReadDTO read = generator.generateRandomObject(MovieReadDTO.class);
        read.setTitle(existingMovie.getName());
        read.setReleaseDate(existingMovie.getReleaseDate().minusYears(2));
        Mockito.when(movieDbClient.getMovie(movieExternalId)).thenReturn(read);

        CreditReadDTO creditReadDTO = generator.generateRandomObject(CreditReadDTO.class);
        creditReadDTO.getCast().forEach(x -> x.setName("Test Name"));
        creditReadDTO.getCrew().forEach(x -> x.setName("Test Name"));
        Mockito.when(movieDbClient.getMovieCredits(movieExternalId)).thenReturn(creditReadDTO);

        PersonReadDTO personReadDTO = generator.generateRandomObject(PersonReadDTO.class);
        personReadDTO.setName("Test Name");
        Mockito.when(movieDbClient.getPerson(ArgumentMatchers.any())).thenReturn(personReadDTO);

        UUID movieId = movieImporterService.importMovie(movieExternalId);
        inTransaction(() -> {
            Movie movie = movieRepository.findById(movieId).get();
            Assert.assertNotEquals(existingMovie.getId(), movieId);
            assertEquals(read.getTitle(), movie.getName());
            assertEquals(2, existingMovie.getReleaseDate().getYear() - movie.getReleaseDate().getYear());
            assertEquals(read.getReleaseDate().getYear(), movie.getReleaseDate().getYear());
        });
    }

    @Test
    public void testImportMovieWhenPersonAlreadyExist()
            throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {
        String movieExternalId = "id1";
        String personName = "Anna";
        LocalDate birthDay = LocalDate.of(1992, 10, 16);

        Person existingPerson = flatGenerator.generateRandomObject(Person.class);
        existingPerson.setName(personName);
        existingPerson.setBirthDate(birthDay);
        existingPerson = personRepository.save(existingPerson);
        UUID existingPersonId = existingPerson.getId();

        MovieReadDTO read = generator.generateRandomObject(MovieReadDTO.class);
        Mockito.when(movieDbClient.getMovie(movieExternalId)).thenReturn(read);

        CreditReadDTO creditReadDTO = generator.generateRandomObject(CreditReadDTO.class);
        Mockito.when(movieDbClient.getMovieCredits(movieExternalId)).thenReturn(creditReadDTO);

        PersonReadDTO personReadDTO = generator.generateRandomObject(PersonReadDTO.class);
        personReadDTO.setName(personName);
        personReadDTO.setBirthday(birthDay);
        Mockito.when(movieDbClient.getPerson(ArgumentMatchers.anyString())).thenReturn(personReadDTO);

        UUID movieId = movieImporterService.importMovie(movieExternalId);
        inTransaction(() -> {
            Movie movie = movieRepository.findById(movieId).get();

            Assertions.assertThat(movie.getActorRoles()).extracting("actor")
                    .extracting("person").extracting("id").contains(existingPersonId);
            Assertions.assertThat(movie.getCrews()).extracting("person").extracting("id")
                    .contains(existingPersonId);
        });
    }

    @Test
    public void testNoCallToClientOnDuplicateImportMovie()
            throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {
        String movieExternalId = "id1";

        MovieReadDTO read = generator.generateRandomObject(MovieReadDTO.class);
        Mockito.when(movieDbClient.getMovie(movieExternalId)).thenReturn(read);

        CreditReadDTO creditReadDTO = generator.generateRandomObject(CreditReadDTO.class);
        Mockito.when(movieDbClient.getMovieCredits(movieExternalId)).thenReturn(creditReadDTO);

        PersonReadDTO personReadDTO = generator.generateRandomObject(PersonReadDTO.class);
        Mockito.when(movieDbClient.getPerson(ArgumentMatchers.any())).thenReturn(personReadDTO);

        movieImporterService.importMovie(movieExternalId);
        Mockito.verify(movieDbClient).getMovie(movieExternalId);
        Mockito.verify(movieDbClient).getMovieCredits(movieExternalId);
        Mockito.verify(movieDbClient, Mockito.atLeastOnce()).getPerson(ArgumentMatchers.anyString());
        Mockito.reset(movieDbClient);

        Assertions.assertThatThrownBy(() -> movieImporterService.importMovie(movieExternalId))
                .isInstanceOf(ImportAlreadyPerformedException.class);

        Mockito.verifyNoInteractions(movieDbClient);
    }

    @Test
    public void testNoCallToClientForImportPersonOnDuplicatePerson()
            throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {
        String movieExternalId = "id1";
        String movieExternalId2 = "id2";
        String personExternalId = "id3";

        MovieReadDTO read = generator.generateRandomObject(MovieReadDTO.class);
        Mockito.when(movieDbClient.getMovie(movieExternalId)).thenReturn(read);

        CreditReadDTO creditReadDTO = generator.generateRandomObject(CreditReadDTO.class);
        creditReadDTO.getCast().forEach(x -> x.setId(personExternalId));
        creditReadDTO.getCrew().forEach(x -> x.setId(personExternalId));
        Mockito.when(movieDbClient.getMovieCredits(movieExternalId)).thenReturn(creditReadDTO);

        PersonReadDTO personReadDTO = generator.generateRandomObject(PersonReadDTO.class);
        Mockito.when(movieDbClient.getPerson(personExternalId)).thenReturn(personReadDTO);

        movieImporterService.importMovie(movieExternalId);
        Mockito.verify(movieDbClient).getMovie(movieExternalId);
        Mockito.verify(movieDbClient).getMovieCredits(movieExternalId);
        Mockito.verify(movieDbClient, Mockito.atLeastOnce()).getPerson(personExternalId);
        Mockito.reset(movieDbClient);

        read = generator.generateRandomObject(MovieReadDTO.class);
        Mockito.when(movieDbClient.getMovie(movieExternalId2)).thenReturn(read);

        creditReadDTO = generator.generateRandomObject(CreditReadDTO.class);
        creditReadDTO.getCast().forEach(x -> x.setId(personExternalId));
        creditReadDTO.getCrew().forEach(x -> x.setId(personExternalId));
        Mockito.when(movieDbClient.getMovieCredits(movieExternalId2)).thenReturn(creditReadDTO);

        Mockito.when(movieDbClient.getPerson(personExternalId)).thenReturn(personReadDTO);

        movieImporterService.importMovie(movieExternalId2);

        Mockito.verify(movieDbClient).getMovie(movieExternalId2);
        Mockito.verify(movieDbClient).getMovieCredits(movieExternalId2);
        Mockito.verify(movieDbClient, Mockito.never()).getPerson(personExternalId);
    }
}
