package org.solvve.restapisolvve.base.dto.moviereview;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class MovieReviewReadDTO {
    private UUID id;
    private String textReview;
    private Boolean isVisible;
    private Integer likeCount;
    private UUID userId;
    private UUID movieId;
    private Instant createdAt;
    private Instant updatedAt;
}
