package org.solvve.restapisolvve.base.job.oneoff;

import lombok.extern.slf4j.Slf4j;
import org.solvve.restapisolvve.base.client.themoviedb.TheMovieDbClient;
import org.solvve.restapisolvve.base.client.themoviedb.dto.MovieReadShortDTO;
import org.solvve.restapisolvve.base.exception.ImportAlreadyPerformedException;
import org.solvve.restapisolvve.base.exception.ImportedEntityAlreadyExistException;
import org.solvve.restapisolvve.base.service.AsyncService;
import org.solvve.restapisolvve.base.service.importer.MovieImporterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

@Slf4j
@Component
public class TheMovieDbImportOneOffJob {

    @Autowired
    private TheMovieDbClient client;

    @Autowired
    private MovieImporterService movieImporterService;

    @Autowired
    private AsyncService asyncService;

    @Value("${themoviedb.import.job.enabled}")
    private boolean enabled;

    @PostConstruct
    void executeJob() {
        if (!enabled) {
            log.info("Import is disabled");
            return;
        }

        asyncService.executeAsync(this::doImport);
    }

    public void doImport() {
        log.info("Starting import");

        try {
            List<MovieReadShortDTO> moviesToImport = client.getTopRatedMovies().getResults();
            int successfullyImported = 0;
            int skipped = 0;
            int failed = 0;

            for (MovieReadShortDTO m : client.getTopRatedMovies().getResults()) {
                try {
                    movieImporterService.importMovie(m.getId());
                    successfullyImported++;
                } catch (ImportedEntityAlreadyExistException | ImportAlreadyPerformedException e) {
                    log.info("Can't import movie id={}, title={}: {}, it will be skipped", m.getId(), m.getTitle(),
                            e.getMessage());
                    skipped++;
                } catch (Exception e) {
                    log.warn("Failed to import movie id={}, title={}", m.getId(), m.getTitle(), e);
                    failed++;
                }
            }
            log.info("Total movies to import: {}, successfully imported: {}, skipped: {}, failed: {}",
                    moviesToImport.size(), successfullyImported, skipped, failed);
        } catch (Exception e) {
            log.warn("Failed to perform import", e);
        }

        log.info("Import finished");
    }
}
