package org.solvve.restapisolvve.base.service;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.*;
import org.solvve.restapisolvve.base.dto.requesttocorrect.RequestToCorrectCreateDTO;
import org.solvve.restapisolvve.base.dto.requesttocorrect.RequestToCorrectFilter;
import org.solvve.restapisolvve.base.dto.requesttocorrect.RequestToCorrectFixDTO;
import org.solvve.restapisolvve.base.dto.requesttocorrect.RequestToCorrectReadDTO;
import org.solvve.restapisolvve.base.exception.EntityNotFoundException;
import org.solvve.restapisolvve.base.exception.UnexpectedRequestStatusException;
import org.solvve.restapisolvve.base.repository.MovieRepository;
import org.solvve.restapisolvve.base.repository.NewsRepository;
import org.solvve.restapisolvve.base.repository.PersonRepository;
import org.solvve.restapisolvve.base.repository.RequestToCorrectRepository;
import org.solvve.restapisolvve.base.util.DtoCreator;
import org.solvve.restapisolvve.base.util.EntityCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.solvve.restapisolvve.base.domain.CorrectStatus.*;

public class RequestToCorrectServiceTest extends BaseTest {

    @Autowired
    private RequestToCorrectRepository requestToCorrectRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private RequestToCorrectService requestToCorrectService;

    @Autowired
    private EntityCreator entityCreator;

    @Autowired
    private DtoCreator dtoCreator;

    @Autowired
    private NewsRepository newsRepository;

    @Test
    public void testGetRequestToCorrect() {
        RequestToCorrect requestToCorrect = entityCreator.createRequestToCorrect();

        RequestToCorrectReadDTO readDTO = requestToCorrectService.getRequestToCorrect(requestToCorrect.getId());

        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(requestToCorrect, "creatorId", "verifierId");
        Assert.assertEquals(readDTO.getCreatorId(), requestToCorrect.getCreator().getId());
        Assert.assertEquals(readDTO.getVerifierId(), requestToCorrect.getVerifier().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetRequestToCorrectWrongId() {
        requestToCorrectService.getRequestToCorrect(UUID.randomUUID());
    }

    @Test
    public void testGetRequestsToCorrect() {
        List<RequestToCorrect> requests = createRequestsForFilter();

        RequestToCorrectFilter filter = new RequestToCorrectFilter();
        filter.setMovieName("Cinderella");

        Assertions.assertThat(requestToCorrectService.getRequestsToCorrect(filter, Pageable.unpaged()).getData())
                .extracting("id")
                .containsExactlyInAnyOrderElementsOf(List.of(requests.get(0), requests.get(1), requests.get(5))
                        .stream().map(RequestToCorrect::getId)
                        .collect(Collectors.toList()));
    }

    @Test
    public void testGetRequestsToCorrectWithEmptyFilterPagingAndSorting() {
        List<RequestToCorrect> requests = createRequestsForFilter();

        RequestToCorrectFilter filter = new RequestToCorrectFilter();

        PageRequest pageRequest1 = PageRequest.of(0, 1, Sort.by(Sort.Direction.ASC, "objectType"));
        Assertions.assertThat(requestToCorrectService.getRequestsToCorrect(filter, pageRequest1).getData())
                .extracting("id")
                .isEqualTo(Arrays.asList(requests.get(0).getId()));

        PageRequest pageRequest2 = PageRequest.of(2, 2, Sort.by(Sort.Direction.ASC, "fixedAt"));
        Assertions.assertThat(requestToCorrectService.getRequestsToCorrect(filter, pageRequest2).getData())
                .extracting("id")
                .isEqualTo(Arrays.asList(requests.get(0).getId(), requests.get(5).getId()));
    }

    @Test
    public void testCreateRequestToCorrect() {
        RequestToCorrectCreateDTO create = dtoCreator.createRequestToCorrectCreateDTO();

        RegisteredUser creator = entityCreator.createRegisteredUser();

        RequestToCorrectReadDTO read = requestToCorrectService.createRequestToCorrect(creator.getId(), create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        RequestToCorrect requestToCorrect = requestToCorrectRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(requestToCorrect, "creatorId",
                "verifierId");
        Assert.assertEquals(read.getCreatorId(), requestToCorrect.getCreator().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testCreateRequestToCorrectWithWrongCreator() {
        RequestToCorrectCreateDTO create = dtoCreator.createRequestToCorrectCreateDTO();
        requestToCorrectService.createRequestToCorrect(UUID.randomUUID(), create);
    }

    @Test
    public void testFixRequestToCorrectNews() {
        RequestToCorrect requestToCorrect = entityCreator.createRequestToCorrectNewsPerson();

        News news = newsRepository.findById(requestToCorrect.getObjectId()).get();
        news.setText("Here is wrong data. I hope, a wrong data is correct.");
        newsRepository.save(news);

        RequestToCorrectFixDTO fixDTO = createRequestToCorrectFix("valid fix");

        Assertions.assertThat(news.getText()).contains(requestToCorrect.getWrongData()).doesNotContain(fixDTO.getFix());

        RequestToCorrectReadDTO readDTO =
                requestToCorrectService.fixRequestToCorrect(requestToCorrect.getVerifier().getId(),
                        requestToCorrect.getId(), fixDTO);

        news = newsRepository.findById(requestToCorrect.getObjectId()).get();
        String dataAfterFix = news.getText();

        Assertions.assertThat(dataAfterFix).isEqualTo("Here is valid fix. I hope, a valid fix is correct.");

        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(requestToCorrect, "creatorId", "verifierId",
                "status", "updatedAt", "fixedAt", "correctData");
        Assert.assertEquals(readDTO.getCreatorId(), requestToCorrect.getCreator().getId());
        Assert.assertEquals(readDTO.getVerifierId(), requestToCorrect.getVerifier().getId());
        Assert.assertEquals(readDTO.getCorrectData(), fixDTO.getFix());
        Assert.assertTrue(readDTO.getStatus() == FIXED);
        Assertions.assertThat(readDTO.getFixedAt()).isNotNull().isBeforeOrEqualTo(readDTO.getUpdatedAt());
    }

    @Test
    public void testFixRequestToCorrectMovie() {
        RequestToCorrect requestToCorrect = entityCreator.createRequestToCorrectMovie();
        requestToCorrect.setStatus(NEED_TO_VERIFY);
        requestToCorrect.setWrongData("wrong data");
        requestToCorrectRepository.save(requestToCorrect);

        Movie movie = movieRepository.findById(requestToCorrect.getObjectId()).get();
        movie.setStoryLine("Here is wrong data. I hope, a wrong data is correct.");
        movieRepository.save(movie);

        RequestToCorrectFixDTO fixDTO = createRequestToCorrectFix("valid fix");

        Assertions.assertThat(movie.getStoryLine()).contains(requestToCorrect.getWrongData())
                .doesNotContain(fixDTO.getFix());

        RequestToCorrectReadDTO readDTO =
                requestToCorrectService.fixRequestToCorrect(requestToCorrect.getVerifier().getId(),
                        requestToCorrect.getId(), fixDTO);

        movie = movieRepository.findById(requestToCorrect.getObjectId()).get();
        String dataAfterFix = movie.getStoryLine();

        Assertions.assertThat(dataAfterFix).isEqualTo("Here is valid fix. I hope, a valid fix is correct.");

        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(requestToCorrect, "creatorId", "verifierId",
                "status", "updatedAt", "fixedAt", "correctData");
        Assert.assertEquals(readDTO.getCreatorId(), requestToCorrect.getCreator().getId());
        Assert.assertEquals(readDTO.getVerifierId(), requestToCorrect.getVerifier().getId());
        Assert.assertEquals(readDTO.getCorrectData(), fixDTO.getFix());
        Assert.assertTrue(readDTO.getStatus() == FIXED);
        Assertions.assertThat(readDTO.getFixedAt()).isNotNull().isBeforeOrEqualTo(readDTO.getUpdatedAt());
    }

    @Test
    public void testFixRequestToCorrectPerson() {
        RequestToCorrect requestToCorrect = entityCreator.createRequestToCorrectPerson();
        requestToCorrect.setStatus(NEED_TO_VERIFY);
        requestToCorrect.setWrongData("wrong data");
        requestToCorrectRepository.save(requestToCorrect);

        Person person = personRepository.findById(requestToCorrect.getObjectId()).get();
        person.setBiography("Here is wrong data. I hope, a wrong data is correct.");
        personRepository.save(person);

        RequestToCorrectFixDTO fixDTO = createRequestToCorrectFix("valid fix");

        Assertions.assertThat(person.getBiography()).contains(requestToCorrect.getWrongData())
                .doesNotContain(fixDTO.getFix());

        RequestToCorrectReadDTO readDTO =
                requestToCorrectService.fixRequestToCorrect(requestToCorrect.getVerifier().getId(),
                        requestToCorrect.getId(), fixDTO);

        person = personRepository.findById(requestToCorrect.getObjectId()).get();
        String dataAfterFix = person.getBiography();

        Assertions.assertThat(dataAfterFix).isEqualTo("Here is valid fix. I hope, a valid fix is correct.");

        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(requestToCorrect, "creatorId", "verifierId",
                "status", "updatedAt", "fixedAt", "correctData");
        Assert.assertEquals(readDTO.getCreatorId(), requestToCorrect.getCreator().getId());
        Assert.assertEquals(readDTO.getVerifierId(), requestToCorrect.getVerifier().getId());
        Assert.assertEquals(readDTO.getCorrectData(), fixDTO.getFix());
        Assert.assertTrue(readDTO.getStatus() == FIXED);
        Assertions.assertThat(readDTO.getFixedAt()).isNotNull().isBeforeOrEqualTo(readDTO.getUpdatedAt());
    }

    @Test
    public void testFixRequestToCorrectWhenWrongDataNotFoundInNews() {
        RequestToCorrect requestToCorrect = entityCreator.createRequestToCorrectNewsPerson();

        News news = newsRepository.findById(requestToCorrect.getObjectId()).get();
        news.setText("Here is no wrong text. All data is correct.");
        newsRepository.save(news);

        String dataBeforeFix = news.getText();

        RequestToCorrectFixDTO fixDTO = createRequestToCorrectFix("valid fix");

        Assertions.assertThat(dataBeforeFix).doesNotContain(requestToCorrect.getWrongData());

        RequestToCorrectReadDTO readDTO =
                requestToCorrectService.fixRequestToCorrect(requestToCorrect.getVerifier().getId(),
                        requestToCorrect.getId(), fixDTO);

        news = newsRepository.findById(requestToCorrect.getObjectId()).get();
        String dataAfterCancel = news.getText();

        Assert.assertEquals(dataBeforeFix, dataAfterCancel);

        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(requestToCorrect, "creatorId", "verifierId",
                "status", "updatedAt");
        Assert.assertEquals(readDTO.getCreatorId(), requestToCorrect.getCreator().getId());
        Assert.assertEquals(readDTO.getVerifierId(), requestToCorrect.getVerifier().getId());
        Assert.assertTrue(readDTO.getStatus() == CANCEL);
        Assertions.assertThat(readDTO.getFixedAt()).isNull();
    }

    @Test
    public void testFixRequestToCorrectWhenWrongDataNotFoundMovie() {
        RequestToCorrect requestToCorrect = entityCreator.createRequestToCorrectMovie();
        requestToCorrect.setStatus(NEED_TO_VERIFY);
        requestToCorrect.setWrongData("data not found");
        requestToCorrect.setFixedAt(null);
        requestToCorrectRepository.save(requestToCorrect);

        Movie movie = movieRepository.findById(requestToCorrect.getObjectId()).get();
        movie.setStoryLine("Here is no wrong text. All data is correct.");
        movieRepository.save(movie);

        String dataBeforeFix = movie.getStoryLine();

        RequestToCorrectFixDTO fixDTO = createRequestToCorrectFix("valid fix");

        Assertions.assertThat(dataBeforeFix).doesNotContain(requestToCorrect.getWrongData());

        RequestToCorrectReadDTO readDTO =
                requestToCorrectService.fixRequestToCorrect(requestToCorrect.getVerifier().getId(),
                        requestToCorrect.getId(), fixDTO);

        movie = movieRepository.findById(requestToCorrect.getObjectId()).get();
        String dataAfterCancel = movie.getStoryLine();

        Assert.assertEquals(dataBeforeFix, dataAfterCancel);

        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(requestToCorrect, "creatorId", "verifierId",
                "status", "updatedAt");
        Assert.assertEquals(readDTO.getCreatorId(), requestToCorrect.getCreator().getId());
        Assert.assertEquals(readDTO.getVerifierId(), requestToCorrect.getVerifier().getId());
        Assert.assertTrue(readDTO.getStatus() == CANCEL);
        Assertions.assertThat(readDTO.getFixedAt()).isNull();
    }

    @Test
    public void testFixRequestToCorrectWhenWrongDataNotFoundPerson() {
        RequestToCorrect requestToCorrect = entityCreator.createRequestToCorrectPerson();
        requestToCorrect.setStatus(NEED_TO_VERIFY);
        requestToCorrect.setWrongData("data not found");
        requestToCorrectRepository.save(requestToCorrect);

        Person person = personRepository.findById(requestToCorrect.getObjectId()).get();
        person.setBiography("Here is no wrong text. All data is correct.");
        personRepository.save(person);

        String dataBeforeFix = person.getBiography();

        RequestToCorrectFixDTO fixDTO = createRequestToCorrectFix("valid fix");

        Assertions.assertThat(dataBeforeFix).doesNotContain(requestToCorrect.getWrongData());

        RequestToCorrectReadDTO readDTO =
                requestToCorrectService.fixRequestToCorrect(requestToCorrect.getVerifier().getId(),
                        requestToCorrect.getId(), fixDTO);

        person = personRepository.findById(requestToCorrect.getObjectId()).get();
        String dataAfterCancel = person.getBiography();

        Assert.assertEquals(dataBeforeFix, dataAfterCancel);

        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(requestToCorrect, "creatorId", "verifierId",
                "status", "updatedAt");
        Assert.assertEquals(readDTO.getCreatorId(), requestToCorrect.getCreator().getId());
        Assert.assertEquals(readDTO.getVerifierId(), requestToCorrect.getVerifier().getId());
        Assert.assertTrue(readDTO.getStatus() == CANCEL);
        Assertions.assertThat(readDTO.getFixedAt()).isNull();
    }

    @Test(expected = EntityNotFoundException.class)
    public void testFixRequestToCorrectWithNonRelatedContentManager() {
        RequestToCorrect requestToCorrect = entityCreator.createRequestToCorrect();
        ContentManager verifier = entityCreator.createContentManager();
        requestToCorrectService
                .fixRequestToCorrect(verifier.getId(), requestToCorrect.getId(), new RequestToCorrectFixDTO());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testFixNonExistentRequestToCorrect() {
        requestToCorrectService.fixRequestToCorrect(UUID.randomUUID(), UUID.randomUUID(), new RequestToCorrectFixDTO());
    }

    @Test(expected = UnexpectedRequestStatusException.class)
    public void testFixRequestToCorrectWithWrongStatus() {
        RequestToCorrect requestToCorrect = entityCreator.createRequestToCorrect();
        requestToCorrect.setStatus(FIXED);
        requestToCorrectRepository.save(requestToCorrect);
        requestToCorrectService.fixRequestToCorrect(requestToCorrect.getVerifier().getId(), requestToCorrect.getId(),
                new RequestToCorrectFixDTO());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testFixRequestToCorrectWithNonExistentObjectId() {
        RequestToCorrect requestToCorrect = entityCreator.createRequestToCorrectNewsPerson();
        requestToCorrect.setObjectId(UUID.randomUUID());
        requestToCorrectRepository.save(requestToCorrect);
        requestToCorrectService.fixRequestToCorrect(requestToCorrect.getVerifier().getId(), requestToCorrect.getId(),
                new RequestToCorrectFixDTO());
    }

    @Test
    public void testCancelRequestToCorrect() {
        RequestToCorrect requestToCorrect = entityCreator.createRequestToCorrectNewsPerson();

        News news = newsRepository.findById(requestToCorrect.getObjectId()).get();
        String dataBeforeCancel = news.getText();

        RequestToCorrectReadDTO readDTO =
                requestToCorrectService.cancelRequestToCorrect(requestToCorrect.getVerifier().getId(),
                        requestToCorrect.getId());

        news = newsRepository.findById(requestToCorrect.getObjectId()).get();
        String dataAfterCancel = news.getText();

        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(requestToCorrect, "creatorId", "verifierId",
                "status", "updatedAt");
        Assert.assertEquals(readDTO.getCreatorId(), requestToCorrect.getCreator().getId());
        Assert.assertEquals(readDTO.getVerifierId(), requestToCorrect.getVerifier().getId());
        Assert.assertTrue(readDTO.getStatus() == CANCEL);

        Assert.assertEquals(dataBeforeCancel, dataAfterCancel);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testCancelRequestToCorrectWithNonRelatedContentManager() {
        RequestToCorrect requestToCorrect = entityCreator.createRequestToCorrect();
        ContentManager verifier = entityCreator.createContentManager();

        requestToCorrectService.cancelRequestToCorrect(verifier.getId(), requestToCorrect.getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testCancelNonExistentRequestToCorrect() {
        requestToCorrectService.cancelRequestToCorrect(UUID.randomUUID(), UUID.randomUUID());
    }

    @Test(expected = UnexpectedRequestStatusException.class)
    public void testCancelRequestToCorrectWithWrongStatus() {
        RequestToCorrect requestToCorrect = entityCreator.createRequestToCorrect();
        requestToCorrect.setStatus(FIXED);
        requestToCorrectRepository.save(requestToCorrect);
        requestToCorrectService
                .cancelRequestToCorrect(requestToCorrect.getVerifier().getId(), requestToCorrect.getId());
    }

    private RequestToCorrectFixDTO createRequestToCorrectFix(String fix) {
        RequestToCorrectFixDTO fixDTO = new RequestToCorrectFixDTO();
        fixDTO.setFix(fix);
        return fixDTO;
    }

    private List<RequestToCorrect> createRequestsForFilter() {
        List<RequestToCorrect> requests = List.of(
                entityCreator.createRequestToCorrectMovie(),
                entityCreator.createRequestToCorrectNewsMovie(),
                entityCreator.createRequestToCorrectPerson(),
                entityCreator.createRequestToCorrectNewsPerson(),
                entityCreator.createRequestToCorrectNews(),
                entityCreator.createRequestToCorrectNewsMoviePerson());
        return requests;
    }
}
