package org.solvve.restapisolvve.base.dto.requesttocorrect;

import lombok.Data;
import org.solvve.restapisolvve.base.domain.ObjectToCorrect;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class RequestToCorrectCreateDTO {

    @NotNull
    private ObjectToCorrect objectType;

    @NotNull
    private UUID objectId;
    private String wrongData;
    private String proposedFix;
}