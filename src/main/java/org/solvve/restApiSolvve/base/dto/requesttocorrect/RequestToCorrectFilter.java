package org.solvve.restapisolvve.base.dto.requesttocorrect;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.solvve.restapisolvve.base.domain.CorrectStatus;
import org.solvve.restapisolvve.base.domain.ObjectToCorrect;

import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestToCorrectFilter {
    private String movieName;
    private String personName;
    private Set<ObjectToCorrect> objectTypes = new HashSet<ObjectToCorrect>();
    private CorrectStatus status;
    private String wrongData;
}