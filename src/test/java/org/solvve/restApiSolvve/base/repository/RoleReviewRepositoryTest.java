package org.solvve.restapisolvve.base.repository;

import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.RoleReview;
import org.solvve.restapisolvve.base.util.EntityCreator;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;

import static org.junit.Assert.*;

public class RoleReviewRepositoryTest extends BaseTest {

    @Autowired
    private RoleReviewRepository roleReviewRepository;

    @Autowired
    private EntityCreator entityCreator;

    @Test
    public void testCreatedAtIsSet() {
        RoleReview roleReview = entityCreator.createRoleReview();

        Instant createdAtBeforeReload = roleReview.getCreatedAt();
        assertNotNull(createdAtBeforeReload);
        roleReview = roleReviewRepository.findById(roleReview.getId()).get();

        Instant createdAtAfterReload = roleReview.getCreatedAt();
        assertNotNull(createdAtAfterReload);
        assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        RoleReview roleReview = entityCreator.createRoleReview();

        Instant updatedAtBeforeReload = roleReview.getUpdatedAt();
        assertNotNull(updatedAtBeforeReload);
        roleReview = roleReviewRepository.findById(roleReview.getId()).get();

        Instant updatedAtAfterReload = roleReview.getUpdatedAt();
        assertNotNull(updatedAtAfterReload);
        assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        roleReview.setTextReview("text");
        roleReviewRepository.save(roleReview);
        roleReview = roleReviewRepository.findById(roleReview.getId()).get();
        Instant updatedAtAfterModificationAndReload = roleReview.getUpdatedAt();
        assertTrue(updatedAtAfterModificationAndReload.isAfter(updatedAtAfterReload));
    }

}
