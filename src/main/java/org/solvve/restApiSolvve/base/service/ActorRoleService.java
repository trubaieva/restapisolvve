package org.solvve.restapisolvve.base.service;

import lombok.extern.slf4j.Slf4j;
import org.solvve.restapisolvve.base.domain.ActorRole;
import org.solvve.restapisolvve.base.domain.Movie;
import org.solvve.restapisolvve.base.dto.actorrole.ActorRoleCreateDTO;
import org.solvve.restapisolvve.base.dto.actorrole.ActorRolePatchDTO;
import org.solvve.restapisolvve.base.dto.actorrole.ActorRolePutDTO;
import org.solvve.restapisolvve.base.dto.actorrole.ActorRoleReadDTO;
import org.solvve.restapisolvve.base.exception.EntityNotFoundException;
import org.solvve.restapisolvve.base.repository.ActorRoleRepository;
import org.solvve.restapisolvve.base.repository.RepositoryHelper;
import org.solvve.restapisolvve.base.repository.RoleRateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ActorRoleService {

    @Autowired
    RepositoryHelper repositoryHelper;

    @Autowired
    private ActorRoleRepository actorRoleRepository;

    @Autowired
    private RoleRateRepository roleRateRepository;

    @Autowired
    private TranslationService translationService;

    public ActorRoleReadDTO getMovieActorRole(UUID movieId, UUID id) {
        ActorRole actorRole = getMovieActorRoleRequired(movieId, id);
        return translationService.translate(actorRole, ActorRoleReadDTO.class);
    }

    public List<ActorRoleReadDTO> getMovieActorRoles(UUID movieId) {
        List<ActorRole> actorRoles = actorRoleRepository.findAllByMovieId(movieId);
        return actorRoles.stream().map(srcObject ->
                translationService.translate(srcObject, ActorRoleReadDTO.class)).collect(
                Collectors.toList());
    }

    public ActorRoleReadDTO createMovieActorRole(UUID movieId, ActorRoleCreateDTO create) {
        ActorRole actorRole = translationService.translate(create, ActorRole.class);
        actorRole.setMovie(repositoryHelper.getReferenceIfExist(Movie.class, movieId));
        actorRole = actorRoleRepository.save(actorRole);

        return translationService.translate(actorRole, ActorRoleReadDTO.class);
    }

    public ActorRoleReadDTO patchMovieActorRole(UUID movieId, UUID id, ActorRolePatchDTO patchDTO) {
        ActorRole actorRole = getMovieActorRoleRequired(movieId, id);

        translationService.map(patchDTO, actorRole);

        actorRole = actorRoleRepository.save(actorRole);
        return translationService.translate(actorRole, ActorRoleReadDTO.class);
    }

    public ActorRoleReadDTO updateMovieActorRole(UUID movieId, UUID id, ActorRolePutDTO putDTO) {
        ActorRole actorRole = getMovieActorRoleRequired(movieId, id);

        translationService.map(putDTO, actorRole);

        actorRole = actorRoleRepository.save(actorRole);
        return translationService.translate(actorRole, ActorRoleReadDTO.class);
    }

    public void deleteMovieActorRole(UUID movieId, UUID id) {
        ActorRole actorRole = actorRoleRepository.findByIdAndMovieId(id, movieId);
        if (actorRole == null) {
            throw new EntityNotFoundException(ActorRole.class, id);
        }
        actorRoleRepository.delete(actorRole);
    }

    private ActorRole getMovieActorRoleRequired(UUID movieId, UUID id) {
        ActorRole actorRole = actorRoleRepository.findByIdAndMovieId(id, movieId);
        if (actorRole == null) {
            throw new EntityNotFoundException(ActorRole.class, id);
        }
        return actorRole;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateAverageRating(UUID roleId) {
        Double averageRate = roleRateRepository.calcAverageRateOfRole(roleId);
        ActorRole role = repositoryHelper.getEntityIfExist(ActorRole.class, roleId);
        log.info("Setting new avarage mark of movie: {}. Old value: {}, new value: {}", roleId, role.getAvgRating(),
                averageRate);
        role.setAvgRating(averageRate);
        actorRoleRepository.save(role);
    }
}
