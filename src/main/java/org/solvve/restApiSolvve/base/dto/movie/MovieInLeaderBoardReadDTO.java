package org.solvve.restapisolvve.base.dto.movie;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class MovieInLeaderBoardReadDTO {
    private UUID id;
    private String name;
    private Double avgRating;
    private Long ratesCount;
}
