package org.solvve.restapisolvve.base.repository;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.solvve.restapisolvve.base.BaseTest;
import org.solvve.restapisolvve.base.domain.RequestToCorrect;
import org.solvve.restapisolvve.base.util.EntityCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import java.time.Instant;
import java.util.List;

import static org.junit.Assert.*;

public class RequestToCorrectRepositoryTest extends BaseTest {

    @Autowired
    private RequestToCorrectRepository requestToCorrectRepository;

    @Autowired
    private EntityCreator entityCreator;

    @Test(expected = TransactionSystemException.class)
    public void testSaveActorRoleValidation() {
        RequestToCorrect request = new RequestToCorrect();
        requestToCorrectRepository.save(request);
    }

    @Test
    public void testFindByIdAndVerifierId() {
        List<RequestToCorrect> requests = List.of(entityCreator.createRequestToCorrectMovie(),
                entityCreator.createRequestToCorrectNewsMovie());
        RequestToCorrect expected = requests.get(0);

        RequestToCorrect actual = requestToCorrectRepository.findByIdAndVerifierId(expected.getId(),
                expected.getVerifier().getId());

        Assertions.assertThat(actual.getId()).isEqualTo(expected.getId());
        Assertions.assertThat(actual.getVerifier().getId()).isEqualTo(expected.getVerifier().getId());
    }

    @Test
    public void testCreatedAtIsSet() {
        RequestToCorrect requestToCorrect = entityCreator.createRequestToCorrect();

        Instant createdAtBeforeReload = requestToCorrect.getCreatedAt();
        assertNotNull(createdAtBeforeReload);
        requestToCorrect = requestToCorrectRepository.findById(requestToCorrect.getId()).get();

        Instant createdAtAfterReload = requestToCorrect.getCreatedAt();
        assertNotNull(createdAtAfterReload);
        assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        RequestToCorrect requestToCorrect = entityCreator.createRequestToCorrect();

        Instant updatedAtBeforeReload = requestToCorrect.getUpdatedAt();
        assertNotNull(updatedAtBeforeReload);
        requestToCorrect = requestToCorrectRepository.findById(requestToCorrect.getId()).get();

        Instant updatedAtAfterReload = requestToCorrect.getUpdatedAt();
        assertNotNull(updatedAtAfterReload);
        assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        requestToCorrect.setWrongData("data");
        requestToCorrectRepository.save(requestToCorrect);
        requestToCorrect = requestToCorrectRepository.findById(requestToCorrect.getId()).get();
        Instant updatedAtAfterModificationAndReload = requestToCorrect.getUpdatedAt();
        assertTrue(updatedAtAfterModificationAndReload.isAfter(updatedAtAfterReload));
    }

}
